<?php

class User extends MvcModel
{

    public $display_field = 'user_login';
    public $has_many = array('Post');

    // TODO: Move to .env or DB config.
    // Config role should be in constructor function.
    public $creator_role = array('give_worker', 'flim-maker');
    public $approver_role = array('give_accountant', 'give_manager', 'administrator');

    public function get_role()
    {
        return wp_get_current_user()->roles;
    }

    public function is_creator_role()
    {
        foreach ($this->get_role() as $key => $role) {
            if (in_array($role, $this->creator_role)) {
                return true;
            }
        }
        return false;
    }

    public function is_approver_role()
    {
        foreach ($this->get_role() as $key => $role) {
            if (in_array($role, $this->approver_role)) {
                return true;
            }
        }
        return false;
    }

}
