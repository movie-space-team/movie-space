<?php

class Post extends MvcModel
{

    public $display_field = 'post_title';
    public $includes = array('User');
    public $primary_key = 'ID';
    public $belongs_to = array(
        'User' => array(
            'foreign_key' => 'post_author',
        ),
    );
    public $has_many = array('ReceiveFunding', 'ApprovalFunding');

    // public $wp_post = array(
    //     'post_type' => true,
    // );

}