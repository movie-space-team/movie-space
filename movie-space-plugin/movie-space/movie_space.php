<?php
/*
Plugin Name: Movie Space
Plugin URI:
Description: Funding features for Wordpress.
Author: Movie Space Team
Version: 1.0
Author URI:
 */

register_activation_hook(__FILE__, 'movie_space_activate');
register_deactivation_hook(__FILE__, 'movie_space_deactivate');

function movie_space_activate()
{
    global $wp_rewrite;
    require_once dirname(__FILE__) . '/movie_space_loader.php';
    $loader = new MovieSpaceLoader();
    $loader->activate();
    $wp_rewrite->flush_rules(true);
}

function movie_space_deactivate()
{
    global $wp_rewrite;
    require_once dirname(__FILE__) . '/movie_space_loader.php';
    $loader = new MovieSpaceLoader();
    $loader->deactivate();
    $wp_rewrite->flush_rules(true);
}