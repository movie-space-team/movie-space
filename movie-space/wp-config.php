<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_moviespace' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '/.]LFmkt4{n4RJqegkN3+/4_z>5[=.$R&^&S#?lj,k.:`y9u2@Dm)Rrfm_$fZRCu' );
define( 'SECURE_AUTH_KEY',  'Oek.q]7?l3[`t?4+01Di&v_6cf6)b~:ZB~Qi[9;9G%UrL9T;y<FQ%(:WnCPdVno[' );
define( 'LOGGED_IN_KEY',    '0VCp40hJ7EB*OzB0Od!a?qW7*Sy4VUbJc<UJC|v;T&,U{V :07Y>nr;>% ,D=S1O' );
define( 'NONCE_KEY',        ')<2=sE$|)suM|rgVTkY|3^TsIeJk$Is&Wc<*Uv@{!T^,O/iLr$z-7AQ#)a <UXuM' );
define( 'AUTH_SALT',        '^W-]DN_-Ne0XxyiHxJh}&I^[Jce)33=pLJ]i@KTvKaSYS*&b8/5j6pvsd[h/B6*`' );
define( 'SECURE_AUTH_SALT', '}1w4^Ys6/XDg+4,KEgMKPczsvO<=2qjjjz9J7ul6`5Y%x0soaJ}CC8KusIy<5k~H' );
define( 'LOGGED_IN_SALT',   ',O1FJ!}oqCkk..>73P40di6 4NqJN^;+xqhs/(arjKYPfso00l%;C7G-eFf}X1Lg' );
define( 'NONCE_SALT',       'C!%c:zpkjP>IP9|1WSQ7E88n(3FW_@aea{ :RJ;=ikT)vm#?a?f6(Wx2Pw#Fbg6l' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
