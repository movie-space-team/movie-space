<?php

class MovieSpaceLoader extends MvcPluginLoader
{

    public $db_version = '1.0';
    public $tables = array();

    public function activate()
    {
        global $wpdb;

        // Set table before running SQL script.
        $this->tables = array(
            'receive_fundings' => $wpdb->prefix . 'receive_fundings',
            'approval_fundings' => $wpdb->prefix . 'approval_fundings',
        );

        // This call needs to be made to activate this app within WP MVC

        $this->activate_app(__FILE__);

        // Perform any databases modifications related to plugin activation here, if necessary

        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        add_option('movie_space_db_version', $this->db_version);

        // Use dbDelta() to create the tables for the app here
        // Receive_fundings table.
        $sql = '
        CREATE TABLE ' . $this->tables['receive_fundings'] . ' (
        id int(11) NOT NULL auto_increment,
        post_id bigint(20) UNSIGNED NOT NULL,
        approved_by_user_id bigint(20) UNSIGNED NOT NULL,
        transaction_number varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        bank_account_number varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        bank_account_name varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        bank_name varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        status varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        estimate_transfer_date datetime DEFAULT "0000-00-00 00:00:00",
        created_date datetime DEFAULT "0000-00-00 00:00:00",
        updated_date datetime DEFAULT "0000-00-00 00:00:00",
        approved_date datetime DEFAULT "0000-00-00 00:00:00",
        remark text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        total_amount double DEFAULT 0,
        fee double DEFAULT 0,
        net_amount double DEFAULT 0,
        vat_amount double DEFAULT 0,
        PRIMARY KEY  (id),
        KEY post_id (post_id),
        KEY approved_by_user_id (approved_by_user_id),
        FOREIGN KEY (post_id) REFERENCES wp_posts(ID),
        FOREIGN KEY (approved_by_user_id) REFERENCES wp_users(ID)
        )
        ';
        dbDelta($sql);

        // Approval_fundings table.
        $sql = '
        CREATE TABLE ' . $this->tables['approval_fundings'] . ' (
        id int(11) NOT NULL auto_increment,
        post_id bigint(20) UNSIGNED NOT NULL,
        approved_by_user_id bigint(20) UNSIGNED NOT NULL,
        status varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        created_date datetime DEFAULT "0000-00-00 00:00:00",
        updated_date datetime DEFAULT "0000-00-00 00:00:00",
        approved_date datetime DEFAULT "0000-00-00 00:00:00",
        remark text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
        PRIMARY KEY  (id),
        KEY post_id (post_id),
        KEY approved_by_user_id (approved_by_user_id),
        FOREIGN KEY (post_id) REFERENCES wp_posts(ID),
        FOREIGN KEY (approved_by_user_id) REFERENCES wp_users(ID)
        )
        ';
        dbDelta($sql);

    }

    public function deactivate()
    {

        // This call needs to be made to deactivate this app within WP MVC

        $this->deactivate_app(__FILE__);

        // Perform any databases modifications related to plugin deactivation here, if necessary

        // TODO: Drop table receive_funding but danger if there's data!
    }

}