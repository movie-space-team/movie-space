<?php

class ApprovalFunding extends MvcModel
{

    // public $display_field = 'Post.post_title';
    public $includes = array('Post');
    public $belongs_to = array('Post');

    public $wp_post = array(
        'post_type' => array(
            'args' => array(
                'menu_icon' => 'dashicons-welcome-write-blog',
            ),
        ),
    );

}