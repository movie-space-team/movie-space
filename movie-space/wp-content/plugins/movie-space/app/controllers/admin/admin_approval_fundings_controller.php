<?php

class AdminApprovalFundingsController extends MvcAdminController
{

    public $default_columns = array(
        // 'id',
        'project_id' => array('value_method' => 'admin_column_id'),
        'project' => array('value_method' => 'admin_column_project'),
        'creator' => array('value_method' => 'admin_column_creator'),
        'updated_date' => array('value_method' => 'admin_column_updated_date'),
        // 'updated_date',
        'status' => array('value_method' => 'admin_column_status'),
        // 'url_edit' => array('value_method' => 'url_link'),
    );
    // TODO: This is duplicate code.

    public function url_link()
    {
        return '';
    }

    public function index()
    {
        // TODO: For custom index page.
        $this->init_default_columns();
        $this->process_params_for_search();

        $params = array_merge($this->params, array(
            'actions' => array('edit' => array('controller' => 'admin_approval_fundings')),
            'left_joins' => array('ApprovalFunding'),
            'includes' => array('ApprovalFunding'),
            'conditions' => array(
                'Post.post_type' => 'give_forms',
                'Post.post_status' => 'pending',
            ),
            'group' => 'Post.ID',
            'order' => 'Post.post_modified DESC',
        ));
        // Load Post model
        $this->load_model('Post');
        $collection = $this->Post->paginate($params);

        foreach ($collection['objects'] as $key => $obj) {
            $obj->__model_name = 'ApprovalFunding';
        }
        // echo '<pre>';
        // print_r($collection);
        // echo '</pre>';
        // die;

        $this->set('objects', $collection['objects']);
        $this->set_pagination($collection);

        // TODO: Move to service.
        $this->load_model('User');

        if ($this->User->is_creator_role()) {
            $admin_table_cells_options = array(
                'actions' => array(
                    'edit' => false,
                    'view' => true,
                    'delete' => false,
                ),
            );

        } else if ($this->User->is_approver_role()) {
            $admin_table_cells_options = array(
                'actions' => array(
                    'edit' => true,
                    'view' => true,
                    'delete' => false,
                ),
            );

        } else {
            $admin_table_cells_options = array(
                'actions' => array(
                    'edit' => false,
                    'view' => false,
                    'delete' => false,
                ),
            );
        }

        $this->set('admin_table_cells_options', $admin_table_cells_options);

    }

    public function add()
    {
        // TODO: Separate code to Service Class.
        // TODO: Adjust with function is_submit().
        // TODO: clean code.
        if (empty($this->params['data'])) {
            global $wpdb;
            $query = "SELECT * FROM `wp_posts` WHERE post_status = 'pending' AND post_type = 'give_forms' ORDER BY `ID` DESC";
            $posts_results = $wpdb->get_results($query);
            $posts = [];
            foreach ($posts_results as $key => $post) {
                $posts[$post->ID] = $post->post_title;
            }

            // TODO: Should move to helper or entity class.
            $status = [
                'PENDING_REVIEW' => 'Pending Review',
                'APPROVED' => 'Approved',
                'REJECTED' => 'Rejected',
            ];

            $this->set('posts', $posts);
            $this->set('status', $status);

        } else {
            $datetime = date("Y-m-d H:i:s");

            // TODO: Need to set cover all fields.
            $this->params['data']['ApprovalFunding']['created_date'] = $datetime;
            $this->params['data']['ApprovalFunding']['updated_date'] = $datetime;
            $this->params['data']['ApprovalFunding']['approved_date'] = $datetime;
            $this->params['data']['ApprovalFunding']['approved_by_user_id'] = get_current_user_id();

            // print_r($this->params['data']);die;
            $post_id = $this->params['data']['ApprovalFunding']['post_id'];
            $status = $this->params['data']['ApprovalFunding']['status'];

            // TODO: Move to Post model.
            // Mapping status ApprovalFunding with WP_Post
            $post_status = 'pending'; // Set default.
            if ($status == 'PENDING_REVIEW') {
                $post_status = 'pending';
            } else if ($status == 'APPROVED') {
                $post_status = 'publish';
            } else if ($status == 'REJECTED') {
                $post_status = 'draft';
            }

            // Delete old data before.
            $this->load_model('ApprovalFunding');
            $this->ApprovalFunding->delete_all(array(
                'conditions' => array('post_id' => $post_id,
                ))
            );

            // Save ApprovalFunding
            global $wpdb;
            // $table_name = $wpdb->prefix . 'approval_fundings';
            $wpdb->insert('wp_approval_fundings', $this->params['data']['ApprovalFunding']);

            // Load Post model
            $this->load_model('Post');
            $this->Post->update($post_id, array(
                'post_status' => $post_status,
                'post_modified' => $datetime,
                'post_modified_gmt' => $datetime,
            ));
            $url = MvcRouter::admin_url(array('controller' => 'admin_approval_fundings', 'action' => 'index'));
            $this->redirect($url);

        }

    }

    public function edit()
    {
        $this->add();
        // echo $this->params['id'];die;
    }

    public function admin_column_id($object)
    {
        return $object->ID;
    }

    public function admin_column_project($object)
    {
        return $object->post_title;
    }

    public function admin_column_creator($object)
    {
        return $object->user->display_name;
    }

    public function admin_column_updated_date($object)
    {
        return $object->post_modified;
    }

    public function admin_column_status($object)
    {
        // TODO: Should get from helper or entity class.
        // $status = [
        //     'PENDING_REVIEW' => 'Pending Review',
        //     'APPROVED' => 'Approved',
        //     'REJECTED' => 'Rejected',
        // ];

        // return $status[$object->status];
        return $object->post_status;

    }

}