<?php

class AdminReceiveFundingsController extends MvcAdminController
{

    public $default_columns = array(
        'id',
        'project' => array('value_method' => 'admin_column_project'),
        'creator' => array('value_method' => 'admin_column_creator'),
        'amount' => array('value_method' => 'admin_column_amount'),
        'updated_date',
        'status' => array('value_method' => 'admin_column_status'),
    );

    public function index()
    {
        // TODO: For custom index page.
        $this->init_default_columns();
        $this->process_params_for_search();
        $collection = $this->model->paginate($this->params);
        $this->set('objects', $collection['objects']);
        $this->set_pagination($collection);

        // TODO: Move to service.
        $this->load_model('User');

        if ($this->User->is_creator_role()) {
            $admin_table_cells_options = array(
                'actions' => array(
                    'edit' => false,
                    'view' => true,
                    'delete' => false,
                ),
            );

        } else if ($this->User->is_approver_role()) {
            $admin_table_cells_options = array(
                'actions' => array(
                    'edit' => true,
                    'view' => true,
                    'delete' => false,
                ),
            );

        } else {
            $admin_table_cells_options = array(
                'actions' => array(
                    'edit' => false,
                    'view' => false,
                    'delete' => false,
                ),
            );
        }

        $this->set('admin_table_cells_options', $admin_table_cells_options);

    }

    public function add()
    {
        // TODO: Separate code to Service Class.
        // TODO: Adjust with function is_submit().
        // TODO: clean code.

        if (empty($this->params['data'])) {
            global $wpdb;
            $query = "SELECT * FROM `wp_posts` WHERE `post_type` LIKE 'give_forms' ORDER BY `post_title` DESC";
            $posts_results = $wpdb->get_results($query);
            $posts = [];
            foreach ($posts_results as $key => $post) {
                $posts[$post->ID] = $post->post_title;
            }

            // TODO: Should move to helper or entity class.
            $status = [
                'PENDING_REVIEW' => 'Pending Review',
                'APPROVED' => 'Approved',
                'REJECTED' => 'Rejected',
                'TRANSFERRED' => 'Transferred',
            ];

            $this->set('posts', $posts);
            $this->set('status', $status);

            // $this->create_or_save();
            // $user = wp_get_current_user();
            // print_r($user->roles);die;
            // $this->load_model('User');
            // print_r($this->User->is_creator_role());die;

        } else {
            // TODO: Need to set cover all fields.
            // print_r($this->params['data']);die;
            $datetime = date("Y-m-d H:i:s");

            $this->params['data']['ReceiveFunding']['created_date'] = $datetime;
            $this->params['data']['ReceiveFunding']['updated_date'] = $datetime;
            $this->params['data']['ReceiveFunding']['approved_date'] = $datetime;

            $this->params['data']['ReceiveFunding']['approved_by_user_id'] = get_current_user_id();

            global $wpdb;
            // $table_name = $wpdb->prefix . 'receive_fundings';
            $wpdb->insert('wp_receive_fundings', $this->params['data']['ReceiveFunding']);
            $url = MvcRouter::admin_url(array('controller' => 'admin_receive_fundings', 'action' => 'index'));
            $this->redirect($url);

        }

    }

    public function edit()
    {
        // TODO: Separate code to Service Class.
        // TODO: Adjust with function is_submit().
        // TODO: clean code.

        if (empty($this->params['data'])) {
            global $wpdb;
            $query = "SELECT * FROM `wp_posts` WHERE `post_type` LIKE 'give_forms' ORDER BY `post_title` DESC";
            $posts_results = $wpdb->get_results($query);
            $posts = [];
            foreach ($posts_results as $key => $post) {
                $posts[$post->ID] = $post->post_title;
            }

            // TODO: Should move to helper or entity class.
            $status = [
                'PENDING_REVIEW' => 'Pending Review',
                'APPROVED' => 'Approved',
                'REJECTED' => 'Rejected',
                'TRANSFERRED' => 'Transferred',
            ];

            $this->load_model('ReceiveFunding');
            $receive_funding = $this->ReceiveFunding->find_by_id($this->params['id']);
            // print_r($receive_funding->bank_name);die;
            $this->set('posts', $posts);
            $this->set('status', $status);
            $this->set('receive_funding', $receive_funding);

            // $this->create_or_save();
            // $user = wp_get_current_user();
            // print_r($user->roles);die;

        } else {
            // TODO: Need to set cover all fields.
            // print_r($this->params['data']);die;
            $datetime = date("Y-m-d H:i:s");

            $this->params['data']['ReceiveFunding']['created_date'] = $datetime;
            $this->params['data']['ReceiveFunding']['updated_date'] = $datetime;
            $this->params['data']['ReceiveFunding']['approved_date'] = $datetime;

            $this->params['data']['ReceiveFunding']['approved_by_user_id'] = get_current_user_id();

            global $wpdb;
            // echo $this->params['id'];
            // $table_name = $wpdb->prefix . 'receive_fundings';
            $wpdb->update('wp_receive_fundings', $this->params['data']['ReceiveFunding'], array('id' => $this->params['data']['ReceiveFunding']['id']));
            // $this->load_model('ReceiveFunding');
            // $this->ReceiveFunding->update($this->params['id'], $this->params['data']['ReceiveFunding']);
            // echo 'OK';
            // die;
            $url = MvcRouter::admin_url(array('controller' => 'admin_receive_fundings', 'action' => 'index'));
            $this->redirect($url);

        }

    }

    public function admin_column_project($object)
    {
        // TODO: Remove when deploy
        // echo '<pre>';
        // print_r($object);
        // echo '</pre>';
        // die;
        return $object->post->post_title;
    }

    public function admin_column_creator($object)
    {
        return $object->post->user->display_name;
    }

    public function admin_column_amount($object)
    {
        return $object->total_amount;
    }

    public function admin_column_status($object)
    {
        // TODO: Should get from helper or entity class.
        $status = [
            'PENDING_REVIEW' => 'Pending Review',
            'APPROVED' => 'Approved',
            'REJECTED' => 'Rejected',
            'TRANSFERRED' => 'Transferred',
        ];

        return ($object->status ? $status[$object->status] : 'Pending Review');
    }
}