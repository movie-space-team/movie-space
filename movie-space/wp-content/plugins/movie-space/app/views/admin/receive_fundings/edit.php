<h2>Edit Receive Funding</h2>

<?php echo $this->form->create($model->name, array('is_admin' => $this->is_admin)); ?>
<?php echo $this->form->open_admin_table(); ?>
<?php echo $this->form->hidden_input('id', array('value' => $receive_funding->id)); ?>
<?php echo $this->form->select('post_id', array('label' => 'Project', 'options' => $posts, 'value' => $receive_funding->post_id)); ?>
<?php echo $this->form->text_input('total_amount', array('label' => 'Amount', 'class' => 'regular-text')); ?>
<?php echo $this->form->text_input('bank_name', array('label' => 'Bank Name', 'class' => 'regular-text', 'value' => $receive_funding->bank_name)); ?>
<?php echo $this->form->text_input('bank_account_number', array('label' => 'Bank Account Number', 'class' => 'regular-text', 'value' => $receive_funding->bank_account_number)); ?>
<?php echo $this->form->text_input('bank_account_name', array('label' => 'Bank Account Name', 'class' => 'regular-text', 'value' => $receive_funding->bank_account_name)); ?>
<?php echo $this->form->close_admin_table(); ?>
<hr />
<?php echo $this->form->open_admin_table(); ?>
<?php echo $this->form->textarea_input('remark', array('label' => 'Remark', 'class' => 'regular-text', 'value' => $receive_funding->remark)); ?>
<?php echo $this->form->select('status', array('label' => 'Status', 'options' => $status, 'value' => $receive_funding->status)); ?>
<?php echo $this->form->close_admin_table(); ?>
<?php echo $this->form->end('Edit Receive Funding'); ?>