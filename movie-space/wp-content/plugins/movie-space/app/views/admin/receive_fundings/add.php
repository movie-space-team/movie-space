<h2>Add New Receive Funding</h2>

<?php echo $this->form->create($model->name, array('is_admin' => $this->is_admin)); ?>
<?php echo $this->form->open_admin_table(); ?>
<?php echo $this->form->select('post_id', array('label' => 'Project', 'options' => $posts)); ?>
<?php echo $this->form->text_input('total_amount', array('label' => 'Amount', 'class' => 'regular-text')); ?>
<?php echo $this->form->text_input('bank_name', array('label' => 'Bank Name', 'class' => 'regular-text')); ?>
<?php echo $this->form->text_input('bank_account_number', array('label' => 'Bank Account Number', 'class' => 'regular-text')); ?>
<?php echo $this->form->text_input('bank_account_name', array('label' => 'Bank Account Name', 'class' => 'regular-text')); ?>
<?php echo $this->form->close_admin_table(); ?>
<!-- <hr /> -->
<?php //echo $this->form->open_admin_table(); ?>
<?php //echo $this->form->textarea_input('remark', array('label' => 'Remark', 'class' => 'regular-text')); ?>
<?php //echo $this->form->select('status', array('label' => 'Status', 'options' => $status)); ?>
<?php //echo $this->form->close_admin_table(); ?>


<?php echo $this->form->end('Add New Receive Funding'); ?>