<h2>Edit Approval Funding</h2>
<?php //print_r($params);?>
<?php echo $this->form->create($model->name, array('is_admin' => $this->is_admin)); ?>
<?php echo $this->form->open_admin_table(); ?>
<?php echo $this->form->select('post_id', array('label' => 'Project', 'options' => $posts, 'value' => $params['id'])); ?>
<?php echo $this->form->textarea_input('remark', array('label' => 'Remark', 'class' => 'regular-text')); ?>
<?php echo $this->form->select('status', array('label' => 'Status', 'options' => $status)); ?>
<?php echo $this->form->close_admin_table(); ?>
<?php echo $this->form->end('Edit Approval Funding');