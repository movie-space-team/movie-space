<?php
// TODO: Clean code when deploy
// MvcConfiguration::set(array(
//     'Debug' => true,
// ));

MvcConfiguration::set(array(
    /* Set permission levels for different components */
    'admin_controller_capabilities' => array(
        // 'receive_fundings' => 'give_manager',
        // 'receive_fundings' => 'administrator',
        'receive_fundings' => 'read',
        'approval_fundings' => 'read',
        // 'posts' => 'read',
        // 'readings' => 'edit_others_posts'
    ),
    'Debug' => false, //for production
));

// MvcConfiguration::append(array(
//     'AdminPages' => array(
//         'approval_fundings' => array(
//             // 'approval_pending',
//             'add',
//             'delete',
//             'edit',
//         ),
//     ),
// ));