<?php
/*
Plugin Name: WP Movie Space
Plugin URI: 
Description: Funding and Streaming features for Wordpress.
Version: 1.0.0
Author: MovieSpace Team
Author URI: mailto:pakinwet@gmail.com
License: 
License URI: 
Text Domain: wp-movie-space
Domain Path: /assets/lang
Requires PHP: 5.4
*/
//------------------------------------------------------------
//
// NOTE:
//
// Try NOT to add any code line in this file.
//
// Use "app\Main.php" to add your hooks.
//
//------------------------------------------------------------
require_once( __DIR__ . '/app/Boot/bootstrap.php' );