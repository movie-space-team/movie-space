<?php

namespace WPMovieSpace\Models;

use WPMVC\MVC\Models\PostModel as Model;
use WPMVC\MVC\Traits\FindTrait;

/**
 * FundingApproval model.
 * WordPress MVC model.
 *
 * @author MovieSpace Team <pakinwet@gmail.com>
 * @package wp-movie-space
 * @version 1.0.0
 */
class FundingApproval extends Model
{
    use FindTrait;

    // protected $type = 'funding_approval';

    /**
     * Labels.
     * @var array
     */
    // protected $registry_labels = [
    //     'name' => 'FundingApprovals',
    //     'singular_name' => 'FundingApproval',
    //     'menu_name' => 'FundingApprovals',
    // ];
}