<?php

namespace WPMovieSpace\Controllers;

use WPMVC\MVC\Controller;

/**
 * AdminMenuController
 * WordPress MVC controller.
 *
 * @author MovieSpace Team <pakinwet@gmail.com>
 * @package wp-movie-space
 * @version 1.0.0
 */
class AdminMenuController extends Controller
{
    /**
     * @since 1.0.0
     *
     *
     * @return
     */
    public function admin_menu_register()
    {
        // add_menu_page(
        //     'Funding Approval',
        //     'Funding Approval',
        //     'funding_approval',
        //     'myplugin/myplugin-admin.php',
        //     '',
        //     plugins_url('myplugin/images/icon.png'),
        //     6
        // );
    }
}