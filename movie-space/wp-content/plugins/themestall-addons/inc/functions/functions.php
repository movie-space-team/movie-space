<?php
function crowdpress_social_share(){
	global $post;
	?>
	<div class="blog-social clearfix text-right">
    	<h5><?php echo esc_html__('Social Share', 'crowdpress'); ?></h5>
    	<a target="_blank" href="//www.facebook.com/share.php?u=<?php print(urlencode(the_permalink())); ?>&title=<?php print(urlencode(get_the_title())); ?>" data-toggle="tooltip" data-placement="bottom" title="<?php esc_html_e('Share on Facebook', 'crowdpress')?>" data-animation="false">
            <div class="socibox">
                <span class="fab fa-facebook"></span>
            </div>
        </a>
        
        <a target="_blank" href="http://twitter.com/home?status=<?php echo urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')); ?>:<?php esc_url(the_permalink()); ?>" data-toggle="tooltip" data-placement="bottom" title="<?php esc_html_e('Share on Twitter', 'crowdpress')?>" data-animation="false">
            <div class="socibox">
                <span class="fab fa-twitter"></span>
            </div>
        </a>
        
        <a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php esc_url(the_permalink()); ?>&title=<?php urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')); ?>&source=LinkedIn" data-toggle="tooltip" data-placement="bottom" title="<?php esc_html_e('Share on Linkedin', 'crowdpress')?>" data-animation="false">
            <div class="socibox">
                <span class="fab fa-linkedin"></span>
            </div>
        </a>
        
        <a target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php esc_url(the_permalink()); ?>&media=<?php echo esc_url(wp_get_attachment_url( get_post_thumbnail_id($post->ID)));?>&description=<?php esc_html(the_title()); ?> on <?php bloginfo('name'); ?> <?php echo esc_url(site_url()); ?>" data-toggle="tooltip" data-placement="bottom" title="<?php esc_html_e('Share on Pinterest', 'crowdpress')?>" data-animation="false">
            <div class="socibox">
                <span class="fab fa-pinterest"></span>
            </div>
        </a>
        
        <a target="_blank" href="mailto:?subject=<?php echo esc_html__('Check this post -', 'crowdpress'); ?> <?php esc_html(the_title());?> &body= <?php esc_url(the_permalink()); ?>&title=<?php esc_html(the_title()); ?>" data-toggle="tooltip" data-placement="bottom" title="<?php esc_html_e('Share by email', 'crowdpress')?>" data-animation="false">
            <div class="socibox">
                <span class="fas fa-envelope"></span>
            </div>
        </a>
	</div>
	<?php
}

function crowdpress_new_contactmethods( $crowdpress_contactmethods ) {
	// Add Twitter
	$crowdpress_contactmethods['twitter'] = esc_html__('Twitter', 'crowdpress');
	//add Facebook
	$crowdpress_contactmethods['facebook'] = esc_html__('Facebook', 'crowdpress');
	//add LinkedIn
	$crowdpress_contactmethods['behance'] = esc_html__('Behance', 'crowdpress');
	//add GooglePlus
	$crowdpress_contactmethods['youtube'] = esc_html__('Youtube', 'crowdpress');
	//add Dribbble
	$crowdpress_contactmethods['linkedin'] = esc_html__('LinkedIn', 'crowdpress');
	 
	return $crowdpress_contactmethods;
}
add_filter('user_contactmethods','crowdpress_new_contactmethods',10,1);

function crowdpress_social_link_author(){
	global $post;
	?>
    <div class="author-social">
    	<?php if(get_the_author_meta('facebook') != ''): ?>
        <a href="<?php echo esc_url(get_the_author_meta('facebook')); ?>" title="<?php echo esc_attr__('Facebook', 'crowdpress'); ?>"><i class="fa fa-facebook"></i></a>
        <?php endif; ?>
        <?php if(get_the_author_meta('twitter') != ''): ?>
        <a href="<?php echo esc_url(get_the_author_meta('twitter')); ?>" title="<?php echo esc_attr__('Twitter', 'crowdpress'); ?>"><i class="fa fa-twitter"></i></a>
        <?php endif; ?>
        <?php if(get_the_author_meta('instagram') != ''): ?>
        <a href="<?php echo esc_url(get_the_author_meta('instagram')); ?>" title="<?php echo esc_attr__('Instagram', 'crowdpress'); ?>"><i class="fa fa-instagram"></i></a>
        <?php endif; ?>
        <?php if(get_the_author_meta('googleplus') != ''): ?>
        <a href="<?php echo esc_url(get_the_author_meta('googleplus')); ?>" title="<?php echo esc_attr__('Google Plus', 'crowdpress'); ?>"><i class="fa fa-google-plus"></i></a>
        <?php endif; ?>
        <?php if(get_the_author_meta('pinterest') != ''): ?>
        <a href="<?php echo esc_url(get_the_author_meta('pinterest')); ?>" title="<?php echo esc_attr__('Pinterest', 'crowdpress'); ?>"><i class="fa fa-pinterest"></i></a>
        <?php endif; ?>
    </div>
	<?php
}

/*----------------register form for woocommerce----------------*/
add_shortcode( 'crowdpress_wc_reg_form', 'crowdpress_separate_registration_form' );
    
function crowdpress_separate_registration_form() {
   if ( is_admin() ) return;
   if ( is_user_logged_in() ) return;
   ob_start();
 
   // NOTE: THE FOLLOWING <FORM></FORM> IS COPIED FROM woocommerce\templates\myaccount\form-login.php
   // IF WOOCOMMERCE RELEASES AN UPDATE TO THAT TEMPLATE, YOU MUST CHANGE THIS ACCORDINGLY
 
   do_action( 'woocommerce_before_customer_login_form' );
 
   ?>
      <form method="post" class="woocommerce-form woocommerce-form-register register" <?php do_action( 'woocommerce_register_form_tag' ); ?> >
 
         <?php do_action( 'woocommerce_register_form_start' ); ?>
 
         <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?> 
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
               <input type="text" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="username" id="reg_username" placeholder="Enter username*" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
            </p>
 
         <?php endif; ?>
 
         <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">            
            <input type="email" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="email" id="reg_email" placeholder="Enter email address*" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
         </p>
 
         <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
 
            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
               <input type="password" class="woocommerce-Input woocommerce-Input--text input-text form-control" name="password" id="reg_password" placeholder="Enter password*" autocomplete="new-password" />
            </p>
 
         <?php else : ?>
 
            <p><?php esc_html_e( 'A password will be sent to your email address.', 'crowdpress' ); ?></p>
 
         <?php endif; ?>
 
         <?php do_action( 'woocommerce_register_form' ); ?>
 
         <p class="woocommerce-FormRow form-row">
            <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
            <button type="submit" class="button btn btn-primary" name="register" value="<?php esc_attr_e( 'Signup Now', 'crowdpress' ); ?>"><?php esc_html_e( 'Signup Now', 'crowdpress' ); ?></button><a class="already-account" href="<?php echo esc_url(get_permalink( get_option('woocommerce_myaccount_page_id')));?>"><?php echo esc_html__('Already Have Account', 'crowdpress'); ?></a>
         </p>
 
         <?php do_action( 'woocommerce_register_form_end' ); ?>
 
      </form>
 
   <?php
     
   return ob_get_clean();
}

/** Add Colorpicker Field to "Add New Category" Form **/
function give_forms_category_form_custom_field_add( $taxonomy ) {
?>
<div class="form-field">
    <label for="icon_id"><?php echo esc_html__('Image URL', 'themestall'); ?></label>
    <input name="term_meta[icon_id]" class="donation-icon" type="text" value="" />
    <p class="description"><?php echo esc_html__('Write image url here', 'themestall'); ?></p>
</div>
<?php
}
add_action('give_forms_category_add_form_fields', 'give_forms_category_form_custom_field_add', 10 );

function give_forms_category_taxonomy_custom_fields($tag) {  
   // Check for existing taxonomy meta for the term you're editing  
    $t_id = $tag->term_id; // Get the ID of the term you're editing  
    $term_meta = get_option( "taxonomy_term_$t_id" ); // Do the check  
?>  
  
<tr class="form-field">  
    <th scope="row" valign="top">  
        <label for="icon_id"><?php esc_html_e('Image URL', 'themestall'); ?></label>  
    </th>  
    <td>  
        <input type="text" name="term_meta[icon_id]" id="term_meta[icon_id]" size="25" style="width:90%;" value="<?php echo $term_meta['icon_id'] ? $term_meta['icon_id'] : ''; ?>"><br />  
        <span class="description"><?php esc_html_e('The image URL', 'themestall'); ?></span>  
    </td>  
</tr>  
  
<?php  
}

function save_taxonomy_custom_fields( $term_id ) {  
    if ( isset( $_POST['term_meta'] ) ) {  
        $t_id = $term_id;  
        $term_meta = get_option( "taxonomy_term_$t_id" );  
        $cat_keys = array_keys( $_POST['term_meta'] );  
            foreach ( $cat_keys as $key ){  
            if ( isset( $_POST['term_meta'][$key] ) ){  
                $term_meta[$key] = $_POST['term_meta'][$key];  
            }  
        }  
        //save the option array  
        update_option( "taxonomy_term_$t_id", $term_meta );  
    }  
}

// Add the fields to the "presenters" taxonomy, using our callback function  
add_action( 'give_forms_category_edit_form_fields', 'give_forms_category_taxonomy_custom_fields', 10, 2 );  
  
// Save the changes made on the "presenters" taxonomy, using our callback function  
add_action( 'edited_give_forms_category', 'save_taxonomy_custom_fields', 10, 2 );
add_action('created_give_forms_category', 'save_taxonomy_custom_fields', 11, 1);

add_action( 'woocommerce_single_product_summary', 'crowdpress_template_single_sharing', 50 );

function crowdpress_template_single_sharing(){
	global $post;
	?>
        
    <ul class="social">
    	<li><?php echo esc_html__('Share This: ', 'crowdpress'); ?></li>
        <li class="facebook">        
        <a href="//www.facebook.com/share.php?u=<?php echo esc_url(get_permalink($post->ID)); ?>" target="_blank" title="<?php echo esc_attr__('Facebook', 'crowdpress'); ?>" data-toggle="tooltip" data-placement="bottom" data-animation="false"><i class="fab fa-facebook"></i></a></li>
        <li class="twitter"><a href="http://twitter.com/home?status=<?php echo urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')); ?>: <?php echo esc_url(get_permalink($post->ID)); ?>" target="_blank" title="<?php echo esc_attr__('Twitter', 'crowdpress'); ?>" data-toggle="tooltip" data-placement="bottom" data-animation="false"><i class="fab fa-twitter"></i></a></li>
        <li class="linkedin"><a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo esc_url(get_permalink($post->ID)); ?>&title=<?php echo esc_attr(get_the_title($post->ID)); ?>&summary=&source=" target="_blank" title="<?php echo esc_attr__('LinkedIn', 'crowdpress'); ?>" data-toggle="tooltip" data-placement="bottom" data-animation="false"><i class="fab fa-linkedin"></i></a></li>        
        <li class="pinterest"><a href="http://pinterest.com/pin/create/button/?url=<?php echo esc_url(get_permalink($post->ID)); ?>&amp;description=<?php echo esc_attr(get_the_title($post->ID)); ?>" target="_blank" title="<?php echo esc_attr__('Pinterest', 'crowdpress'); ?>" data-toggle="tooltip" data-placement="bottom" data-animation="false"><i class="fab fa-pinterest"></i></a>
    </ul>
    <?php
}