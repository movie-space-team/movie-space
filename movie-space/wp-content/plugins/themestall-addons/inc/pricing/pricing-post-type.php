<?php
if ( ! function_exists('themestall_pricing_post_type') ) {

// Register Custom Post Type
function themestall_pricing_post_type() {

	$labels = array(
		'name'                => _x( 'Pricing tables', 'Post Type General Name', 'themestall' ),
		'singular_name'       => _x( 'Pricing table', 'Post Type Singular Name', 'themestall' ),
		'menu_name'           => __( 'Pricing table', 'themestall' ),
		'parent_item_colon'   => __( 'Parent Item:', 'themestall' ),
		'all_items'           => __( 'All Items', 'themestall' ),
		'view_item'           => __( 'View Item', 'themestall' ),
		'add_new_item'        => __( 'Add New Item', 'themestall' ),
		'add_new'             => __( 'Add New', 'themestall' ),
		'edit_item'           => __( 'Edit Item', 'themestall' ),
		'update_item'         => __( 'Update Item', 'themestall' ),
		'search_items'        => __( 'Search Item', 'themestall' ),
		'not_found'           => __( 'Not found', 'themestall' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'themestall' ),
	);
	$args = array(
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'revisions', 'page-attributes', ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'pricing', $args );

}

// Hook into the 'init' action
add_action( 'init', 'themestall_pricing_post_type', 0 );

}


include 'meta-boxes-pricing.php';

?>