<?php
/**
 * Initialize the meta boxes. 
 */
add_action( 'admin_init', 'pricing_meta_boxes' );

function pricing_meta_boxes(){
  global $wpdb, $post;
  if( function_exists( 'ot_get_option' ) ):
  $my_meta_boxx = array(
    'id'        => 'my_meta_box',
    'title'     => __('Package Information', 'themestall'),
    'desc'      => '',
    'pages'     => array( 'pricing' ),
    'context'   => 'normal',
    'priority'  => 'high',
    'fields'    => array(
	array(
        'id'          => 'price_currency',
        'label'       => __( 'Price', 'themestall' ),
        'desc'        => '',
        'std'         => '$12',
        'type'        => 'text',
        'rows'        => '1',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'price_period',
        'label'       => __( 'Price Period', 'themestall' ),
        'desc'        => '',
        'std'         => '/Month',
        'type'        => 'text',
        'rows'        => '1',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
    array(
        'id'          => 'featured',
        'label'       => __( 'Featured', 'themestall' ),
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),   
      array(
        'id'          => 'feature_info',
        'label'       => __( 'Feature info', 'themestall' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'list-item',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'settings'    => array(
          array(
            'id'          => 'feature_text',
            'label'       => __( 'Description', 'themestall' ),
            'desc'        => '',
            'std'         => '',
            'type'        => 'textarea',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => '',
            'condition'   => '',
            'operator'    => 'and'
          ),
		  array(
            'id'          => 'select_icon',
            'label'       => __( 'Set Icon', 'themestall' ),
            'desc'        => __( 'Paste a icon class here', 'themestall' ),
            'std'         => 'fa fa-envelope-o',
            'type'        => 'text',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => '',
            'condition'   => '',
            'operator'    => 'and'
          )
        )
      ), 
      array(
        'id'          => 'button_link',
        'label'       => __( 'Button link', 'themestall' ),
        'desc'        => '',
        'std'         => '#',
        'type'        => 'text',
        'rows'        => '1',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ), 
      array(
        'id'          => 'button_text',
        'label'       => __( 'Button text', 'themestall' ),
        'desc'        => '',
        'std'         => 'Order Now',
        'type'        => 'text',
        'rows'        => '1',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
         
    )
  );
  
  ot_register_meta_box( $my_meta_boxx );
  endif;  //if( function_exists( 'ot_get_option' ) ):
}
?>