<?php
if ( ! function_exists('themestall_portfolio_post_type') ) {

// Register Custom Post Type
function themestall_portfolio_post_type() {

	$labels = array(
		'name'                => _x( 'Portfolio', 'Post Type General Name', 'themestall' ),
		'singular_name'       => _x( 'Portfolio', 'Post Type Singular Name', 'themestall' ),
		'menu_name'           => __( 'Portfolio', 'themestall' ),
		'parent_item_colon'   => __( 'Parent Item:', 'themestall' ),
		'all_items'           => __( 'All Items', 'themestall' ),
		'view_item'           => __( 'View Item', 'themestall' ),
		'add_new_item'        => __( 'Add New Item', 'themestall' ),
		'add_new'             => __( 'Add New', 'themestall' ),
		'edit_item'           => __( 'Edit Item', 'themestall' ),
		'update_item'         => __( 'Update Item', 'themestall' ),
		'search_items'        => __( 'Search Item', 'themestall' ),
		'not_found'           => __( 'Not found', 'themestall' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'themestall' ),
	);
	
	$rewrite = array(
		'slug'                => 'portfolio',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	
	$args = array(
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail',),
		'taxonomies'          => array( 'portfolio_category', 'portfolio_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'query_var'           => 'portfolio',
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	
	register_post_type( 'portfolio', $args );

}

// Hook into the 'init' action
add_action( 'init', 'themestall_portfolio_post_type', 0 );

}

if ( ! function_exists( 'themestall_portfolio_category_taxonomy' ) ) {

// Register Custom Taxonomy
function themestall_portfolio_category_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Categories', 'Taxonomy General Name', 'themestall'),
		'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'themestall'),
		'menu_name'                  => __( 'Category', 'themestall'),
		'all_items'                  => __( 'All Items', 'themestall'),
		'parent_item'                => __( 'Parent Item', 'themestall'),
		'parent_item_colon'          => __( 'Parent Item:', 'themestall'),
		'new_item_name'              => __( 'New Item Name', 'themestall'),
		'add_new_item'               => __( 'Add New Item', 'themestall'),
		'edit_item'                  => __( 'Edit Item', 'themestall'),
		'update_item'                => __( 'Update Item', 'themestall'),
		'separate_items_with_commas' => __( 'Separate items with commas', 'themestall'),
		'search_items'               => __( 'Search Items', 'themestall'),
		'add_or_remove_items'        => __( 'Add or remove items', 'themestall'),
		'choose_from_most_used'      => __( 'Choose from the most used items', 'themestall'),
		'not_found'                  => __( 'Not Found', 'themestall'),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'portfolio_category', array( 'portfolio' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'themestall_portfolio_category_taxonomy', 0 );

}

if ( ! function_exists( 'yoke_portfolio_tag_taxonomy' ) ) {

// Register Custom Taxonomy
function yoke_portfolio_tag_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Tags', 'Taxonomy General Name', 'themestall'),
		'singular_name'              => _x( 'Tag', 'Taxonomy Singular Name', 'themestall'),
		'menu_name'                  => __( 'Tag', 'themestall'),
		'all_items'                  => __( 'All Items', 'themestall'),
		'parent_item'                => __( 'Parent Item', 'themestall'),
		'parent_item_colon'          => __( 'Parent Item:', 'themestall'),
		'new_item_name'              => __( 'New Item Name', 'themestall'),
		'add_new_item'               => __( 'Add New Item', 'themestall'),
		'edit_item'                  => __( 'Edit Item', 'themestall'),
		'update_item'                => __( 'Update Item', 'themestall'),
		'separate_items_with_commas' => __( 'Separate items with commas', 'themestall'),
		'search_items'               => __( 'Search Items', 'themestall'),
		'add_or_remove_items'        => __( 'Add or remove items', 'themestall'),
		'choose_from_most_used'      => __( 'Choose from the most used items', 'themestall'),
		'not_found'                  => __( 'Not Found', 'themestall'),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'portfolio_tag', array( 'portfolio' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'yoke_portfolio_tag_taxonomy', 0 );

}

require_once 'meta-boxes-portfolio.php';

?>