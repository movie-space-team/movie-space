<?php
/**
 * Template Name: Submit Project Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in prestige consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage crowdpress
 * @since crowdpress 1.0.0
 */
if(!is_user_logged_in()):
if(class_exists( 'woocommerce' )):
	$log_in_url = get_permalink( get_option('woocommerce_myaccount_page_id') );
else:
	$log_in_url = wp_login_url();
endif;
wp_redirect( $log_in_url );
endif;

$success_message = '';
$current_user = wp_get_current_user();
$author_link = get_author_posts_url( $current_user->ID );
if( isset ( $_POST['submit_project'] ) ) {
	$errors = array();
	$check = true;
	
	$post_title = $_POST['project_name'];
	$post_content = sanitize_text_field($_POST['submit_description']);
	$amount_goal = $_POST['amount_goal'];
	$maximum_amount = $_POST['maximum_amount'];
	$minimum_amount = $_POST['minimum_amount'];
	
	if ( $post_title == '' ){
		$errors['project_name'] = '<span class="color">'.esc_html__('Please Type your project name', 'crowdpress').'</span>';
		$check = false;
	}		
	
	if ( $post_content == '' ){
		$errors['submit_description'] = '<span class="color">'.esc_html__('Please Type your project description', 'crowdpress').'</span>';
		$check = false;
	}
	
	if ( $amount_goal == '' ){
		$errors['amount_goal'] = '<span class="color">'.esc_html__('Please Type your project goal', 'crowdpress').'</span>';
		$check = false;
	}
	
	if ( $maximum_amount == '' ){
		$errors['maximum_amount'] = '<span class="color">'.esc_html__('Please Type Maximum amount', 'crowdpress').'</span>';
		$check = false;
	}
	
	if ( $minimum_amount == '' ){
		$errors['minimum_amount'] = '<span class="color">'.esc_html__('Please Type Minimum amount', 'crowdpress').'</span>';
		$check = false;
	}
	
	if ($_FILES['submit_projects_image']['size'] == 0) {
		$errors['submit_projects_image'] = '<span class="color">'.esc_html__('Please upload screenshot', 'crowdpress').'</span>';
		$check = false;
	}
	
	if($check){
		$my_post = array(
		'post_title'    => $post_title,
		'post_content'  => $post_content,
		'post_status'   => 'pending',
		'post_author'   => $current_user->ID,
		'post_type'     => 'give_forms',
		);
		
		// Insert the post into the database
		$post_id = wp_insert_post( $my_post );
		
		if(isset($_POST['submit_category'])){
			$terms1 = $_POST['submit_category'];
			wp_set_post_terms( $post_id, $terms1, 'give_forms_category' );
		}
		
		if(isset($_POST['submit_tags'])){
			$terms2 = explode(',', $_POST['submit_tags']);
			wp_set_post_terms( $post_id, $terms2, 'give_forms_tag' );
		}		
		
		if ($_FILES) {
			array_reverse($_FILES);
			foreach ($_FILES as $file => $array) {
				$set_feature = 1; //if $i ==0 then we are dealing with the first post
				$newupload = crowdpress_insert_attachment($file,$post_id, $set_feature);
			}
		}
		
		$settings['display_settings'][ 'display_style' ] = 'buttons';

		update_post_meta( $post_id, '_give_legacy_form_template_settings', $settings );
		update_post_meta( $post_id, '_give_form_template', 'legacy' );
		update_post_meta( $post_id, '_give_content_option', 'enabled' );
		update_post_meta( $post_id, '_give_goal_format', 'percentage' );
		update_post_meta( $post_id, '_give_goal_option', 'enabled' );
		update_post_meta( $post_id, '_give_set_goal', $amount_goal );
		update_post_meta( $post_id, '_give_custom_amount_range_maximum', $maximum_amount );
		update_post_meta( $post_id, '_give_custom_amount_range_minimum', $minimum_amount );
		$redirect_url =(function_exists('ot_get_option'))? ot_get_option( 'thank_you_page' ) : '';
		if($redirect_url != ''){
			wp_redirect(esc_url(get_permalink($redirect_url)));
		} else{
			$success_message = esc_html__('Project Submit Successful!', 'crowdpress');
		}		
		
	}
}
 
   ?>
<?php get_header(); ?>
<?php get_template_part( 'layout/before', '' ); ?>	
   
   <div class="start-project-section">
   		<div class="row d-flex justify-content-center">
    	<div class="col-lg-10">
            <form class="start-project" method="post" enctype="multipart/form-data" autocomplete="off">
                <div class="content clearfix">
                	<?php if($success_message != ''): ?>
                	<div class="form-group"><p class="success-message color"><?php echo esc_html($success_message); ?></p></div>
                    <?php endif; ?>
                    <div class="form-group row">
                        <label class="col-md-2 control-label text-left"><?php esc_html_e('Title *', 'crowdpress'); ?>
                        <?php if(isset($errors['project_name'])) echo wp_kses($errors['project_name'], array('span'=>array('class'=>array()))); ?>
                        </label>
                        <div class="col-md-10">
                            <input type="text" value="<?php if(isset($_POST['project_name'])) echo esc_html($_POST['project_name']); ?>" class="form-control" name="project_name">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <?php
                        $terms = get_terms( 'give_forms_category', array( 'hide_empty' => false ) );
                        ?>
                        <label class="col-md-2 control-label text-left"><?php esc_html_e('Category *', 'crowdpress'); ?></label>
                        <div class="col-md-10">
                            <select name="submit_category" class="select-project">
                                <?php 
								foreach ( $terms as $term ):
								?>
                                <option value="<?php echo esc_html($term->term_id); ?>"><?php echo esc_html($term->name); ?></option>
                                <?php 
								endforeach; ?>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <?php
                        $terms1 = get_terms( 'give_forms_tag', array( 'hide_empty' => false ) );
                        ?>
                        <label class="col-md-2 control-label text-left"><?php esc_html_e('Tags *', 'crowdpress'); ?></label>
                        <div class="col-md-10">
                        	<input type="text" value="<?php if(isset($_POST['submit_tags'])) echo esc_html($_POST['submit_tags']); ?>" class="form-control" name="submit_tags">
                        </div>
                    </div>
    
                    <div class="form-group row">
                        <label class="col-md-2 control-label text-left"><?php esc_html_e('Description *', 'crowdpress'); ?>
                        <?php if(isset($errors['submit_description'])) echo wp_kses($errors['submit_description'], array('span'=>array('class'=>array()))); ?>
                        </label>
                        <div class="col-md-10">
                            <textarea name="submit_description" class="form-control"><?php if(isset($_POST['submit_description'])) echo esc_html($_POST['submit_description']); ?></textarea>
                        </div>
                    </div>
    
                    <div class="form-group row">
                        <label class="col-md-2 control-label text-left images-label"><?php esc_html_e('Image *', 'crowdpress'); ?>
                        <?php if(isset($errors['submit_projects_image'])) echo wp_kses($errors['submit_projects_image'], array('span'=>array('class'=>array()))); ?>
                        </label>
                        <div class="col-md-10">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-preview thumbnail"></div>
                                <span class="btn btn-primary btn-file">
                                    <span class="fileupload-new"></span>
                                    <span class="fileupload-exists"></span>
                                    <input type="file" name="submit_projects_image">
                                </span>
                                <a href="#" class="btn btn-primary fileupload-exists" data-dismiss="fileupload"><?php esc_html_e('Remove', 'crowdpress'); ?></a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-md-2 control-label text-left"><?php esc_html_e('Goal *', 'crowdpress'); ?>
                        <?php if(isset($errors['amount_goal'])) echo wp_kses($errors['amount_goal'], array('span'=>array('class'=>array()))); ?>
                        </label>
                        <div class="col-md-10">
                            <input type="text" value="<?php if(isset($_POST['amount_goal'])) echo esc_html($_POST['amount_goal']); ?>" class="form-control" name="amount_goal">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-md-2 control-label text-left"><?php esc_html_e('Min Amount *', 'crowdpress'); ?>
                        <?php if(isset($errors['minimum_amount'])) echo wp_kses($errors['minimum_amount'], array('span'=>array('class'=>array()))); ?>
                        </label>
                        <div class="col-md-10">
                            <input type="text" value="<?php if(isset($_POST['minimum_amount'])) echo esc_html($_POST['minimum_amount']); ?>" class="form-control" name="minimum_amount">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label class="col-md-2 control-label text-left"><?php esc_html_e('Max Amount *', 'crowdpress'); ?>
                        <?php if(isset($errors['maximum_amount'])) echo wp_kses($errors['maximum_amount'], array('span'=>array('class'=>array()))); ?>
                        </label>
                        <div class="col-md-10">
                            <input type="text" value="<?php if(isset($_POST['maximum_amount'])) echo esc_html($_POST['maximum_amount']); ?>" class="form-control" name="maximum_amount">
                        </div>
                    </div>
                    
                </div>
                
                <div class="content clearfix">                    
                    <div class="product-fields-free-version">
                    <div class="form-group row">
                    	<div class="col-md-2"></div>
                        <div class="col-md-10">
                            <button class="btn btn-primary" type="submit" name="submit_project"><?php esc_html_e('Submit', 'crowdpress'); ?></button>
                        </div>
                    </div>
                    </div>
                </div>
            </form>
        </div>
        </div>
    </div>
    <?php get_template_part( 'layout/after', '' ); ?>
<?php get_footer(); ?>