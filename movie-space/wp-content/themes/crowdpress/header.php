<?php
/**
 * The Header for multipage of theme
 *
 * Displays all of the <head> section and everything up till </header>
 *
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>
	<?php $preloader = (function_exists('ot_get_option'))? ot_get_option( 'preloader', 'off' ) : 'off'; ?>
    <?php if($preloader == 'on'): ?>
	<div id="loader-wrapper">
        <div id="loader"></div>
        <div class="loader-section section-left"></div>
        <div class="loader-section section-right"></div>
    </div>
    <?php endif; ?>

    <div id="wrapper">
        <?php $header_style = (function_exists('ot_get_option'))? ot_get_option( 'header_style', 'style1' ) : 'style1'; ?>
        <?php get_template_part( 'header/header', $header_style ); ?>

        <?php if(is_page_template( 'page-templates/home-page.php' )): ?>
        <?php else: ?>
        	<?php if(is_page()): ?>
                    <?php get_template_part( 'header/banner', '' ); ?>
            <?php else: ?>
            <?php get_template_part( 'header/banner', '' ); ?>
            <?php endif; ?>
        <?php endif; ?>