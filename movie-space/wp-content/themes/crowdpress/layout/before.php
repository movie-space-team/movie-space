<?php
	$col_class = 'col-lg-8 col-md-8 col-12';
	$bg_class = 'bglight';
	$sidebar = 'sidebar-1';
	if(is_singular('tribe_events')){
		$layout = (function_exists('ot_get_option'))? ot_get_option( 'single_event_layout', 'full' ) : 'full';
	}elseif(is_page()){
		$bg_class = 'bgwhite';
		$layout = get_post_meta( get_the_ID(), 'page_layout', true );
		$sidebar = get_post_meta( get_the_ID(), 'sidebar', true );		
		if($sidebar == '') $sidebar = 'sidebar-1';
		if($layout != ''){
			$layout = $layout;
		} else {
			$layout = 'full';
		}
	}elseif(is_single()){
		$layout = (function_exists('ot_get_option'))? ot_get_option( 'single_layout', 'rs' ) : 'rs';
		$sidebar = (function_exists('ot_get_option'))? ot_get_option( 'blog_single_sidebar', 'sidebar-1' ) : 'sidebar-1';
	}
	else{
		$layout = (function_exists('ot_get_option'))? ot_get_option( 'blog_layout', 'rs' ) : 'rs';
		$sidebar = (function_exists('ot_get_option'))? ot_get_option( 'blog_sidebar', 'sidebar-1' ) : 'sidebar-1';
	}
	
	if(is_author()){
		$layout = 'full';
	}
	
	if(is_singular('portfolio')){
		$layout = (function_exists('ot_get_option'))? ot_get_option( 'portfolio_single_layout', 'full' ) : 'full';
		$sidebar = (function_exists('ot_get_option'))? ot_get_option( 'portfolio_single_sidebar', 'sidebar-1' ) : 'sidebar-1';
		$bg_class = 'bgwhite';
	}
	
	if(is_post_type_archive('portfolio')){
		$layout = (function_exists('ot_get_option'))? ot_get_option( 'portfolio_layout', 'full' ) : 'full';
		$sidebar = (function_exists('ot_get_option'))? ot_get_option( 'portfolio_sidebar', 'sidebar-1' ) : 'sidebar-1';
		$bg_class = 'bgwhite';
	}
	
	if(is_singular('give_forms')){
		$layout = 'full';
	}
	
	if(is_post_type_archive('give_forms') || is_tax( 'give_forms_category' )){
		$layout = 'full';
		$bg_class = 'bgwhite';
	}
	
	if ( class_exists( 'woocommerce' ) ){
		if( is_product() ){
			$layout = (function_exists('ot_get_option'))? ot_get_option( 'shop_single_layout', 'full' ) : 'full';
			$sidebar = (function_exists('ot_get_option'))? ot_get_option( 'shop_single_sidebar', 'sidebar-1' ) : 'sidebar-1';
			$bg_class = 'bgwhite';
		}
		elseif( is_woocommerce() ){
			$layout = (function_exists('ot_get_option'))? ot_get_option( 'shop_layout', 'rs' ) : 'rs';
			$sidebar = (function_exists('ot_get_option'))? ot_get_option( 'shop_sidebar', 'sidebar-1' ) : 'sidebar-1';
			$col_class = 'col-lg-9 col-md-8 col-12';
			$bg_class = 'bgwhite';
		}
	}
	
	if(is_post_type_archive('tribe_events')){
		$layout = (function_exists('ot_get_option'))? ot_get_option( 'event_layout', 'full' ) : 'full';
	}
	
	if ( !is_active_sidebar( $sidebar ) ){
		$layout = 'full';
	}
	
	if( $layout == 'full' ){
		$container_class = 'col-md-12 col-lg-12 col-12';
	}
	else{
		$container_class = $col_class;
	}	
	
?>
<?php 
$content_padding_type = get_post_meta(get_the_ID(), 'content_padding_type', true);
$class_pad = '';
if($content_padding_type == 'no_padding'):
	$class_pad = ' no-padding';
else:
	$class_pad = ' with-padding';
endif;

$section_classes = 'section-main-container section '.$bg_class.$class_pad;
?>
        
<?php if( $layout == 'ls' ){?>
	<section class="<?php echo esc_attr($section_classes); ?>">
    	<div class="container">
        	<div class="row">
            	<?php if( $layout != 'full' ): ?>    
				<?php get_sidebar(); ?>                
                <?php endif; // endif ?>
                
            	<div class="<?php echo esc_attr($container_class); ?>">
                	<div class="content">
<?php } else { ?>
<section class="<?php echo esc_attr($section_classes); ?>">
    <div class="container">    
        <div class="row">    
            <div class="<?php echo esc_attr($container_class); ?>">
                <div class="content">
<?php } ?>
