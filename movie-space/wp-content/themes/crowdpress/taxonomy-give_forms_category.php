<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, crowdpress already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage crowdpress
 * @since crowdpress 1.0.0
 */


get_header(); ?>

	<?php get_template_part( 'layout/before', '' ); ?>
		<?php
			if ( have_posts() ) :
			?>
            <div class="projects-content">
                <div class="row justify-content-center">
                
            <?php
				
				while ( have_posts() ) : the_post();

					/*
					 * Include the post format-specific template for the content. If you want to
					 * use this in a child theme, then include a file called called content-___.php
					 * (where ___ is the post format) and that will be used instead.
					 */
					?>
                    <?php
					$no_border_class = '';
					$form        = new Give_Donate_Form(get_the_ID());
					$goal        = $form->goal;
					$income      = $form->get_earnings();
		
					if ($income && $goal) {
						$progress = round( ( $income / $goal ) * 100 );
					} else {
						$progress = '';
					}
					if ( $income >= $goal ) {
					  $progress = 100;
					}
		
					$income = give_human_format_large_amount( give_format_amount( $income ) );
					$goal = give_human_format_large_amount( give_format_amount( $goal ) );
					
					if(function_exists('give_get_payments')) {
					$args = array(
					'give_forms' => array( get_the_ID() ),
					);
					$donations = give_get_payments( $args );
					$donor_count = count($donations);
					} else {
					$donor_count = 0;
					}
					?>
                    <?php if(!has_post_thumbnail()){
						$no_border_class = ' no-thumb-des';
					} ?>
                    
                    <div class="col-md-6 col-lg-4 projects-loop">
                    	<?php if(has_post_thumbnail()): ?>
                        <div class="blog-media">
                            <a href="<?php esc_url(the_permalink()); ?>">
                            <?php
                            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
							$img_src = crowdpress_image_resize($featured_img_url , 740, 520);
							?>
                            <img src="<?php echo esc_url($img_src); ?>" alt="<?php echo esc_attr(get_the_title()); ?>" />
                            </a>
                            <div class="cat-readmore-con text-right">
                                <?php
								$cat_count = 1;
								$terms = get_the_terms( get_the_ID(), 'give_forms_category' );						 
                                foreach($terms as $term){
                                    ?>
                                    <a href="<?php echo esc_url( get_term_link( $term->term_id ) ) ?>"><?php echo esc_html($term->name); ?></a>
                                    <?php
									if($cat_count == 1){
										break;
									}
									$cat_count++;
                                }
                                ?>
                            </div>                        
                    	</div>
                        <?php endif; ?>
                        
                        <div class="rel-blog-desc<?php echo esc_attr($no_border_class); ?>">
                        	
                        
                            <h4 class="blog-des-title"><a href="<?php esc_url(the_permalink()); ?>"><?php echo get_the_title(); ?></a></h4>
                            <div class="progress-bar-details">
                            	<div class="project-cause-bar"><span class="progress-bar" role="progressbar" aria-valuenow="<?php echo esc_attr($progress); ?>" aria-valuemin="0" aria-valuemax="100"></span></div>
                                
                                    <div class="cause-amounts row">
                                    	<div class="col-6">
                                        	<p class="theme-color"><?php echo esc_html(give_currency_symbol().$income); ?></p>
                                            <p><?php echo esc_html__( 'Raised of', 'crowdpress' ); ?> <?php echo esc_html(give_currency_symbol().$goal); ?></p>
                                        </div>
                                        <div class="col-6">
                                        	<p class="theme-color"><?php echo esc_html($donor_count); ?>+</p>
		  									<p><?php echo esc_html__( 'Backers We Got', 'crowdpress' ); ?></p>
                                        </div>
                                    </div>
                            </div>
                            
                            <div class="blog-meta">
                            	<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>"><i class="far fa-user"></i><?php echo esc_html__( 'By ', 'crowdpress' ); ?><?php echo get_the_author_meta('display_name'); ?></a>
                                <a href="<?php esc_url(the_permalink()); ?>"><i class="far fa-calendar-alt"></i><?php echo get_the_date(); ?></a>
                                <a href="<?php esc_url(the_permalink()); ?>"><i class="far fa-heart"></i></a>
                            </div>
                        </div>
                                          
                    </div>
                    <?php

				endwhile;
				?>
                </div>     
			</div>
                <?php
				
				crowdpress_posts_nav();

			else :
				// If no content, include the "No posts found" template.
				get_template_part( 'content', 'none' );

			endif;
		?>       
            
	<?php get_template_part( 'layout/after', '' ); ?>
    
<?php get_footer(); ?>