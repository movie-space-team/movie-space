<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 */
?>
	<?php
	$page_footer_style = get_post_meta(get_the_ID(), 'page_footer_style', true);
	$footer_styles = (function_exists('ot_get_option'))? ot_get_option('footer_style', 'style1') : 'style1';
	if($page_footer_style != ''){
		$footer_style = $page_footer_style;
	} else {
		$footer_style = $footer_styles; 
	}
    $footer_widget_area = (function_exists('ot_get_option'))? ot_get_option('footer_widget_area', 'on') : 'on';
	
	if($footer_widget_area == 'on'):
		if(is_active_sidebar( 'footer-1' ) || is_active_sidebar( 'footer-2' ) || is_active_sidebar( 'footer-3' ) || is_active_sidebar( 'footer-4' )): ?>
		<div class="footer section footer-<?php echo esc_attr($footer_style); ?>">
        	<div class="container">
			<?php get_template_part('footer/footer-widget-area', ''); ?>
            </div>
		</div>
	<?php endif;
	endif; ?>
    
    <div class="copyrights copyright-<?php echo esc_attr($footer_style); ?>">
        <div class="container">
        	<div class="row align-items-center">
            	<div class="col-md-6">
                	<?php
					$copyright_text = (function_exists('ot_get_option'))? ot_get_option( 'copyright_text', sprintf(esc_html__('%1$s &copy; Copyright, CrowdPress. All rights reserved.', 'crowdpress'), date('Y'))) : sprintf(esc_html__('%1$s &copy; Copyright, CrowdPress. All rights reserved.', 'crowdpress'), date('Y'));
					echo wp_kses(do_shortcode($copyright_text), array('div'=>array('class'=>array(), 'id'=>array()), 'a'=>array('class'=>array(), 'title'=>array(), 'href'=>array()), 'p'=>array('class'=>array())));
					?>
                </div>
                <div class="col-md-6">
                	<?php											
					wp_nav_menu(array(
					'theme_location'  => 'footer-menu',
					'menu_class'      => 'list-inline text-right footer-menu',
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'fallback_cb'     => '',
					'container'       => '',
					'depth'			  => 1
					));
					?>
                </div>
            </div>            
        </div><!-- end container -->
        <div class="dmtop"><a href="#"><i class="fas fa-long-arrow-alt-up"></i></a></div>
    </div>
</div>
<!-- Wrapper Ends -->
  <?php wp_footer(); ?>
</body>
</html>