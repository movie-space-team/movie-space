<?php if(!is_single()): ?>
<?php
$categories = '';
$categories2 = '';
$portfolio_category = get_the_terms( get_the_ID(), 'portfolio_category' );
if ( $portfolio_category && ! is_wp_error( $portfolio_category ) ) : 
	$category_class = array();
	foreach ( $portfolio_category as $term1 ) {
	$category_class[] = $term1->slug;
	}							
$categories = join( " ", $category_class );
$categories2 = join( ", ", $category_class );	
endif;
?>

<?php $fullsize = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' ); ?>
<div class="cbp-item <?php echo esc_attr($categories); ?>">
    <a href="<?php echo esc_url($fullsize[0]); ?>" class="cbp-caption cbp-lightbox" data-title="<?php the_title(); ?>">
    <div class="cbp-caption-defaultWrap">
        <?php crowdpress_post_thumb(1200, 800); ?>
    </div>
    <div class="cbp-caption-activeWrap">
      <div class="cbp-l-caption-alignLeft">
        <div class="cbp-l-caption-body">
          <div class="cbp-l-caption-title"><?php the_title(); ?></div>
          <div class="cbp-l-caption-desc">
          <?php echo esc_html($categories2); ?>
          </div>
        </div>
      </div>
    </div>
    </a>
</div>
<?php else: ?>
<div id="portfolio-<?php the_ID(); ?>" <?php post_class() ?>>
    <div class="single-portfolio-content">
        <?php the_content(); ?>
    </div>    
</div>
<?php endif; ?>