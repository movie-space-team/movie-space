<?php
function crowdpress_import_files() {
    return array(
        array(
            'import_file_name'           => esc_html__('Home 1', 'crowdpress'),
            'categories'                 => array('Home 1'),
            'import_file_url'            => CROWDPRESSTHEMEURI.'/admin/assets/demo-content/demo-content.xml',
            'import_widget_file_url'     => CROWDPRESSTHEMEURI.'/admin/assets/demo-content/widgets.wie',
            'import_preview_image_url'   => CROWDPRESSTHEMEURI.'/admin/assets/demo-content/demo_01.jpg',
            'import_notice'              => esc_html__( 'After importing this demo, you will have to setup the slider separately.', 'crowdpress' ),
            'preview_url'                => 'http://theme-stall.com/crowdpress/demos',
        ),
        array(
            'import_file_name'           => esc_html__('Home 2', 'crowdpress'),
            'categories'                 => array('Home 2' ),
            'import_file_url'            => CROWDPRESSTHEMEURI.'/admin/assets/demo-content/demo-content2.xml',
            'import_widget_file_url'     => CROWDPRESSTHEMEURI.'/admin/assets/demo-content/widgets_2.wie',
            'import_preview_image_url'   => CROWDPRESSTHEMEURI.'/admin/assets/demo-content/demo_02.jpg',
            'import_notice'              => esc_html__( 'After importing demo, you will have to setup the slider separately.', 'crowdpress' ),
            'preview_url'                => 'http://theme-stall.com/crowdpress/demos/demo2/',
        ),
		array(
            'import_file_name'           => esc_html__('Home 3', 'crowdpress'),
            'categories'                 => array('Home 3' ),
            'import_file_url'            => CROWDPRESSTHEMEURI.'/admin/assets/demo-content/demo-content3.xml',
            'import_widget_file_url'     => CROWDPRESSTHEMEURI.'/admin/assets/demo-content/widgets_3.wie',
            'import_preview_image_url'   => CROWDPRESSTHEMEURI.'/admin/assets/demo-content/demo_03.jpg',
            'import_notice'              => esc_html__( 'After importing this demo, you will have to setup header top menu separately.', 'crowdpress' ),
            'preview_url'                => 'http://theme-stall.com/crowdpress/demos/demo3/',
        ),
    );
}
add_filter( 'pt-ocdi/import_files', 'crowdpress_import_files' );

function crowdpress_after_import_setup() {
    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Header Menu', 'nav_menu' );
	$footer_menu = get_term_by( 'name', 'Footer Menu', 'nav_menu' );
	

    set_theme_mod( 'nav_menu_locations', array(
            'main-menu' => $main_menu->term_id,
			'footer-menu' => $footer_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Home' );
    $blog_page_id  = get_page_by_title( 'Blog' );

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
    update_option( 'page_for_posts', $blog_page_id->ID );

}
add_action( 'pt-ocdi/after_import', 'crowdpress_after_import_setup' );