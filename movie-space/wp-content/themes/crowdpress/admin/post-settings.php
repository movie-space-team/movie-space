<?php
/**
 * Initialize the meta boxes. 
 */

add_action( 'admin_init', 'crowdpress_post_meta_boxes' );

function crowdpress_post_meta_boxes() {
  if( function_exists( 'ot_get_option' ) ): 
  $my_meta_box = array(
    'id'        => 'crowdpress_post_meta_box',
    'title'     => esc_html__('Crowdpress Post Settings', 'crowdpress'),
    'desc'      => '',
    'pages'     => array( 'post' ),
    'context'   => 'normal',
    'priority'  => 'high',
    'fields'    => array(
	  array(
        'id'          => 'header_settings',
        'label'       => esc_html__('Content Settings', 'crowdpress'),      
        'type'        => 'tab',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'video_link',
        'label'       => esc_html__('Video Link', 'crowdpress'),
        'desc'        => esc_html__('Only Work for post formate video', 'crowdpress'),
        'std'         => '',
        'type'        => 'text',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
      ),
	  array(
        'id'          => 'audio_link',
        'label'       => esc_html__('Audio Link', 'crowdpress'),
        'desc'        => esc_html__('Only Work for post formate audio', 'crowdpress'),
        'std'         => '',
        'type'        => 'text',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
      ),
	  array(
        'id'          => 'image_gallery',
        'label'       => esc_html__('Gallery', 'crowdpress'),
        'desc'        => esc_html__('Only Work for post formate gallery', 'crowdpress'),
        'std'         => '',
        'type'      => 'gallery',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
      ),
	  array(
        'id'          => 'quote_text',
        'label'       => esc_html__('Quote', 'crowdpress'),
        'desc'        => esc_html__('Only Work for post formate quote', 'crowdpress'),
        'std'         => '',
        'type'        => 'textarea',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
      ),
	  array(
        'id'          => 'quote_text_url',
        'label'       => esc_html__('Quote URL', 'crowdpress'),
        'desc'        => esc_html__('Only Work for post formate quote', 'crowdpress'),
        'std'         => '',
        'type'        => 'text',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
      ),
	  array(
        'id'          => 'quote_cite',
        'label'       => esc_html__('Quote Cite', 'crowdpress'),
        'desc'        => esc_html__('Only Work for post formate quote', 'crowdpress'),
        'std'         => '',
        'type'        => 'text',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
      ),
    )
  );
  
  ot_register_meta_box( $my_meta_box );
  endif;
}
?>