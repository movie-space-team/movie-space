<?php
// Register Style
function crowdpress_admin_styles() {

	wp_register_style( 'crowdpress-style', CROWDPRESSTHEMEURI. 'admin/assets/css/style.css', false, '1.0.0', 'all' );
	wp_enqueue_style( 'crowdpress-style' );

}

// Hook into the 'admin_enqueue_scripts' action
add_action( 'admin_enqueue_scripts', 'crowdpress_admin_styles' );