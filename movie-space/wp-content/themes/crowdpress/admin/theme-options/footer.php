<?php
function crowdpress_footer_options( $options = array() ){
	$social_array = array(						
			array(
			  'title' => esc_html__( 'FB', 'crowdpress' ),
			  'link'  => '#',
			  ),
			array(
			  'title' => esc_html__( 'TW', 'crowdpress' ),
			  'link'  => '#',
			  ),
			  array(
			  'title' => esc_html__( 'DR', 'crowdpress' ),
			  'link'  => '#',
			  ),
			  array(
			  'title' => esc_html__( 'BE', 'crowdpress' ),
			  'link'  => '#',
			  ),
			   array(
			  'title' => esc_html__( 'YU', 'crowdpress' ),
			  'link'  => '#',
			  ),						
			);	
	$options = array(
		array(
        'id'          => 'footer_style',
        'label'       => esc_html__( 'Footer Style', 'crowdpress' ),
        'desc'        => esc_html__( 'Select footer style', 'crowdpress' ),
        'std'         => 'style1',
        'type'        => 'select',
        'section'     => 'footer_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
		  array(
            'value'       => 'style1',
            'label'       => esc_html__( 'Style 1', 'crowdpress' ),
          ),
          array(
            'value'       => 'style2',
            'label'       => esc_html__( 'Style 2', 'crowdpress' ),
          ),
		  array(
            'value'       => 'style3',
            'label'       => esc_html__( 'Style 3', 'crowdpress' ),
          )
        )
      ),
      array(
        'id'          => 'footer_widget_area',
        'label'       => esc_html__( 'Footer Widget Area', 'crowdpress' ),
        'desc'        => '',
        'std'         => 'on',
        'type'        => 'on-off',
        'section'     => 'footer_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'footer_widget_box',
        'label'       => esc_html__( 'Footer Widget Box', 'crowdpress' ),
        'desc'        => '',
        'std'         => '3',
        'type'        => 'numeric-slider',
        'section'     => 'footer_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '1,4,1',
        'class'       => '',
        'condition'   => 'footer_widget_area:not(off)',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'footer_social_icons',
        'label'       => esc_html__( 'Footer Social Icons', 'crowdpress' ),
        'desc'        => '',
        'std'         => $social_array,
        'type'        => 'list-item',
        'section'     => 'footer_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'settings'    => array( 
          array(
            'id'          => 'link',
            'label'       => esc_html__( 'Link', 'crowdpress' ),
            'desc'        => '',
            'std'         => '',
            'type'        => 'text',
            'rows'        => '',
            'post_type'   => '',
            'taxonomy'    => '',
            'min_max_step'=> '',
            'class'       => '',
            'condition'   => '',
            'operator'    => 'and'
          ),
        ),
      ),
	  array(
        'id'          => 'copyright_text',
        'label'       => esc_html__( 'Copyright Text', 'crowdpress' ),
        'desc'        => '',
        'std'         => sprintf(esc_html__('%1$s &copy; Copyright, CrowdPress. All rights reserved.', 'crowdpress'), date('Y')),
        'type'        => 'textarea',
        'section'     => 'footer_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'footer_description_text',
        'label'       => esc_html__( 'Footer Description Text', 'crowdpress' ),
        'desc'        => '',
        'std'         => '',
        'type'        => 'textarea',
        'section'     => 'footer_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'footer_style:is(style2)',
        'operator'    => 'and'
      ),
    );

	return apply_filters( 'crowdpress_footer_options', $options );
}  
?>