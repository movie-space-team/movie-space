<?php
function crowdpress_general_options( $options = array() ){
	
	$options = array(
	  array(
        'id'          => 'main_logo',
        'label'       => esc_html__( 'Header Logo', 'crowdpress' ),
        'desc'        => esc_html__( 'Header logo', 'crowdpress' ),
        'std'         => CROWDPRESSTHEMEURI . 'images/logo.png',
        'type'        => 'upload',
        'section'     => 'general_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'footer_logo',
        'label'       => esc_html__( 'Footer Logo', 'crowdpress' ),
        'desc'        => esc_html__( 'Footer logo', 'crowdpress' ),
        'std'         => CROWDPRESSTHEMEURI . 'images/footer-logo.png',
        'type'        => 'upload',
        'section'     => 'general_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'mobile_logo',
        'label'       => esc_html__( 'Responsive Logo', 'crowdpress' ),
        'desc'        => esc_html__( 'Logo for responsive devices( mobile, tab etc )', 'crowdpress' ),
        'std'         => CROWDPRESSTHEMEURI . 'images/logo-light.png',
        'type'        => 'upload',
        'section'     => 'general_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'preloader',
        'label'       => esc_html__( 'Display Preloader', 'crowdpress' ),
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'general_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'global_style',
        'label'       => esc_html__( 'Global Button Style', 'crowdpress' ),
        'desc'        => esc_html__( 'Select global button style', 'crowdpress' ),
        'std'         => 'button-square',
        'type'        => 'select',
        'section'     => 'general_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
		  array(
            'value'       => 'button-square',
            'label'       => esc_html__( 'Square', 'crowdpress' ),
          ),
          array(
            'value'       => 'button-circle',
            'label'       => esc_html__( 'Circle', 'crowdpress' ),
          )
        )
      ),
	);

	return apply_filters( 'crowdpress_general_options', $options );
}
?>