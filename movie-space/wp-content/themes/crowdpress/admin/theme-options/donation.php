<?php
function crowdpress_donation_options( $options = array() ){
	$options = array(
	array(
        'id'          => 'give_title',
        'label'       => esc_html__( 'Donation Header Title', 'crowdpress' ),
        'desc'        => esc_html__( 'Donation page header title', 'crowdpress' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'donation_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'give_subtitle',
        'label'       => esc_html__( 'Donation Header Sub-Title', 'crowdpress' ),
        'desc'        => esc_html__( 'Donation page header sub-title', 'crowdpress' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'donation_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'give_single_title',
        'label'       => esc_html__( 'Donation Single Header Title', 'crowdpress' ),
        'desc'        => esc_html__( 'Donation single header title', 'crowdpress' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'donation_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'give_single_subtitle',
        'label'       => esc_html__( 'Donation Single Header Sub-Title', 'crowdpress' ),
        'desc'        => esc_html__( 'Donation single header sub-title', 'crowdpress' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'donation_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'donation_header_banner_type',
        'label'       => esc_html__( 'Header Banner Type', 'crowdpress' ),
        'desc'        => esc_html__( 'Select your header banner type', 'crowdpress' ),
        'std'         => 'donation_custom_image',
        'type'        => 'select',
        'section'     => 'donation_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'donation_bg_color',
            'label'       => esc_html__( 'Background Color', 'crowdpress' ),
          ),
		  array(
            'value'       => 'donation_custom_image',
            'label'       => esc_html__( 'Custom Image', 'crowdpress' ),
          )
        )
      ),
	  array(
        'id'          => 'donation_header_custom_color',
        'label'       => esc_html__('Select Color', 'crowdpress'),
        'desc'        => '',
        'std'         => '#001d23',
        'type'        => 'colorpicker',
		'section'     => 'donation_options',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
        'condition'   => 'donation_header_banner_type:is(donation_bg_color)',
      ),
	  array(
        'id'          => 'donation_default_banner_image',
        'label'       => esc_html__( 'Background Image', 'crowdpress' ),
        'desc'        => esc_html__( 'Background image', 'crowdpress' ),
        'std'         => CROWDPRESSTHEMEURI. 'images/banner.jpg',
        'type'        => 'upload',
        'section'     => 'donation_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'donation_header_banner_type:is(donation_custom_image)',
        'operator'    => 'and'
      ),
    );

	return apply_filters( 'crowdpress_donation_options', $options );
}  
?>