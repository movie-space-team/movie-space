<?php
function crowdpress_styling_options( $options = array() ){
	$options = array(
		array(
        'id'          => 'preset_color',
        'label'       => esc_html__( 'Preset Color', 'crowdpress' ),
        'desc'        => esc_html__( 'Main preset color', 'crowdpress' ),
        'std'         => '#009e74',
        'type'        => 'colorpicker',
        'section'     => 'styling_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'action'      => array(
                array(
                'selector' => '.team-desc small,.show > .btn-primary.dropdown-toggle,.show > .btn-primary.dropdown-toggle,.process-box:hover .process-end, 
.normal-box h4::after,.image-box h4::after,.widget-title:after,.onsale,.image-box h4::after,.tooltip-inner,.post-publish .fa:hover,.da-thumbs.classic-portfolio .pitem a div i,.page-link,
mark,.bg,.tagcloud a:hover,.blog-tags li a:hover,
.tagwidget li a:hover,.wp-block-quote,#loader-wrapper .loader-section,.social a:hover,.social a:focus,.cbp-l-filters-button .cbp-filter-item:hover,.cbp-l-filters-button .cbp-filter-item.cbp-filter-item-active,.cbp-l-filters-button .cbp-filter-counter,.cbp-l-caption-alignLeft .cbp-l-caption-body > a,.widget-title:before,.dmtop,.btn-primary,.team-name span,.team-social-icons li a,.search-form .search-submit,.woocommerce-product-search input[type="submit"],.video-popup,.services-content.style1:hover .number,.testi-item:hover .testi-name h5 .svg-inline--fa,.testimonials-content.owl-carousel button.owl-dot.active,.listicon-content p span,.project-cause-bar .progress-bar,.give-btn:hover,
.give-btn:focus,.give-single-form-sidebar-left .give-btn,.cat-readmore-con a,.posts-loop:hover .cat-readmore-con .read-more-link,.portfolio-next-prev .absolute-pager.text-right,.cat-title,.pagination li a:hover,.pagination li a:focus,.pagination li.current a,.sidebar .widget_categories li:hover:before,.woocommerce nav.woocommerce-pagination ul li a:focus,.woocommerce nav.woocommerce-pagination ul li a:hover,
.woocommerce nav.woocommerce-pagination ul li span.current,.woocommerce #respond input#submit,.woocommerce a.button,.woocommerce button.button,
.woocommerce input.button,.woocommerce #respond input#submit.alt,.woocommerce a.button.alt,.woocommerce button.button.alt,.woocommerce input.button.alt,.woocommerce span.onsale,.woocommerce #respond input#submit.alt.disabled,.woocommerce #respond input#submit.alt.disabled:hover,
.woocommerce #respond input#submit.alt:disabled,.woocommerce #respond input#submit.alt:disabled:hover,.woocommerce #respond input#submit.alt:disabled[disabled],.woocommerce #respond input#submit.alt:disabled[disabled]:hover,.woocommerce a.button.alt.disabled,
.woocommerce a.button.alt.disabled:hover,.woocommerce a.button.alt:disabled,.woocommerce a.button.alt:disabled:hover,.woocommerce a.button.alt:disabled[disabled],.woocommerce a.button.alt:disabled[disabled]:hover,.woocommerce button.button.alt.disabled,
.woocommerce button.button.alt.disabled:hover,.woocommerce button.button.alt:disabled,.woocommerce button.button.alt:disabled:hover,
.woocommerce button.button.alt:disabled[disabled],.woocommerce button.button.alt:disabled[disabled]:hover,
.woocommerce input.button.alt.disabled,.woocommerce input.button.alt.disabled:hover,.woocommerce input.button.alt:disabled,
.woocommerce input.button.alt:disabled:hover,.woocommerce input.button.alt:disabled[disabled],.woocommerce input.button.alt:disabled[disabled]:hover,.woocommerce .widget_price_filter .ui-slider .ui-slider-handle,.woocommerce .widget_price_filter .ui-slider .ui-slider-range,.elementor-widget-wp-widget-give_forms_widget .give-form-type-multi > .give-btn,.give-submit-button-wrap .give-submit.give-btn,.events-loop .buy-tickets-btn:hover, .events-loop .buy-tickets-btn:focus,.sidebar .crowdpress-categories li a:hover, .sidebar .crowdpress-categories li a:focus,.sidebar .crowdpress-categories li span',
                'property'   => 'background-color'
                ),
                array(
                'selector' => 'a:hover,a:focus,.profile-menu li a:hover,.profile-menu li a:focus,.navbar-nav .menu-item.current-menu-item > a,.navbar-nav .menu-item.current_page_parent > a,.navbar-nav .menu-item:hover > a,.footer .social a:hover,
.footer .social a:focus,.breadcrumb li.active,.footer-widget-area ul li a:hover,.footer-widget-area ul li a:focus,.footer .footer-contact li .svg-inline--fa,#ctf .ctf-corner-logo .svg-inline--fa,.footer .social a:hover,.footer .social a:focus,.read-more-wrapper:hover i,.team-name p,.comment-form-fields .form-group:before,.wpcf7 i,.top-search-submit,.contact-icon,.comment-edit-link:hover,
.comment-reply-link:hover,.comment-edit-link:focus,.comment-reply-link:focus,.authorbox h4 span,.heading-content .sub-title,.services-content.style3 h3 span,.services-content .number,.services-content.style2 a,.testi-name p,.testi-name h5 .svg-inline--fa,.cause-amounts p.theme-color,.give-donor__total,.posts-content .blog-meta .svg-inline--fa,.center-icons,.text-left-previous span,.text-right-next span,.aut-dat-box span,.project-meta-content .port-cat,.projects-content .blog-meta .svg-inline--fa,.socibox:hover,.socibox:focus,.user-social a:hover,
.user-social a:focus,.widget_product_tag_cloud .tagcloud a:hover,.widget_product_tag_cloud .tagcloud a:focus,.woocommerce .woocommerce-ordering:after,.woocommerce .star-rating span::before,.woocommerce div.product p.price,.woocommerce div.product span.price,.product_meta,.woocommerce div.product .woocommerce-tabs ul.tabs li.active a,.woocommerce-message::before,.woocommerce-info::before,.shop-title span,.elementor-widget-wp-widget-give_forms_widget h5:before,.buy-tickets-btn i,.product_meta a,.summary.entry-summary .social a:hover,.summary.entry-summary .social a:focus,.navbar ul li .dropdown-menu li:hover > a,.crowdpress-donation-form-widget p.theme-color,.cause-amounts p.theme-color, .cause-amounts span.theme-color,.events-loop .buy-tickets-btn i,.posts-content .blog-read-more,.footer-widget-area .blog-list-widget .w-100:hover h5,.blog-list-widget .w-100:hover h5, .blog-list-widget .w-100:focus h5,.blog-meta .svg-inline--fa,.footer-widget-area .blog-list-widget .w-100:focus h5',
                'property'   => 'color'
                ),
                array(
                    'selector' => '.contact-box-content:hover:before,.contact-box-content:hover:after,.cat-lists li:nth-child(2),.partner-content a,.give-default-level.give-btn,.give-btn:hover,.give-btn:focus,.give-single-form-sidebar-left .give-btn,.elementor-widget-wp-widget-give_forms_widget .give-form-type-multi > .give-btn,.give-submit-button-wrap .give-submit.give-btn,.events-loop .buy-tickets-btn:hover, .events-loop .buy-tickets-btn:focus,.pagination li a:hover, .pagination li a:focus, .pagination li.current a,.woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current',
                    'property'   => 'border-color'
                ),
				array(
                    'selector' => '.tooltip.tooltip-top .tooltip-inner::before,.tooltip.bs-tether-element-attached-bottom .tooltip-inner::before,.cbp-l-filters-button .cbp-filter-counter::after,.woocommerce-message,.woocommerce-info',
                    'property'   => 'border-top-color'
                ),
				array(
                    'selector' => '.tooltip.tooltip-bottom .tooltip-inner::before,.tooltip.bs-tether-element-attached-top .tooltip-inner::before,.heading-content .sub-title:before,.heading-content .sub-title:after,.services-content.style3:hover,.woocommerce div.product .woocommerce-tabs ul.tabs li.active',
                    'property'   => 'border-bottom-color'
                ),
				array(
                    'selector' => '.heading-content p:before',
                    'property'   => 'border-left-color'
                ),
				array(
                    'selector' => '',
                    'property'   => 'text-decoration-color'
                ),
            )
      ),
	  
	  array(
        'id'          => 'preset_color_2',
        'label'       => esc_html__( 'Secondary Preset Color', 'crowdpress' ),
        'desc'        => esc_html__( 'Secondary preset color', 'crowdpress' ),
        'std'         => '#001d23',
        'type'        => 'colorpicker',
        'section'     => 'styling_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'action'      => array(
                array(
                'selector' => '.collapse-button .icon-bar,.wp-block-button__link,.bgdark,.portfolio-filter ul li,.cbp-l-filters-button .cbp-filter-item,.cbp-l-caption-alignLeft .cbp-l-caption-body > a:hover,.cbp-l-filters-button,.light-box-img span,.sidebar .widget:hover .widget-title:before,.dmtop:hover,.dmtop:hover i,.process-box,.btn-primary:hover,.btn-primary:focus,.team-social-icons li a:hover,.posts-loop:hover .cat-readmore-con a,.page .tnp-widget-minimal input.tnp-submit,.portfolio-next-prev .absolute-pager,.cat-title:hover,.cat-title:focus,.page-link:hover,.page-link:focus,.woocommerce #respond input#submit.alt:hover,.woocommerce a.button.alt:hover,.woocommerce button.button.alt:hover,.woocommerce input.button.alt:hover,.woocommerce #respond input#submit:hover,.woocommerce a.button:hover,.woocommerce button.button:hover,.woocommerce input.button:hover,.woocommerce #respond input#submit.disabled:hover,.woocommerce #respond input#submit:disabled:hover,.woocommerce #respond input#submit:disabled[disabled]:hover,
.woocommerce a.button.disabled:hover,.woocommerce a.button:disabled:hover,.woocommerce a.button:disabled[disabled]:hover,.woocommerce button.button.disabled:hover,.woocommerce button.button:disabled:hover,.woocommerce button.button:disabled[disabled]:hover,
.woocommerce input.button.disabled:hover,.woocommerce input.button:disabled:hover,.woocommerce input.button:disabled[disabled]:hover,.woocommerce a.added_to_cart,.woocommerce .widget_price_filter .price_slider_wrapper .ui-widget-content,.projects-content .cat-readmore-con a:hover,
.projects-content .cat-readmore-con a:focus',
                'property'   => 'background-color'
                ),
                array(
                'selector' => 'a,.wp-block-quote footer,.wp-block-quote .wp-block-quote__citation,.slim-wrap ul.menu-items.collapsed li a,.slim-wrap ul.menu-items li .sub-collapser > i,h1,h2,h3,h4,h5,h6,.readmore,.sticky-post,.tagcloud a,.blog-tags li a,
.tagwidget li a,.profile-menu li a,.navbar-nav .menu-item a,.social li a,.social a,.cbp-l-caption-title,.cbp-item-margin small,.widget-title,.footer-widget-area ul li a,.process-end h3,.process-end p,.normal-box h4,.more-link,.comment-form-fields .form-control,.search-form .search-field,.woocommerce-product-search .search-field,.wpcf7 .form-control,.gform .form-control:focus,.search-form .search-field:focus,
.woocommerce-product-search .search-field:focus,.wpcf7 .form-control:focus,.search-form .search-submit:hover,.woocommerce-product-search input[type="submit"]:hover,.search-form .search-submit:focus,.woocommerce-product-search input[type="submit"]:focus,.contact-icon span,.comment-edit-link,
.comment-reply-link,.heading-content h2,.give-btn,.cat-readmore-con .read-more-link,.sidebar .widget-title,.widget-title.dark-color,.sidewrapper .widget-title,.woocommerce .star-rating::before,.woocommerce nav.woocommerce-pagination ul li span,.woocommerce div.product .woocommerce-tabs ul.tabs li a,.woocommerce div.product .stock,.form-control,.woocommerce-Input.woocommerce-Input--text.input-text,.woocommerce-MyAccount-navigation ul li a,.woocommerce .widget_price_filter .price_slider_amount .button,.text-left-previous,.text-right-next',
                'property'   => 'color'
                ),
                array(
                    'selector' => '.wp-block-button__link,.wp-block-quote:not(.is-large):not(.is-style-large),.woocommerce #review_form #respond textarea:focus,.woocommerce #review_form #respond input[type="text"]:focus,.woocommerce #review_form #respond input[type="password"]:focus,
.woocommerce #review_form #respond input[type="email"]:focus',
                    'property'   => 'border-color'
                ),
				array(
                    'selector' => '',
                    'property'   => 'border-top-color'
                ),
				array(
                    'selector' => '.breadcrumb li span.delimiter',
                    'property'   => 'border-left-color'
                ),
				array(
                    'selector' => '',
                    'property'   => 'border-bottom-color'
                ),
				array(
                    'selector' => '.cbp-item-margin .readmore:hover',
                    'property'   => 'text-decoration-color' 
                ),
            )
      ),
	  array(
        'id'          => 'preset_color_3',
        'label'       => esc_html__( 'Global Button Color', 'crowdpress' ),
        'desc'        => esc_html__( 'Global button color', 'crowdpress' ),
        'std'         => '#fff',
        'type'        => 'colorpicker',
        'section'     => 'styling_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'action'      => array(
                array(
                'selector' => '',
                'property'   => 'background-color'
                ),
                array(
                'selector' => '.btn-primary,.give-submit-button-wrap .give-submit.give-btn,.comment-respond .form-submit:before, .give-submit-button-wrap:before, .dmtop a,.give-btn:hover, .give-btn:focus,.projects-content .cat-readmore-con a,.events-loop .buy-tickets-btn:hover, .events-loop .buy-tickets-btn:focus,.events-loop .buy-tickets-btn:hover i, .events-loop .buy-tickets-btn:focus i,.team-name span,.team-social-icons li a,.social a:hover, .social a:focus,.cat-title,.cat-title a,.search-form:after,.wp-block-quote p,.wp-block-quote cite, .wp-block-quote footer, .wp-block-quote__citation, .wp-block-quote cite a,.sidebar .crowdpress-categories li span,.sidebar .crowdpress-categories li a:hover, .sidebar .crowdpress-categories li a:focus,.tagcloud a:hover, .blog-tags li a:hover, .tagwidget li a:hover,.blog-media.formate-quote i,.woocommerce nav.woocommerce-pagination ul li a:focus, .woocommerce nav.woocommerce-pagination ul li a:hover, .woocommerce nav.woocommerce-pagination ul li span.current,.woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button, .woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt,.woocommerce #respond input#submit.disabled, .woocommerce #respond input#submit:disabled, .woocommerce #respond input#submit:disabled[disabled], .woocommerce a.button.disabled, .woocommerce a.button:disabled, .woocommerce a.button:disabled[disabled], .woocommerce button.button.disabled, .woocommerce button.button:disabled, .woocommerce button.button:disabled[disabled], .woocommerce input.button.disabled, .woocommerce input.button:disabled, .woocommerce input.button:disabled[disabled],.wp-block-quote a, .wp-block-quote .blog-meta a,.pagination li a:hover, .pagination li a:focus, .pagination li.current a,.video-popup,.wp-block-quote .blog-meta .svg-inline--fa',
                'property'   => 'color'
                ),
            )
      ),
    );

	return apply_filters( 'crowdpress_styling_options', $options );
}  
?>