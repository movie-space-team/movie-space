<?php
function crowdpress_background_options( $options = array() ){
	$options = array(
	  array(
        'id'          => 'background_layout',
        'label'       => esc_html__( 'Background Layout', 'crowdpress' ),
        'desc'        => esc_html__( 'Background Layout', 'crowdpress' ),
        'std'         => 'wide',
        'type'        => 'select',
        'section'     => 'background_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'wide',
            'label'       => esc_html__( 'Wide', 'crowdpress' ),
          ),
          array(
            'value'       => 'boxed',
            'label'       => esc_html__( 'Boxed', 'crowdpress' ),
          )
        )
      ),
	  array(
        'id'          => 'boxed_background_image',
        'label'       => esc_html__( 'Boxed Background Image', 'crowdpress' ),
        'desc'        => esc_html__( 'Boxed background image', 'crowdpress' ),
        'std'         => CROWDPRESSTHEMEURI.'images/wood_03.jpg',
        'type'        => 'upload',
        'section'     => 'background_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'background_layout:is(boxed)',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'body_background',
        'label'       => esc_html__( 'Body background', 'crowdpress' ),
        'desc'        => esc_html__( 'Background can be image or color', 'crowdpress' ),
        'std'         => array(),
        'type'        => 'background',
        'section'     => 'background_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'action'   => array(
                array(
                    'selector' => 'body'
                    )
            )
      ),
      array(
        'id'          => 'main_container_background',
        'label'       => esc_html__( 'Main container background', 'crowdpress' ),
        'desc'        => esc_html__( 'Background can be image or color', 'crowdpress' ),
        'std'         => array(),
        'type'        => 'background',
        'section'     => 'background_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'action'   => array(
                array(
                    'selector' => '.container'
                    )
            )
      ),
	  array(
        'id'          => 'sidebar_background',
        'label'       => esc_html__( 'Sidebar background', 'crowdpress' ),
        'desc'        => esc_html__( 'Sidebar Background', 'crowdpress' ),
        'std'         => array('background-image' => '', 'background-color' => ''),
        'type'        => 'background',
        'section'     => 'background_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'action'   => array(
                array(
                    'selector' => '.sidebar'
                    )
            )
      ),
	  array(
        'id'          => 'footer_widget_background',
        'label'       => esc_html__( 'Footer Widget background', 'crowdpress' ),
        'desc'        => esc_html__( 'Footer Widget background', 'crowdpress' ),
        'std'         => array('background-image' => '', 'background-color' => ''),
        'type'        => 'background',
        'section'     => 'background_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'action'   => array(
                array(
                    'selector' => '.footer.section'
                    )
            )
      ),
	  array(
        'id'          => 'footer_copyright_background',
        'label'       => esc_html__( 'Footer Copyright background', 'crowdpress' ),
        'desc'        => esc_html__( 'Footer Copyright background', 'crowdpress' ),
        'std'         => array('background-image' => '', 'background-color' => ''),
        'type'        => 'background',
        'section'     => 'background_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'action'   => array(
                array(
                    'selector' => '.copyrights'
                    )
            )
      ),    
    );

	return apply_filters( 'crowdpress_background_options', $options );
}  
?>