<?php
function crowdpress_woocommerce_options( $options = array() ){
	$options = array(
	  array(
        'id'          => 'shop_title',
        'label'       => esc_html__( 'Shop Title', 'crowdpress' ),
        'desc'        => esc_html__( 'Shop page title', 'crowdpress' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'woocommerce_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'shop_subtitle',
        'label'       => esc_html__( 'Shop Sub-Title', 'crowdpress' ),
        'desc'        => esc_html__( 'Shop page Sub title', 'crowdpress' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'woocommerce_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'shop_single_title',
        'label'       => esc_html__( 'Shop Single Title', 'crowdpress' ),
        'desc'        => esc_html__( 'Shop single page title', 'crowdpress' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'woocommerce_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'shop_single_subtitle',
        'label'       => esc_html__( 'Shop Single Sub-Title', 'crowdpress' ),
        'desc'        => esc_html__( 'Shop single page Sub title', 'crowdpress' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'woocommerce_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'product_column',
        'label'       => esc_html__( 'Product Column', 'crowdpress' ),
        'desc'        => '',
        'std'         => '3',
        'type'        => 'numeric-slider',
        'section'     => 'woocommerce_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '1,4,1',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'related_product_column',
        'label'       => esc_html__( 'Related Product Column', 'crowdpress' ),
        'desc'        => '',
        'std'         => '4',
        'type'        => 'numeric-slider',
        'section'     => 'woocommerce_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '1,4,1',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'category_product_column',
        'label'       => esc_html__( 'Category Product Column', 'crowdpress' ),
        'desc'        => '',
        'std'         => '3',
        'type'        => 'numeric-slider',
        'section'     => 'woocommerce_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '1,4,1',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'shop_layout',
        'label'       => esc_html__( 'Shop layout', 'crowdpress' ),
        'desc'        => esc_html__( 'Shop layout for blog page', 'crowdpress' ),
        'std'         => 'rs',
        'type'        => 'radio-image',
        'section'     => 'woocommerce_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'full',
            'label'       => esc_html__( 'Full width', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/full-width.png'
          ),
          array(
            'value'       => 'ls',
            'label'       => esc_html__( 'Left sidebar', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/left-sidebar.png'
          ),
          array(
            'value'       => 'rs',
            'label'       => esc_html__( 'Right sidebar', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/right-sidebar.png'
          )
        )
      ),
      array(
        'id'          => 'shop_sidebar',
        'label'       => esc_html__( 'Shop Sidebar', 'crowdpress' ),
        'desc'        => esc_html__( 'Select your shop sidebar', 'crowdpress' ),
        'std'         => 'sidebar-1',
        'type'        => 'sidebar-select',
        'section'     => 'woocommerce_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'shop_layout:not(full)',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'shop_single_layout',
        'label'       => esc_html__( 'Shop single post layout', 'crowdpress' ),
        'desc'        => esc_html__( 'Shop single post layout', 'crowdpress' ),
        'std'         => 'full',
        'type'        => 'radio-image',
        'section'     => 'woocommerce_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'full',
            'label'       => esc_html__( 'Full width', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/full-width.png'
          ),
          array(
            'value'       => 'ls',
            'label'       => esc_html__( 'Left sidebar', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/left-sidebar.png'
          ),
          array(
            'value'       => 'rs',
            'label'       => esc_html__( 'Right sidebar', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/right-sidebar.png'
          )
        )
      ),
    array(
        'id'          => 'shop_single_sidebar',
        'label'       => esc_html__( 'Single shop Sidebar', 'crowdpress' ),
        'desc'        => esc_html__( 'Single shop sidebar', 'crowdpress' ),
        'std'         => 'sidebar-1',
        'type'        => 'sidebar-select',
        'section'     => 'woocommerce_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'shop_single_layout:not(full)',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'woo_header_banner_type',
        'label'       => esc_html__( 'Header Banner Type', 'crowdpress' ),
        'desc'        => esc_html__( 'Select your header banner type', 'crowdpress' ),
        'std'         => 'woo_custom_image',
        'type'        => 'select',
        'section'     => 'woocommerce_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'woo_bg_color',
            'label'       => esc_html__( 'Background Color', 'crowdpress' ),
          ),
		  array(
            'value'       => 'woo_custom_image',
            'label'       => esc_html__( 'Custom Image', 'crowdpress' ),
          ),
        )
      ),
	  array(
        'id'          => 'woo_header_custom_color',
        'label'       => esc_html__('Select Color', 'crowdpress'),
        'desc'        => '',
        'std'         => '#001d23',
        'type'        => 'colorpicker',
		'section'     => 'woocommerce_options',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
        'condition'   => 'woo_header_banner_type:is(woo_bg_color)',
      ),
	  array(
        'id'          => 'woo_default_banner_image',
        'label'       => esc_html__( 'Background Image', 'crowdpress' ),
        'desc'        => esc_html__( 'Background image', 'crowdpress' ),
        'std'         => CROWDPRESSTHEMEURI. 'images/banner.jpg',
        'type'        => 'upload',
        'section'     => 'woocommerce_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'woo_header_banner_type:is(woo_custom_image)',
        'operator'    => 'and'
      ),
    );

	return apply_filters( 'crowdpress_woocommerce_options', $options );
}  
?>