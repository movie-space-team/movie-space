<?php
function crowdpress_blog_options( $options = array() ){
	$options = array(
	array(
        'id'          => 'blog_style',
        'label'       => esc_html__( 'Blog Style', 'crowdpress' ),
        'desc'        => esc_html__( 'Select blog style', 'crowdpress' ),
        'std'         => 'default',
        'type'        => 'select',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
		  array(
            'value'       => 'default',
            'label'       => esc_html__( 'Default View', 'crowdpress' ),
          ),
          array(
            'value'       => 'list_view',
            'label'       => esc_html__( 'List View', 'crowdpress' ),
          ),
		  array(
            'value'       => 'grid_view',
            'label'       => esc_html__( 'Grid View', 'crowdpress' ),
          )
        )
      ),
	array(
        'id'          => 'blog_text_align',
        'label'       => esc_html__( 'Blog Text Align', 'crowdpress' ),
        'desc'        => esc_html__( 'Select blog text align', 'crowdpress' ),
        'std'         => '',
        'type'        => 'select',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
		  array(
            'value'       => 'left',
            'label'       => esc_html__( 'Default', 'crowdpress' ),
          ),
          array(
            'value'       => 'center',
            'label'       => esc_html__( 'Center', 'crowdpress' ),
          ),
		  array(
            'value'       => 'right',
            'label'       => esc_html__( 'Right', 'crowdpress' ),
          )
        )
      ),
	array(
        'id'          => 'blog_title',
        'label'       => esc_html__( 'Blog Header Title', 'crowdpress' ),
        'desc'        => esc_html__( 'Blog page header title', 'crowdpress' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'blog_subtitle',
        'label'       => esc_html__( 'Blog Header Sub-Title', 'crowdpress' ),
        'desc'        => esc_html__( 'Blog page header sub-title', 'crowdpress' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
		array(
        'id'          => 'blog_layout',
        'label'       => esc_html__( 'Blog layout', 'crowdpress' ),
        'desc'        => esc_html__( 'Blog layout for blog page', 'crowdpress' ),
        'std'         => 'rs',
        'type'        => 'radio-image',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'full',
            'label'       => esc_html__( 'Full width', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/full-width.png'
          ),
          array(
            'value'       => 'ls',
            'label'       => esc_html__( 'Left sidebar', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/left-sidebar.png'
          ),
          array(
            'value'       => 'rs',
            'label'       => esc_html__( 'Right sidebar', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/right-sidebar.png'
          )
        )
      ),
      array(
        'id'          => 'blog_sidebar',
        'label'       => esc_html__( 'Blog Sidebar', 'crowdpress' ),
        'desc'        => esc_html__( 'Select your blog sidebar', 'crowdpress' ),
        'std'         => 'sidebar-1',
        'type'        => 'sidebar-select',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'blog_layout:not(full)',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'blog_single_title',
        'label'       => esc_html__( 'Blog Single Header Title', 'crowdpress' ),
        'desc'        => esc_html__( 'Blog single header title', 'crowdpress' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'blog_single_subtitle',
        'label'       => esc_html__( 'Blog Single Header Sub-Title', 'crowdpress' ),
        'desc'        => esc_html__( 'Blog single header sub-title', 'crowdpress' ),
        'std'         => '',
        'type'        => 'text',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'single_layout',
        'label'       => esc_html__( 'Blog single post layout', 'crowdpress' ),
        'desc'        => esc_html__( 'Blog single post layout', 'crowdpress' ),
        'std'         => 'rs',
        'type'        => 'radio-image',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'full',
            'label'       => esc_html__( 'Full width', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/full-width.png'
          ),
          array(
            'value'       => 'ls',
            'label'       => esc_html__( 'Left sidebar', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/left-sidebar.png'
          ),
          array(
            'value'       => 'rs',
            'label'       => esc_html__( 'Right sidebar', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/right-sidebar.png'
          )
        )
      ),
    array(
        'id'          => 'blog_single_sidebar',
        'label'       => esc_html__( 'Single post Sidebar', 'crowdpress' ),
        'desc'        => esc_html__( 'Single post sidebar', 'crowdpress' ),
        'std'         => 'sidebar-1',
        'type'        => 'sidebar-select',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'single_layout:not(full)',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'blog_header_banner_type',
        'label'       => esc_html__( 'Header Banner Type', 'crowdpress' ),
        'desc'        => esc_html__( 'Select your header banner type', 'crowdpress' ),
        'std'         => 'blog_custom_image',
        'type'        => 'select',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'blog_bg_color',
            'label'       => esc_html__( 'Background Color', 'crowdpress' ),
          ),
		  array(
            'value'       => 'blog_custom_image',
            'label'       => esc_html__( 'Custom Image', 'crowdpress' ),
          )
        )
      ),
	  array(
        'id'          => 'blog_header_custom_color',
        'label'       => esc_html__('Select Color', 'crowdpress'),
        'desc'        => '',
        'std'         => '#001d23',
        'type'        => 'colorpicker',
		'section'     => 'blog_options',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
        'condition'   => 'blog_header_banner_type:is(blog_bg_color)',
      ),
	  array(
        'id'          => 'blog_default_banner_image',
        'label'       => esc_html__( 'Background Image', 'crowdpress' ),
        'desc'        => esc_html__( 'Background image', 'crowdpress' ),
        'std'         => CROWDPRESSTHEMEURI. 'images/banner.jpg',
        'type'        => 'upload',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'blog_header_banner_type:is(blog_custom_image)',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'sticky_post_text',
        'label'       => esc_html__( 'Sticky post text', 'crowdpress' ),
        'desc'        => esc_html__( 'Sticky post text', 'crowdpress' ),
        'std'         => 'Featured',
        'type'        => 'text',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'excerpt_length',
        'label'       => esc_html__( 'Excerpt Length', 'crowdpress' ),
        'desc'        => esc_html__( 'Excerpt Length', 'crowdpress' ),
        'std'         => '10',
        'type'        => 'text',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'show_social_sharing_icons',
        'label'       => esc_html__( 'Display Social Sharing Icons', 'crowdpress' ),
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'next_previous_title_crop',
        'label'       => esc_html__( 'Next/Prev Title Crop', 'crowdpress' ),
        'desc'        => esc_html__( 'Next/Prev title crop in single post', 'crowdpress' ),
        'std'         => '100',
        'type'        => 'text',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'blog_gallery_rtl',
        'label'       => esc_html__( 'Turn on RTL for Blog Gallery Formate', 'crowdpress' ),
        'desc'        => '',
        'std'         => 'off',
        'type'        => 'on-off',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
    );

	return apply_filters( 'crowdpress_blog_options', $options );
}  
?>