<?php
/**
 * Initialize the meta boxes. 
 */

add_action( 'admin_init', 'crowdpress_meta_boxes' );

function crowdpress_meta_boxes() {
  if( function_exists( 'ot_get_option' ) ): 
  $my_meta_box = array(
    'id'        => 'crowdpress_meta_box',
    'title'     => esc_html__('crowdpress Page Settings', 'crowdpress'),
    'desc'      => '',
    'pages'     => array( 'page' ),
    'context'   => 'normal',
    'priority'  => 'high',
    'fields'    => array(
	  array(
        'id'          => 'header_settings',
        'label'       => esc_html__('Header Settings', 'crowdpress'),      
        'type'        => 'tab',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'custom_title',
        'label'       => esc_html__('Custom Title', 'crowdpress'),
        'desc'        => '',
        'std'         => 'on',
        'type'        => 'on-off',
        'class'       => '',
        'choices'     => array(),
        'condition'	  => '',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'title',
        'label'       => esc_html__('Title', 'crowdpress'),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
        'condition'	  => 'custom_title:is(on)'
      ),
      array(
        'id'          => 'subtitle',
        'label'       => esc_html__('Sub Title', 'crowdpress'),
        'desc'        => '',
        'std'         => '',
        'type'        => 'text',
        'class'       => '',
        'rows'		  => 3,
        'choices'     => array(),
        'operator'    => 'and',
        'condition'	  => 'custom_title:is(on)'
      ),
	   array(
        'id'          => 'content_tab',
        'label'       => esc_html__('Layout Settings', 'crowdpress'),      
        'type'        => 'tab',
        'operator'    => 'and'
      ),
      array(
        'id'          => 'page_layout',
        'label'       => esc_html__( 'Default Layout', 'crowdpress' ),
        'desc'        => '',
        'std'         => 'full',
        'type'        => 'radio-image',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'full',
            'label'       => esc_html__( 'Full Width', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/full-width.png'
          ),
          array(
            'value'       => 'ls',
            'label'       => esc_html__( 'With Left Sidebar', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/left-sidebar.png'
          ),
          array(
            'value'       => 'rs',
            'label'       => esc_html__( 'With Right sidebar', 'crowdpress' ),
            'src'         => OT_URL . '/assets/images/layout/right-sidebar.png'
          ),
        )
      ),
      array(
        'id'          => 'sidebar',
        'label'       => esc_html__('Select Sidebar', 'crowdpress'),
        'desc'        => '',
        'std'         => 'sidebar-1',
        'type'        => 'sidebar-select',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
        'condition'   => 'page_layout:not(full)'
      ),
	  array(
        'id'          => 'header_banner_type',
        'label'       => esc_html__( 'Header Banner Type', 'crowdpress' ),
        'desc'        => esc_html__( 'Select your header banner type', 'crowdpress' ),
        'std'         => 'custom_image',
        'type'        => 'select',
        'section'     => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'bg_color',
            'label'       => esc_html__( 'Background Color', 'crowdpress' ),
          ),
		  array(
            'value'       => 'custom_image',
            'label'       => esc_html__( 'Background Image', 'crowdpress' ),
          )
        )
      ),
	  array(
        'id'          => 'page_header_custom_color',
        'label'       => esc_html__('Select Color', 'crowdpress'),
        'desc'        => '',
        'std'         => '#001d23',
        'type'        => 'colorpicker',
		'section'     => 'blog_options',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
        'condition'   => 'header_banner_type:is(bg_color)',
      ),
	  array(
        'id'          => 'header_custom_image',
        'label'       => esc_html__( 'Background Image', 'crowdpress' ),
        'desc'        => esc_html__( 'Background image', 'crowdpress' ),
        'std'         => CROWDPRESSTHEMEURI. 'images/banner.jpg',
        'type'        => 'upload',
        'section'     => 'blog_options',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => 'header_banner_type:is(custom_image)',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'breadcrumb_bg_color',
        'label'       => esc_html__('Breadcrumb BG Color', 'crowdpress'),
        'desc'        => '',
        'std'         => '#ffffff',
        'type'        => 'colorpicker',
		'section'     => 'blog_options',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
        'condition'   => '',
      ),
	  array(
        'id'          => 'content_padding_type',
        'label'       => esc_html__( 'Main Content Padding', 'crowdpress' ),
        'desc'        => esc_html__( 'Main content padding', 'crowdpress' ),
        'std'         => 'with_padding',
        'type'        => 'select',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array( 
          array(
            'value'       => 'no_padding',
            'label'       => esc_html__( 'Main Content No Padding', 'crowdpress' ),
          ),
		  array(
            'value'       => 'with_padding',
            'label'       => esc_html__( 'Main Content With Padding', 'crowdpress' ),
          )
        )
      ),
	  array(
        'id'          => 'content_background',
        'label'       => esc_html__('Content Background', 'crowdpress'),
        'desc'        => '',
        'std'         => '#ffffff',
        'type'        => 'colorpicker',
		'section'     => 'blog_options',
        'class'       => '',
        'choices'     => array(),
        'operator'    => 'and',
      ),
	  array(
        'id'          => 'footer_settings',
        'label'       => esc_html__('Footer Settings', 'crowdpress'),      
        'type'        => 'tab',
        'operator'    => 'and'
      ),
	  array(
        'id'          => 'page_footer_style',
        'label'       => esc_html__( 'Footer Style', 'crowdpress' ),
        'desc'        => esc_html__( 'Select footer style', 'crowdpress' ),
        'std'         => 'style1',
        'type'        => 'select',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
		  array(
            'value'       => 'style1',
            'label'       => esc_html__( 'Style 1', 'crowdpress' ),
          ),
          array(
            'value'       => 'style2',
            'label'       => esc_html__( 'Style 2', 'crowdpress' ),
          ),
		  array(
            'value'       => 'style3',
            'label'       => esc_html__( 'Style 3', 'crowdpress' ),
          )
        )
      ),
	  array(
        'id'          => 'page_footer_logo',
        'label'       => esc_html__( 'Page Footer Logo', 'crowdpress' ),
        'desc'        => esc_html__( 'Footer logo for this page', 'crowdpress' ),
        'std'         => CROWDPRESSTHEMEURI . 'images/footer-logo.png',
        'type'        => 'upload',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and'
      ),
    )
  );
  
  ot_register_meta_box( $my_meta_box );
  endif;
}
?>