<?php
//include theme options
include CROWDPRESSTHEMEDIR . '/admin/theme-options/general.php';
include CROWDPRESSTHEMEDIR . '/admin/theme-options/background.php';
include CROWDPRESSTHEMEDIR . '/admin/theme-options/header.php';
include CROWDPRESSTHEMEDIR . '/admin/theme-options/sidebar.php';
include CROWDPRESSTHEMEDIR . '/admin/theme-options/footer.php';
include CROWDPRESSTHEMEDIR . '/admin/theme-options/blog.php';
include CROWDPRESSTHEMEDIR . '/admin/theme-options/portfolio.php';
include CROWDPRESSTHEMEDIR . '/admin/theme-options/donation.php';
include CROWDPRESSTHEMEDIR . '/admin/theme-options/woocommerce.php';
include CROWDPRESSTHEMEDIR . '/admin/theme-options/typography.php';
include CROWDPRESSTHEMEDIR . '/admin/theme-options/styling.php';
/**
 * Initialize the custom theme options.
 */
add_action( 'admin_init', 'crowdpress_theme_options', 1 );

/**
 * Build the custom settings & update OptionTree.
 */
function crowdpress_theme_options() {
  
  /* OptionTree is not loaded yet */
  if ( ! function_exists( 'ot_settings_id' ) )
    return false;
    
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( ot_settings_id(), array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  //available option functions - return type array()
  $general_options = crowdpress_general_options();
  $background_options = crowdpress_background_options();
  $header_options = crowdpress_header_options();
  $sidebar_options = crowdpress_sidebar_options();
  $footer_options = crowdpress_footer_options();
  $blog_options = crowdpress_blog_options();
  $portfolio_options = crowdpress_portfolio_options();
  $donation_options = crowdpress_donation_options();
  $woocommerce_options = crowdpress_woocommerce_options();
  $typography_options = crowdpress_typography_options();
  $styling_options = crowdpress_styling_options();


  //merge all available options
  $settings = array_merge( $general_options, $background_options, $header_options, $sidebar_options, $footer_options, $blog_options, $portfolio_options,$donation_options, $woocommerce_options, $typography_options, $styling_options );

 

  $custom_settings = array( 
    'contextual_help' => array( 
      'sidebar'       => ''
    ),
    'sections'        => array( 
      array(
        'id'          => 'general_options',
        'title'       => esc_html__( 'General Options', 'crowdpress' )
      ),
      array(
        'id'          => 'background_options',
        'title'       => esc_html__( 'Background Options', 'crowdpress' )
      ),
     array(
        'id'          => 'header_options',
        'title'       => esc_html__( 'Header Options', 'crowdpress' )
      ),
      array(
        'id'          => 'footer_options',
        'title'       => esc_html__( 'Footer Options', 'crowdpress' )
      ),
      array(
        'id'          => 'sidebar_option',
        'title'       => esc_html__( 'Sidebar Options', 'crowdpress' )
      ),
      array(
        'id'          => 'blog_options',
        'title'       => esc_html__( 'Blog Options', 'crowdpress' )
      ),
	  array(
        'id'          => 'donation_options',
        'title'       => esc_html__( 'Donation Options', 'crowdpress' )
      ),
	  array(
        'id'          => 'portfolio_options',
        'title'       => esc_html__( 'Portfolio Options', 'crowdpress' )
      ),
	  array(
        'id'          => 'woocommerce_options',
        'title'       => esc_html__( 'WooCommerce Options', 'crowdpress' )
      ),
      array(
        'id'          => 'fonts',
        'title'       => esc_html__( 'Typography Options', 'crowdpress' )
      ),
      array(
        'id'          => 'styling_options',
        'title'       => esc_html__( 'Styling Options', 'crowdpress' )
      ),
	  
    ),
    'settings'        => $settings
  );

  
  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );
  
  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings ); 
  }
  
  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;

  return $custom_settings[ 'settings' ];
  
}