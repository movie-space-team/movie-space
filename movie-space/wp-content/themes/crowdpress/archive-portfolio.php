<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, crowdpress already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage crowdpress
 * @since crowdpress 1.0.0
 */

get_header(); ?>

	<?php get_template_part( 'layout/before', '' ); ?>
		<?php 
			$terms = get_terms( 'portfolio_category' );
			if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
			?>
        	<div id="filters-container" class="cbp-l-filters-button portfolio-filter text-center">
            <div data-filter="*" class="cbp-filter-item-active cbp-filter-item"> <?php echo esc_html__('All', 'crowdpress'); ?>
              <div class="cbp-filter-counter"></div>
            </div>
            <?php
        	foreach ( $terms as $term ) {
			?>
            <div data-filter=".<?php echo esc_attr($term->slug); ?>" class="cbp-filter-item">
				<?php echo esc_html($term->name); ?>
                <div class="cbp-filter-counter"></div>
            </div>
            <?php } ?>
          </div>
          <?php } ?>
          
        <?php
		$column = (function_exists('ot_get_option'))? ot_get_option( 'portfolio_column', '3' ) : '3';
        // Loop through the posts and do something with them.
        if ( have_posts() ) : ?>        	
        
        	<div id="grid-container" class="cbp" data-columnnum="<?php echo esc_attr($column); ?>" data-layout="grid">

				<?php while ( have_posts() ) : the_post(); ?>
                
                	<?php get_template_part( 'content', 'portfolio' ); ?>
				
                <?php endwhile; ?>                
            </div>
			
			<?php crowdpress_posts_nav(); ?>
            
            <?php  
		else:
			// If no content, include the "No posts found" template.
        	get_template_part( 'content', 'none' );
        endif;
		?> 
		
<?php get_template_part( 'layout/after', '' ); ?>
    
<?php get_footer(); ?>