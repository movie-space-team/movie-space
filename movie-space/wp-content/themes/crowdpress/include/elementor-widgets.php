<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if(defined('ELEMENTOR_VERSION')):

class CrowdPress_Elementor_Widgets_Init{
	
    public static $instance;

	/**
     * Load Construct
     * 
     * @since 1.0
     */
	/** CrowdPress Class Constructor **/
	public function __construct(){
		add_action('elementor/init', array($this, 'crowdpress_elementor_category'));
        add_action('elementor/widgets/widgets_registered', array($this, 'crowdpress_all_elements'));
		add_action('elementor/frontend/after_enqueue_scripts', array( $this, 'crowdpress_enqueue_scripts' ) );        
	}
	
	public static function get_instance(){
		if (null === self::$instance) {
			self::$instance = new self();
		}
		return self::$instance;
	}

    /** Enqueue Scripts and Stylesheets **/ 
    
     public function crowdpress_enqueue_scripts() {
		 wp_enqueue_script( 'crowdpress-elementor', CROWDPRESSTHEMEURI . 'js/lib/scripts-elementor.js', [ 'jquery' ], false, true );
    }
	
	/** Elementor add category **/

    public function crowdpress_elementor_category(){    
		\Elementor\Plugin::$instance->elements_manager->add_category(
			'crowdpress-all-elements',
			[
				'title' =>esc_html__( 'CrowdPress', 'crowdpress' ),
				'icon' => 'fas fa-hand-holding-usd',
			],
			1
		);
    }

    public function crowdpress_all_elements($elements){

      require_once CROWDPRESSTHEMEDIR .'include/elements/title.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Title());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/services.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Services());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/featured-category.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Featured_Category());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/projects.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Projects());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/counter.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Counter());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/listicon.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Listicons());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/testmonials.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Testmonials());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/partner.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Partner());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/posts.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Posts());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/portfolios.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Portfolios());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/gallery.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Gallery());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/team.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Team());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/contact-box.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Contact_Box());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/portfolio-meta.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Portfolio_Meta());
	  
	  if ( class_exists( 'Tribe__Events__Main' ) ) {
	  require_once CROWDPRESSTHEMEDIR .'include/elements/events.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Events());
	  }
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/percent-box.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Percent_Box());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/video-popup.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Video_Popup());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/feature-lists.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Feature_Lists());
	  
	  require_once CROWDPRESSTHEMEDIR .'include/elements/main-logo.php';
      $elements->register_widget_type(new Elementor\CrowdPress_Main_Logo_Widget());
	  
    }

}

if (class_exists('CrowdPress_Elementor_Widgets_Init')){
	CrowdPress_Elementor_Widgets_Init::get_instance();
}

endif;