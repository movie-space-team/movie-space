<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


class CrowdPress_Gallery extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-gallery';
    }

    public function get_title() {

        return esc_html__( 'Gallery', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fas fa-flag';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
        );
		
		$this->add_control(
			'gallery_style',
			[
				'label' => esc_html__( 'Style', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'gallery',
				'options' => [
					'gallery'  => esc_html__( 'Gallery', 'crowdpress' ),
					'slider' => esc_html__( 'Slider', 'crowdpress' ),
				],
			]
		);
		
		$this->add_control(
			'gallery',
			[
				'label' => __( 'Add Images', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::GALLERY,
				'default' => [],
			]
		);
		
		$this->add_control(
			'number_of_cloumn',
			[
				'label' => esc_html__( 'Number of Column', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 6,
				'step' => 1,
				'default' => 3,
			]
		);
		
		$this->add_control(
			'gutter_style',
			[
				'label' => esc_html__( 'Gutter', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'gutter',
				'condition' => ['gallery_style' => ['gallery']],
				'options' => [
					'gutter'  => esc_html__( 'Gutter', 'crowdpress' ),
					'no-gutters' => esc_html__( 'No Gutter', 'crowdpress' ),
				],
			]
		);
 

        $this->add_responsive_control(
			'padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .gallery-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
		$this->end_controls_section();
		
		// Style Section //		
		$this->start_controls_section(
			'gal_style_section',
			[
				'label' => esc_html__( 'Style', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'bg_color',
			[
				'label' => esc_html__( 'Background Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .image-link' => 'background-color: {{VALUE}}',
				],
			]
		);
       
	  $this->end_controls_section();
	  // End Style Section //
    }

    protected function render() {
        $settings = $this->get_settings_for_display();
		$column = round(12/$settings['number_of_cloumn']);
        ?>
        <div class="gallery-content">
        	<?php if($settings['gallery_style'] == 'slider'): ?>
            	<div class="slider slider-nav slick-slider" data-column="<?php echo esc_attr($settings['number_of_cloumn']); ?>">
            	<?php
					foreach ( $settings['gallery'] as $image ) {
						$img_src = crowdpress_image_resize($image['url'] , 520, 440);
						?>
						<div class="slick-slide-div"><img src="<?php echo esc_url($img_src); ?>" alt="<?php echo esc_attr__('gallery image', 'crowdpress'); ?>"></div>
						<?php
					}
				?>
                </div>
            <?php else: ?>
            <div class="row <?php echo esc_attr($settings['gutter_style']); ?>">
            <?php
                foreach ( $settings['gallery'] as $image ) {
					$img_src = crowdpress_image_resize($image['url'] , 370, 260);
                    ?>
                    <div class="col-md-<?php echo esc_attr($column); ?>">
                        <div class="single-img-gal">
                        <img src="<?php echo esc_url($img_src); ?>" alt="<?php echo esc_attr__('gallery image', 'crowdpress'); ?>">
                        <a href="<?php echo esc_url($image['url']); ?>" class="image-link"><i class="fal fa-expand"></i></a>
                        </div>
                    </div>
                    <?php
                }
            ?>
            </div>
            <?php endif; ?>     
        </div>

        <?php
	}

    protected function _content_template() {}
}