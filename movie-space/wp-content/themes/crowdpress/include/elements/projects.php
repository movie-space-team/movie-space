<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class CrowdPress_Projects extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-projects';
    }

    public function get_title() {

        return esc_html__( 'Projects', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fas fa-folder';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
		
		$this->add_control(
			'posts_style',
			[
				'label' => esc_html__( 'Style', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'style1',
				'options' => [
					'style1'  => esc_html__( 'Style 1', 'crowdpress' ),
					'style2' => esc_html__( 'Style 2', 'crowdpress' ),
				],
			]
		);
	
		$this->add_control(
			'number_of_posts',
			[
				'label' => esc_html__( 'Number of Projects', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 48,
				'step' => 1,
				'default' => 6,
			]
		);
		
		$this->add_control(
			'cateogries',
			[
				'label' => esc_html__( 'Select Categories', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => true,
				'options' => $this->get_projects_categories(),
				'label_block' => true,
				'multiple'		=> true,
			]			
		);
		
		$this->add_control(
			'order',
			[
				'label' => esc_html__( 'Order', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'DESC',
				'options' => [
					'DESC'  => esc_html__( 'DESC', 'crowdpress' ),
					'ASC' => esc_html__( 'ASC', 'crowdpress' ),
				],
			]
		);
		
		$this->add_control(
			'orderby',
			[
				'label' => esc_html__( 'Order By', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'date',
				'options' => [
					'title'  => esc_html__( 'Title', 'crowdpress' ),
					'date' => esc_html__( 'Date', 'crowdpress' ),
					'ID'  => esc_html__( 'ID', 'crowdpress' ),
					'name'  => esc_html__( 'Name', 'crowdpress' ),
					'rand' => esc_html__( 'Rand', 'crowdpress' ),
					'comment_count'  => esc_html__( 'Comment Count', 'crowdpress' ),
					'menu_order' => esc_html__( 'Menu Order', 'crowdpress' ),					
					'author' => esc_html__( 'Author', 'crowdpress' ),
				],
			]
		);
		
		$this->add_control(
			'remaining_title',
			[
				'label' => esc_html__( 'Remaining Text', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Raised of', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your remaining text here', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'donor_title',
			[
				'label' => esc_html__( 'Donor Before Text', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Backers We Got', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your donor here', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'number_of_categories',
			[
				'label' => esc_html__( 'Number of Categories', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 9,
				'step' => 1,
				'default' => 1,
			]
		);		
		
		
		$this->end_controls_section();

		// Style Section //
		
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Title', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'title_tabs'
		);
		
		$this->start_controls_tab(
			'title_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .blog-des-title a' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'title_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'title_color_hover',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .projects-loop:hover .blog-des-title a' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .blog-des-title a',
			]
		);

        $this->add_responsive_control(
			'title_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .blog-des-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
	  
	  // Style Cause Section //
		
		$this->start_controls_section(
			'cause_section',
			[
				'label' => esc_html__( 'Progress Bar', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'cause_tabs'
		);
		
		$this->start_controls_tab(
			'cause_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'cause_color',
			[
				'label' => esc_html__( 'Bar Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .project-cause-bar' => 'background: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'price_color',
			[
				'label' => esc_html__( 'Price Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cause-amounts p.theme-color' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'cause_hover_tab',
			[
				'label' => esc_html__( 'Active', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'cause_color_hover',
			[
				'label' => esc_html__( 'Bar Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .project-cause-bar .progress-bar' => 'background: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'cause_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .cause-amounts p',
			]
		);

        $this->add_responsive_control(
			'cause_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .cause-amounts' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
	  
	  // Meta Style Section //
		
		$this->start_controls_section(
			'meta_section',
			[
				'label' => esc_html__( 'Meta', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'meta_tabs'
		);
		
		$this->start_controls_tab(
			'meta_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'meta_color',
			[
				'label' => esc_html__( 'Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .blog-meta a' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'meta_icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .projects-content .blog-meta .svg-inline--fa, {{WRAPPER}} .projects-content .blog-meta i' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'meta_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'meta_color_hover',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .blog-meta a:hover' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'meta_hover_icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .blog-meta a:hover i, {{WRAPPER}} .blog-meta a:hover .svg-inline--fa' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'meta_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .blog-meta a',
			]
		);

        $this->add_responsive_control(
			'meta_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .blog-meta' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
	  
	  $this->add_control(
			'meta_content_color',
			[
				'label' => esc_html__( 'Content Border Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .projects-content .rel-blog-desc' => 'border-color: {{VALUE}}',
				],
			]
		);
       
      $this->end_controls_section();
		
    }

    protected function render( ) { 
        $settings = $this->get_settings_for_display();
		$no_border_class = '';
		
		if ( class_exists( 'Give' ) ){
		$args = array(
		'post_type' => 'give_forms',
		'posts_per_page' => $settings['number_of_posts'],
		'order'          => $settings['order'],
		'orderby'        => $settings['orderby'],
		);
		
		if(!empty($settings['cateogries'])){
			
			$args['tax_query'] = array(
            array(
				'taxonomy' => 'give_forms_category',
				'terms'    => $settings['cateogries'],
				'field' => 'id',
				),
		    );		
		}
		
		$query = get_posts( $args );
		?>
        
		<div class="projects-content">
			<?php
            if ($query): ?>
            <div class="row justify-content-center">
                <?php foreach ( $query as $post ): ?>
                <?php
                $form        = new \Give_Donate_Form($post->ID);
                $goal        = $form->goal;
                $income      = $form->get_earnings();
    
                if ($income && $goal) {
                    $progress = round( ( $income / $goal ) * 100 );
                } else {
                    $progress = '';
                }
                if ( $income >= $goal ) {
                  $progress = 100;
                }
    
                $income = give_human_format_large_amount( give_format_amount( $income ) );
                $goal = give_human_format_large_amount( give_format_amount( $goal ) );
                
                if(function_exists('give_get_payments')) {
                $args = array(
                'give_forms' => array( $post->ID ),
                );
                $donations = give_get_payments( $args );
                $donor_count = count($donations);
                } else {
                $donor_count = 0;
                }
                ?>
                <?php if(!has_post_thumbnail($post->ID)){
                    $no_border_class = ' no-thumb-des';
                } ?>
                <div class="col-md-6 col-lg-4 projects-loop">
                    <?php if(has_post_thumbnail($post->ID)): ?>
                    <div class="blog-media">
                        <a href="<?php esc_url(the_permalink($post->ID)); ?>">
                        <?php
                        $featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');
                        $img_src = crowdpress_image_resize($featured_img_url , 740, 520);
                        ?>
                        <img src="<?php echo esc_url($img_src); ?>" alt="<?php echo esc_attr(get_the_title($post->ID)); ?>" />
                        </a>
                        <div class="cat-readmore-con text-right">
                            <?php
							$cat_count = 1;
                            $terms = get_the_terms( $post->ID, 'give_forms_category' );						 
                            foreach($terms as $term){
                                ?>
                                <a href="<?php echo esc_url( get_term_link( $term->term_id ) ) ?>"><?php echo esc_html($term->name); ?></a>
                                <?php
								if($cat_count == $settings['number_of_categories']){
									break;
								}
								$cat_count++;
                            }
                            ?>
                        </div>                        
                    </div>
                    <?php endif; ?>
                    
                    <div class="rel-blog-desc<?php echo esc_attr($no_border_class); ?>">
                        
                    
                        <h4 class="blog-des-title"><a href="<?php esc_url(the_permalink($post->ID)); ?>"><?php echo get_the_title($post->ID); ?></a></h4>
                        <div class="progress-bar-details">
                            <div class="project-cause-bar"><span class="progress-bar" role="progressbar" aria-valuenow="<?php echo esc_attr($progress); ?>" aria-valuemin="0" aria-valuemax="100"></span></div>
                            
                                <div class="cause-amounts row">
                                    <div class="col-6">
                                        <p class="theme-color"><?php echo esc_html(give_currency_symbol().$income); ?></p>
                                        <p><?php echo esc_html($settings['remaining_title']); ?> <?php echo esc_html(give_currency_symbol().$goal); ?></p>
                                    </div>
                                    <div class="col-6">
                                        <p class="theme-color"><?php echo esc_html($donor_count); ?>+</p>
                                        <p><?php echo esc_html($settings['donor_title']); ?></p>
                                    </div>
                                </div>
                        </div>
                        
                        <div class="blog-meta">
                            <?php $author_id = $post->post_author; ?>
                            <a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID', $author_id ) ) ); ?>"><i class="far fa-user"></i><?php echo esc_html__( 'By ', 'crowdpress' ); ?><?php echo get_the_author_meta('display_name', $author_id); ?></a>
                            <a href="<?php esc_url(the_permalink($post->ID)); ?>"><i class="far fa-calendar-alt"></i><?php echo get_the_date(get_option( 'date_format' ), $post->ID); ?></a>
                            <a href="<?php esc_url(the_permalink($post->ID)); ?>"><i class="far fa-heart"></i></a>
                        </div>
                    </div>
                                      
                </div>
                <?php
                endforeach;
                ?>
            </div>
            <?php endif; ?>      
		</div>
    	<?php
		}
    }
    protected function _content_template() {}
	
	public function get_projects_categories(){
		$category = [];
		if ( class_exists( 'Give' ) ){
		$args = array( 'hide_empty=0' );
		$terms = get_terms( 'give_forms_category', $args );		
		if(!empty($terms)){
			foreach ( $terms as $term ) {
				$category[$term->term_id] = [$term->name];
			}
		}
		}
		return $category;
	}
}