<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class CrowdPress_Portfolio_Meta extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-portfolio-meta';
    }

    public function get_title() {

        return esc_html__( 'Portfolio Meta', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fas fa-folder';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
		
		$this->add_control(
			'show_title',
			[
				'label' => esc_html__( 'Show Title', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'crowdpress' ),
				'label_off' => esc_html__( 'Hide', 'crowdpress' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		
		$this->add_control(
			'show_category',
			[
				'label' => esc_html__( 'Show Category', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'crowdpress' ),
				'label_off' => esc_html__( 'Hide', 'crowdpress' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		
		$this->add_control(
			'show_author',
			[
				'label' => esc_html__( 'Show Author', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'crowdpress' ),
				'label_off' => esc_html__( 'Hide', 'crowdpress' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		
		$this->add_control(
			'show_date',
			[
				'label' => esc_html__( 'Show Date', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'crowdpress' ),
				'label_off' => esc_html__( 'Hide', 'crowdpress' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		
		$this->add_control(
			'show_readmore',
			[
				'label' => esc_html__( 'Show Link', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'crowdpress' ),
				'label_off' => esc_html__( 'Hide', 'crowdpress' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);		
		
		
		$this->end_controls_section();

		// posts Style Section //
		
		$this->start_controls_section(
			'posts_style_section',
			[
				'label' => esc_html__( 'Title', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'posts_color',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cat-list-column h5' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .cat-list-column h5',
			]
		);
		
		$this->add_control(
			'title_color_hover',
			[
				'label' => esc_html__( 'Title Color Hover', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cat-list-column h5:hover' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'border_color',
			[
				'label' => esc_html__( 'Border Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cat-list-column' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'border_color_hover',
			[
				'label' => esc_html__( 'Border Color Hover', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cat-list-column:hover' => 'border-color: {{VALUE}}',
				],
			]
		);
       
	  $this->end_controls_section();
		
    }

    protected function render( ) { 
        $settings = $this->get_settings_for_display();
		?>
        
		<div class="project-meta-content">
            <div class="row">
                <div class="col-md-5">        
                <?php
                if($settings['show_category'] == 'yes'){
                    $terms = get_the_terms( get_the_ID(), 'portfolio_category' );						 
                    foreach($terms as $term){
                        ?>
                        <a href="<?php echo esc_url( get_term_link( $term->term_id ) ) ?>" class="port-cat"><?php echo esc_html($term->name); ?></a>
                        <?php
                    }
                }
                ?>
                <?php if($settings['show_title'] == 'yes'){ ?>
                <h2><?php echo get_the_title(get_the_ID()); ?></h2>
                <?php } ?>
                </div>
                <?php if($settings['show_author'] == 'yes' || $settings['show_date'] == 'yes'){ ?>
                <div class="col-md-5">
                	<div class="aut-dat-box">
                	<span><?php echo esc_html__('User', 'crowdpress'); ?></span>
                    <h4><?php echo esc_html(get_post_meta(get_the_ID(), 'port_author', true)); ?></h4>
                    </div>
                    <div class="aut-dat-box">
                    	<span><?php echo esc_html__('Date', 'crowdpress'); ?></span>
                    	<h4><?php echo esc_html(get_post_meta(get_the_ID(), 'port_date', true)); ?></h4>
                    </div>
                </div>
                <?php } ?>
                <?php if($settings['show_readmore'] == 'yes'){ ?>
                <div class="col-md-2 align-self-end text-right">
                	<a class="btn btn-primary" href="<?php echo esc_url(get_post_meta(get_the_ID(), 'port_link', true)); ?>" target="_blank"><i class="fal fa-arrow-right"></i></a>
                </div>
                <?php } ?>    
            </div>
        </div>
    	<?php
    }
    protected function _content_template() {}
}