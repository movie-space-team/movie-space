<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


class CrowdPress_Listicons extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-listicon';
    }

    public function get_title() {

        return esc_html__( 'Listicon', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fab fa-fonticons-fi';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
        );

        $this->add_control(
			'listicon',
			[
				'label' => esc_html__( 'Icon', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'default' => [
					'value' => 'fas fa-check',
					'library' => 'solid',
				],
			]
		);

		$this->add_control(
			'title',
			[
				'label' => esc_html__( 'Title', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Raise funds with a crowdfunding campaign', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your title here', 'crowdpress' ),
			]
        );
        
        $this->add_responsive_control(
            'text_align', [
                'label'          => esc_html__( 'Alignment', 'crowdpress'  ),
                'type'           => Controls_Manager::CHOOSE,
                'options'        => [
    
                    'left'         => [
                        'title'    => esc_html__( 'Left', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-left',
                    ],
                    'center'     => [
                        'title'    => esc_html__( 'Center', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-center',
                    ],
                    'right'         => [
                        'title'     => esc_html__( 'Right', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-right',
                    ],
                ],
               'default'         => 'left',
               'selectors' => [
                   '{{WRAPPER}} .listicon-content' => 'text-align: {{VALUE}};'
               ],
            ]
        );
 

        $this->add_responsive_control(
			'padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .listicon-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
        $this->end_controls_section();

        // Number style tab //
		
		$this->start_controls_section(
			'style_number_section',
			[
				'label' => esc_html__( 'Icon', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .listicon-content span' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'icon_bg_color',
			[
				'label' => esc_html__( 'Icon Background Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .listicon-content span' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Text Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .listicon-content p' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'text_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .listicon-content p',
			]
		);
		
		$this->add_responsive_control(
			'border_radius',
			[
				'label' =>esc_html__( 'Border Radius', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .listicon-content p span' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );

		$this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings_for_display();
        ?>
        <div class="listicon-content">
		<?php if($settings['title'] != ''): ?>
        <p><span><?php \Elementor\Icons_Manager::render_icon( $settings['listicon']); ?></span><?php echo esc_html($settings['title']); ?></p>
		<?php endif; ?>
		</div>

        <?php
	}

    protected function _content_template() {}
}