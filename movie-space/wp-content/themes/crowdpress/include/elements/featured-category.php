<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class CrowdPress_Featured_Category extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-featured-cateogry';
    }

    public function get_title() {

        return esc_html__( 'Featured Category', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fas fa-folder';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
		
		$this->add_control(
			'cat_style',
			[
				'label' => esc_html__( 'Style', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'style1',
				'options' => [
					'style1'  => esc_html__( 'Donation Category', 'crowdpress' ),
					'style2' => esc_html__( 'Product Category', 'crowdpress' ),
				],
			]
		);
	
		$this->add_control(
			'number_of_category',
			[
				'label' => esc_html__( 'Number of Category', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 100,
				'step' => 1,
				'default' => 6,
			]
		);
		
		$this->add_control(
			'cateogries_1',
			[
				'label' => esc_html__( 'Select Categories', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => true,
				'options' => $this->get_product_cat(),
				'label_block' => true,
				'multiple'		=> true,
				'condition' => ['cat_style' => ['style2']],
			]			
		);
		
		$this->add_control(
			'cateogries_2',
			[
				'label' => esc_html__( 'Select Categories', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => true,
				'options' => $this->get_donatoion_cat(),
				'label_block' => true,
				'multiple'		=> true,
				'condition' => ['cat_style' => ['style1']],
			]			
		);
		
		$this->add_control(
			'show_link_category',
			[
				'label' => esc_html__( 'Show Link Category', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'crowdpress' ),
				'label_off' => esc_html__( 'Hide', 'crowdpress' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		
		$this->add_control(
			'show_link_text',
			[
				'label' => esc_html__( 'Link Text', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'More Listing', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type text here', 'crowdpress' ),
				'condition' => ['show_link_category' => ['yes']],
			]
		);
		
		$this->add_control(
			'more_link_category',
			[
				'label' => esc_html__( 'More Link', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://', 'crowdpress' ),
				'show_external' => false,
				'default' => [
					'url' => '',
					'is_external' => false,
					'nofollow' => true,
				],
				'condition' => ['show_link_category' => ['yes']],
			]
		);
		
		$this->add_control(
			'more_cat_bg_image',
			[
				'label' => esc_html__( 'More BG Image', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'condition' => ['show_link_category' => ['yes']],
			]
		);		
		
		
		$this->end_controls_section();

		// Category Style Section //
		
		$this->start_controls_section(
			'category_style_section',
			[
				'label' => esc_html__( 'Category', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'category_color',
			[
				'label' => esc_html__( 'Category Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cat-list-column h5' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'category_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .cat-list-column h5',
			]
		);
		
		$this->add_control(
			'category_color_hover',
			[
				'label' => esc_html__( 'Category Color Hover', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cat-list-column h5:hover' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'border_color',
			[
				'label' => esc_html__( 'Border Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cat-list-column' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'border_color_hover',
			[
				'label' => esc_html__( 'Border Color Hover', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cat-list-column:hover' => 'border-color: {{VALUE}}',
				],
			]
		);
       
	  $this->end_controls_section();
		
    }

    protected function render( ) { 
        $settings = $this->get_settings_for_display();
		if($settings['cat_style'] == 'style1'){
			$cat_list = $settings['cateogries_2'];
			$taxonomy = 'give_forms_category';
			$post_type = 'give_forms';
		} else{
			$cat_list = $settings['cateogries_1'];
			$taxonomy = 'product_cat';
			$post_type = 'product';
		}				
		
		$args = array(
			'taxonomy' 		=> $taxonomy,
			'hide_empty' 	=> true,
			'number'		=> $settings['number_of_category'],
			'include'		=> $cat_list,			
		);
 
		$terms = get_terms( $args );
		?>
        
		<div class="featured-category cat-lists text-center">
				<?php
                $i = 0;
				$class = '';
				if ( ! empty( $terms ) && ! is_wp_error( $terms ) ): ?>
                <div class="row justify-content-center">
                    <?php foreach ( $terms as $term ):
					
					$args = array(
					'post_type' => $post_type,
					'tax_query' => array(
						array(
						'taxonomy' => $taxonomy,
						'field' => 'term_id',
						'terms' => $term->term_id
						 )
					  )
					);
					$query = get_posts( $args );
					
                    ?>
                    <div class="col-lg-3 col-md-4">
                        <div class="cat-list-column d-flex align-items-center">
                            <a class="justify-content-center" href="<?php echo esc_url( get_term_link( $term ) ); ?>">
                                <?php if($settings['cat_style'] == 'style2'){ ?>
                                <?php $term_image = get_term_meta( $term->term_id, 'thumbnail_id', true); ?>
                                <?php if($term_image != ''): ?>
                                <div class="img-icon-box"><img src="<?php echo esc_url(wp_get_attachment_url($term_image)); ?>" alt="<?php echo esc_attr($term->name); ?>" /></div>
                                <?php endif; ?>
                                <?php } else{
                                    $term_id = $term->term_id;
                                    $icon_id = get_option( "taxonomy_term_$term_id" );
                                    if(!empty($icon_id)){
                                    ?>
                                    <div class="img-icon-box"><img src="<?php echo esc_url($icon_id['icon_id']); ?>" alt="<?php echo esc_attr($term->name); ?>" /></div>
                                    <?php
                                    }
                                }
                                ?>
                                <h5><?php echo esc_html($term->name); ?></h5>
                                <p class="item-count">0<?php echo esc_html(count($query)); ?>+<?php echo esc_html__(' Listings', 'crowdpress'); ?></p>
                            </a>                        
                        </div>
                    </div>
                    <?php					
					endforeach; ?>
                    <?php if($settings['show_link_category'] == 'yes'):
					$target = $settings['more_link_category']['is_external'] ? ' target="_blank"' : '';
					$nofollow = $settings['more_link_category']['nofollow'] ? ' rel="nofollow"' : '';
					$style_bg = '';
					if($settings['more_cat_bg_image']['url'] != ''){
						$style_bg = 'style="background-image:url('.$settings['more_cat_bg_image']['url'].');"';
					}
					?>
                    <div class="col-lg-3 col-md-4">
                        <div class="cat-list-column last-item-more d-flex align-items-center" <?php echo wp_kses($style_bg, array('style'=>array())); ?>>
                            <a class="justify-content-center" href="<?php echo esc_url($settings['more_link_category']['url']); ?>">
                                <h5><?php echo esc_html($settings['show_link_text']); ?></h5>
                            </a>                        
                        </div>
                    </div>
                    <?php endif; ?>
                    
                </div>
                <?php endif; ?>     
		</div>
    	<?php  
    }
    protected function _content_template() {}
	
	public function get_product_cat(){
		$args = array( 'hide_empty=0' );
		$terms = get_terms( 'product_cat', $args );
		$product_cat = [];
		foreach ( $terms as $term ) {
			$product_cat[$term->term_id] = [$term->name];
		}
		return $product_cat;
	}
	
	public function get_donatoion_cat(){
		$give_cat = [];
		if ( class_exists( 'Give' ) ){
			$args = array( 'hide_empty=0' );
			$terms = get_terms( 'give_forms_category', $args );		
			foreach ( $terms as $term ) {
				$give_cat[$term->term_id] = [$term->name];
			}
		}
		return $give_cat;
	}
	
}