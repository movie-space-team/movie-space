<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


class CrowdPress_Testmonials extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-testmonials';
    }

    public function get_title() {

        return esc_html__( 'Testmonials', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fas fa-comments';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
        );
		
		$this->add_control(
			'testi_style',
			[
				'label' => esc_html__( 'Style', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'style1',
				'options' => [
					'style1'  => esc_html__( 'Style 1', 'crowdpress' ),
					'style2' => esc_html__( 'Style 2', 'crowdpress' ),
				],
			]
		);
		
		$repeater = new \Elementor\Repeater();
        
        $repeater->add_control(
			'testmonials_description',
			[
				'label' => esc_html__( 'Description', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'rows' => 10,
				'default' => esc_html__( 'When you think about collecting customer feed
back, it’s easy to get overwhelmed by the sheer
volume of possibilities. With so many more
customers — and so many ways to connect
with their feedback — it’s hard to know to
where to start.', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your description here', 'crowdpress' ),
			]
        );

        $repeater->add_control(
			'description_icon',
			[
				'label' => esc_html__( 'Description Icon', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'default' => [
					'value' => 'fas fa-quote-right',
					'library' => 'solid',
				],
			]
		);

        $repeater->add_control(
			'testmonials_image',
			[
				'label' => esc_html__( 'Author Image', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
			]
        );
        
        $repeater->add_control(
			'author_name',
			[
				'label' => esc_html__( 'Author Name', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Rosalina D. William', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your title here', 'crowdpress' ),
			]
        );

        $repeater->add_control(
			'author_designation',
			[
				'label' => esc_html__( 'Author Designation', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Founder of Mirax Co.', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your designation', 'crowdpress' ),
			]
        );

        $repeater->add_control(
			'author_icon',
			[
				'label' => esc_html__( 'Author Icon', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'default' => [
					'value' => 'fas fa-star',
					'library' => 'solid',
				],
			]
		);
		
		$this->add_control(
			'list',
			[
				'label' => esc_html__( 'Testominials List', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'testmonials_description' => esc_html__( 'When you think about collecting customer feed
back, it’s easy to get overwhelmed by the sheer
volume of possibilities. With so many more
customers — and so many ways to connect
with their feedback — it’s hard to know to
where to start.', 'crowdpress' ),
						'description_icon' => esc_html__( 'Description Icon', 'crowdpress' ),
						'testmonials_image' => esc_html__( 'Author Image', 'crowdpress' ),
						'author_name' => esc_html__( 'Rosalina D. William', 'crowdpress' ),
						'author_designation' => esc_html__( 'Founder of Mirax Co.', 'crowdpress' ),
						'author_icon' => esc_html__( 'Author Icon', 'crowdpress' ),
					],
				],
				'title_field' => '{{{ author_name }}}',
			]
		);
        
        $this->add_responsive_control(
            'text_align', [
                'label'          => esc_html__( 'Alignment', 'crowdpress'  ),
                'type'           => Controls_Manager::CHOOSE,
                'options'        => [
    
                    'left'         => [
                        'title'    => esc_html__( 'Left', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-left',
                    ],
                    'center'     => [
                        'title'    => esc_html__( 'Center', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-center',
                    ],
                    'right'         => [
                        'title'     => esc_html__( 'Right', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-right',
                    ],
                ],
               'default'         => '',
               'selectors' => [
                   '{{WRAPPER}} .testimonials-content' => 'text-align: {{VALUE}};'
               ],
            ]
        );
 

        $this->add_responsive_control(
			'padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .testimonials-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
	  
	  $this->start_controls_section(
			'control_tab_section',
			[
				'label' => esc_html__( 'Controls', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
        );
		
		$this->add_control(
			'number_of_items',
			[
				'label' => esc_html__( 'Number of Items', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 6,
				'step' => 1,
				'default' => 3,
			]
		);
		
		$this->add_control(
			'show_dots',
			[
				'label' => esc_html__( 'Show Dots', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'crowdpress' ),
				'label_off' => esc_html__( 'Hide', 'crowdpress' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		
		$this->add_control(
			'rtl_option',
			[
				'label' => esc_html__( 'RTL', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Yes', 'crowdpress' ),
				'label_off' => esc_html__( 'No', 'crowdpress' ),
				'default' => 'no',
			]
		);
		
		$this->end_controls_section();
	  
	  // Style Section //
		
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Name', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'title_tabs'
		);
		
		$this->start_controls_tab(
			'title_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testi-name h5' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testi-name h5 .svg-inline--fa' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'icon_bg_color',
			[
				'label' => esc_html__( 'Icon BG Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testi-name h5 .svg-inline--fa' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'title_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'title_color_hover',
			[
				'label' => esc_html__( 'Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testi-item:hover .testi-name h5' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'icon_hover_color',
			[
				'label' => esc_html__( 'Icon Hover Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testi-item:hover .testi-name h5 .svg-inline--fa' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'icon_hover_bg_color',
			[
				'label' => esc_html__( 'Icon Hover BG Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testi-item:hover .testi-name h5 .svg-inline--fa' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .testi-name h5',
			]
		);
		
		$this->add_control(
			'title_background',
			[
				'label' => esc_html__( 'Background Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testi-name' => 'background-color: {{VALUE}}',
				],
			]
		);		

        $this->add_responsive_control(
			'title_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .testi-name h5' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
	  
	  $this->start_controls_section(
			'style_sub_section',
			[
				'label' => esc_html__( 'Designation', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'subtitle_tabs'
		);
		
		$this->start_controls_tab(
			'subtitle_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'subtitle_color',
			[
				'label' => esc_html__( 'Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testi-name p' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'subtitle_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'subtitle_color_hover',
			[
				'label' => esc_html__( 'Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testi-item:hover .testi-name p' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'sub_title_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .testi-name p',
			]
		);

        $this->add_responsive_control(
			'sub_title_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .testi-item:hover .testi-name p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
	  
	  $this->start_controls_section(
			'style_bg_section',
			[
				'label' => esc_html__( 'Description', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'bgtitle_tabs'
		);
		
		$this->start_controls_tab(
			'bgtitle_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'bgtitle_color',
			[
				'label' => esc_html__( 'Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testi-des p' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'bgtitle_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'bgtitle_color_hover',
			[
				'label' => esc_html__( 'Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testi-item:hover .testi-des p' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'bg_title_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .testi-des p',
			]
		);
		
		$this->add_control(
			'bg_border_color',
			[
				'label' => esc_html__( 'Border Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testi-des' => 'border-color: {{VALUE}}',
				],
			]
		);

        $this->add_responsive_control(
			'bg_title_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .testi-des' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
	  $this->end_controls_section();
	  
	  $this->start_controls_section(
			'style_control_section',
			[
				'label' => esc_html__( 'Controls', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'bgcontrol_tabs'
		);
		
		$this->start_controls_tab(
			'bgcontrol_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'bgcontrol_color',
			[
				'label' => esc_html__( 'Dots Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonials-content.owl-carousel button.owl-dot' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'bgcontrol_hover_tab',
			[
				'label' => esc_html__( 'Active', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'bgcontrol_color_hover',
			[
				'label' => esc_html__( 'Dots Active Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .testimonials-content.owl-carousel button.owl-dot.active' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
       
	  $this->end_controls_section();
	  
	  // End Style Section //
    }

    protected function render() {
        $settings = $this->get_settings_for_display();
		
        if ( $settings['list'] ):
		$show_dots = $settings['show_dots'];
		if($show_dots == 'yes'){
			$show_dots = 'true';
		} else {
			$show_dots = 'false';
		}
		
		$rtl_option = $settings['rtl_option'];
		if($rtl_option == 'yes'){
			$rtl_option = 'true';
		} else {
			$rtl_option = 'false';
		}
		
		if($settings['testi_style'] == 'style2'){
			$animateout = 'fadeOut';
			$data_tablet = '1';
		} else {
			$animateout = 'false';
			$data_tablet = '2';
		}
		
		?>
        <div class="testimonials-content testi-carousel owl-carousel" data-items-tablet="<?php echo esc_attr($data_tablet); ?>" data-items="<?php echo esc_attr($settings['number_of_items']); ?>" data-nav="false" data-dots="<?php echo esc_attr($show_dots); ?>" data-margin="30" data-autoplay="true" data-animateout="<?php echo esc_attr($animateout); ?>" data-rtl="<?php echo esc_attr($rtl_option); ?>">           
        	<?php			
			foreach (  $settings['list'] as $item ):
			?>
                <div class="item testi-<?php echo esc_attr($settings['testi_style']); ?>">
                	<?php if($settings['testi_style'] == 'style2'): ?>
                    	<div class="testi-item">
                        <?php if($item['testmonials_image']['url'] != ''): ?>
                        <img src="<?php echo esc_html($item['testmonials_image']['url']); ?>" alt="<?php echo esc_attr($item['author_name']); ?>" />
                        <?php endif; ?>
                            
                    	<?php if($item['testmonials_description'] != ''): ?>
                    	<div class="testi-des">
                        	<p>
                            <?php \Elementor\Icons_Manager::render_icon( $item['description_icon'], [ 'aria-hidden' => 'true' ] ); ?>
                            <?php echo esc_html($item['testmonials_description']); ?>
                            </p>
                        </div>
                        <?php endif; ?>
                        
                    	<div class="testi-name">                        	
                        	<?php if($item['author_name'] != ''): ?>
                        	<h5><?php echo esc_html($item['author_name']); ?></h5>
                            <?php endif; ?>
                            <?php if($item['author_designation'] != ''): ?>
                            <p><?php echo esc_html($item['author_designation']); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                    
					<?php else: ?>
                	<div class="testi-item">
                    	<?php if($item['testmonials_description'] != ''): ?>
                    	<div class="testi-des">
                        	<p><?php echo esc_html($item['testmonials_description']); ?>
                            <?php \Elementor\Icons_Manager::render_icon( $item['description_icon'], [ 'aria-hidden' => 'true' ] ); ?>
                            </p>
                        </div>
                        <?php endif; ?>
                        
                    	<div class="testi-name">
                        	<?php if($item['testmonials_image']['url'] != ''): ?>
                        	<img src="<?php echo esc_html($item['testmonials_image']['url']); ?>" alt="<?php echo esc_attr($item['author_name']); ?>" />
                            <?php endif; ?>
                        	<?php if($item['author_name'] != ''): ?>
                        	<h5><?php echo esc_html($item['author_name']); ?>
                            <?php \Elementor\Icons_Manager::render_icon( $item['author_icon'], [ 'aria-hidden' => 'true' ] ); ?>
                            </h5>
                            <?php endif; ?>
                            <?php if($item['author_designation'] != ''): ?>
                            <p><?php echo esc_html($item['author_designation']); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            <?php endforeach; ?>  
        </div>
        <?php endif; ?>

        <?php
	}

    protected function _content_template() {}
}