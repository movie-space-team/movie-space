<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class CrowdPress_Portfolios extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-portfolios';
    }

    public function get_title() {

        return esc_html__( 'Portfolios', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fas fa-folder';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
	
		$this->add_control(
			'number_of_posts',
			[
				'label' => esc_html__( 'Number of Portfolios', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 48,
				'step' => 1,
				'default' => 12,
			]
		);
		
		$this->add_control(
			'cateogries',
			[
				'label' => esc_html__( 'Select Categories', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => true,
				'options' => $this->get_portfolios_categories(),
				'label_block' => true,
				'multiple'		=> true,
			]			
		);
		
		$this->add_control(
			'order',
			[
				'label' => esc_html__( 'Order', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'DESC',
				'options' => [
					'DESC'  => esc_html__( 'DESC', 'crowdpress' ),
					'ASC' => esc_html__( 'ASC', 'crowdpress' ),
				],
			]
		);
		
		$this->add_control(
			'orderby',
			[
				'label' => esc_html__( 'Order By', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'date',
				'options' => [
					'title'  => esc_html__( 'Title', 'crowdpress' ),
					'date' => esc_html__( 'Date', 'crowdpress' ),
					'ID'  => esc_html__( 'ID', 'crowdpress' ),
					'name'  => esc_html__( 'Name', 'crowdpress' ),
					'rand' => esc_html__( 'Rand', 'crowdpress' ),
					'comment_count'  => esc_html__( 'Comment Count', 'crowdpress' ),
					'menu_order' => esc_html__( 'Menu Order', 'crowdpress' ),					
					'author' => esc_html__( 'Author', 'crowdpress' ),
				],
			]
		);
		
		$this->add_control(
			'show_filter',
			[
				'label' => esc_html__( 'Show Filter', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'crowdpress' ),
				'label_off' => esc_html__( 'Hide', 'crowdpress' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		
		$this->add_control(
			'filter_all_text',
			[
				'label' => esc_html__( 'Filter Text', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'All Projects', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your filter text here', 'crowdpress' ),
			]
		);		
		
		$this->end_controls_section();

		// posts Style Section //
		
		$this->start_controls_section(
			'posts_style_section',
			[
				'label' => esc_html__( 'Title', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'posts_color',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cbp-l-caption-title a' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .cbp-l-caption-title a',
			]
		);
		
		$this->add_control(
			'title_color_hover',
			[
				'label' => esc_html__( 'Title Color Hover', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .cbp-l-caption-title:hover a' => 'color: {{VALUE}}',
				],
			]
		);
       
	  $this->end_controls_section();
		
    }

    protected function render( ) { 
        $settings = $this->get_settings_for_display();
		
		if ( class_exists( 'Ts_Shortcodes' ) ){
		$args = array(
		'post_type' => 'portfolio',
		'posts_per_page' => $settings['number_of_posts'],
		'order'          => $settings['order'],
		'orderby'        => $settings['orderby'],
		);
		
		if(!empty($settings['cateogries'])){
			
			$args['tax_query'] = array(
            array(
				'taxonomy' => 'portfolio_category',
				'terms'    => $settings['cateogries'],
				'field' => 'id',
				),
		    );		
		}
		
		$query = get_posts( $args );
		?>
        
		<div class="portfolios-content">
        	<?php if($settings['show_filter'] == 'yes'): ?>
				<?php
                $args = array(
                    'hide_empty'        => true
                );  
                $terms = get_terms( 'portfolio_category', $args );
                if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
                ?>
                <div id="filters-container" class="cbp-l-filters-button portfolio-filter text-center">
                <div data-filter="*" class="cbp-filter-item-active cbp-filter-item"> <?php echo esc_html($settings['filter_all_text']); ?>
                  <div class="cbp-filter-counter"></div>
                </div>
                <?php
                foreach ( $terms as $term ) {
                ?>
                <div data-filter=".<?php echo esc_attr($term->slug); ?>" class="cbp-filter-item">
                    <?php echo esc_html($term->name); ?>
                    <div class="cbp-filter-counter"></div>
                </div>
                <?php } ?>
              </div>
              <?php } ?>
            <?php endif; ?>
          
			
				<?php
				if ($query): ?>
                <div id="grid-container" class="cbp" data-columnnum="3" data-layout="grid">
                    <?php foreach ( $query as $post ): ?>
                    <?php
					$categories = '';
					$categories2 = '';
					$portfolio_category = get_the_terms( $post->ID, 'portfolio_category' );
					if ( $portfolio_category && ! is_wp_error( $portfolio_category ) ) : 
						$category_class = array();
						$category_class2 = array();
						foreach ( $portfolio_category as $term1 ) {
						$category_class[] = $term1->slug;
						$category_class2[] = $term1->name;
						}							
					$categories = join( " ", $category_class );
					$categories2 = join( ", ", $category_class2 );	
					endif;
					
					$featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');
					
					?>
                    <div class="cbp-item <?php echo esc_attr($categories); ?>">
                        <a href="<?php esc_url(the_permalink($post->ID)); ?>" class="cbp-caption" data-title="<?php echo esc_attr(get_the_title($post->ID)); ?>">
                            <div class="cbp-caption-defaultWrap">
                            	<?php echo get_the_post_thumbnail($post->ID, 'full'); ?>
                            </div>
                            </a>
                            <div class="cbp-caption-activeWrap">
                                <div class="cbp-l-caption-alignLeft">
                                    <div class="cbp-l-caption-body">
                                    	<a href="<?php esc_url(the_permalink($post->ID)); ?>"><i class="fal fa-arrow-right"></i></a>
                                        <div class="cbp-l-caption-desc">
                                        	<?php echo esc_html($categories2); ?>
                                        </div>
                                    	<div class="cbp-l-caption-title"><a href="<?php esc_url(the_permalink($post->ID)); ?>"><?php echo get_the_title($post->ID); ?></a></div>                                    
                                    </div>
                                </div>
                            </div>
                        
                    </div>
                    <?php
					endforeach;
					?>
                </div>
                <?php endif; ?>      
		</div>
    	<?php
		}
    }
    protected function _content_template() {}
	
	public function get_portfolios_categories(){
		$category = [];
		if ( class_exists( 'Ts_Shortcodes' ) ){
		$args = array( 'hide_empty=0' );
		$terms = get_terms( 'portfolio_category', $args );		
		if(!empty($terms)){
			foreach ( $terms as $term ) {
				$category[$term->term_id] = [$term->name];
			}
		}
		}
		return $category;
	}
}