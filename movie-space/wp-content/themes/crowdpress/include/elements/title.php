<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


class CrowdPress_Title extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-title';
    }

    public function get_title() {

        return esc_html__( 'Title', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fas fa-heading';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
		
		$this->add_control(
			'title_style',
			[
				'label' => esc_html__( 'Style', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'style1',
				'options' => [
					'style1'  => esc_html__( 'Style 1', 'crowdpress' ),
					'style2' => esc_html__( 'Style 2', 'crowdpress' ),
				],
			]
		);
	
		$this->add_control(
			'title',
			[
				'label' => esc_html__( 'Title', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'How It Works', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your title here', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'sub_title',
			[
				'label' => esc_html__( 'Sub Title', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Process', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your sub-title here', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'bg_title',
			[
				'label' => esc_html__( 'BG Title', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Process', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your bg-title here', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'description',
			[
				'label' => esc_html__( 'Description', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'rows' => 10,
				'default' => '',
				'placeholder' => esc_html__( 'Type your description here', 'crowdpress' ),
				'condition' => ['title_style' => ['style2']],
			]
		);
		
		$this->add_responsive_control(
            'text_align', [
                'label'          => esc_html__( 'Alignment', 'crowdpress'  ),
                'type'           => Controls_Manager::CHOOSE,
                'options'        => [
    
                    'left'         => [
                        'title'    => esc_html__( 'Left', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-left',
                    ],
                    'center'     => [
                        'title'    => esc_html__( 'Center', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-center',
                    ],
                    'right'         => [
                        'title'     => esc_html__( 'Right', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-right',
                    ],
                ],
               'default'         => 'center',
               'selectors' => [
                   '{{WRAPPER}} .heading-content' => 'text-align: {{VALUE}};'
               ],
            ]
        );
		
		$this->add_control(
			'show_border',
			[
				'label' => esc_html__( 'Show Top Border', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'crowdpress' ),
				'label_off' => esc_html__( 'Hide', 'crowdpress' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);
		
		$this->end_controls_section();

		// Style Section //
		
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Title', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'title_tabs'
		);
		
		$this->start_controls_tab(
			'title_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .heading-content h2' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'title_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'title_color_hover',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .heading-content:hover h2' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .heading-content h2',
			]
		);

        $this->add_responsive_control(
			'title_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .heading-content h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
	  
	  $this->start_controls_section(
			'style_sub_section',
			[
				'label' => esc_html__( 'Sub Title', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'subtitle_tabs'
		);
		
		$this->start_controls_tab(
			'subtitle_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'subtitle_color',
			[
				'label' => esc_html__( 'Sub Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .heading-content .sub-title' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'subtitle_border_color',
			[
				'label' => esc_html__( 'SubTitle Border Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'condition' => ['show_border' => ['yes']],
				'selectors' => [
					'{{WRAPPER}} .heading-content .sub-title:before, {{WRAPPER}} .heading-content .sub-title:after' => 'border-bottom-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'subtitle_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'subtitle_color_hover',
			[
				'label' => esc_html__( 'Sub Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .heading-content:hover .sub-title' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'subtitle_hover_border_color',
			[
				'label' => esc_html__( 'SubTitle Hover Border Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'condition' => ['show_border' => ['yes']],
				'selectors' => [
					'{{WRAPPER}} .heading-content:hover .sub-title:before, {{WRAPPER}} .heading-content:hover .sub-title:after' => 'border-bottom-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'sub_title_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .heading-content .sub-title',
			]
		);

        $this->add_responsive_control(
			'sub_title_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .heading-content .sub-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
	  
	  $this->start_controls_section(
			'style_bg_section',
			[
				'label' => esc_html__( 'BG Title', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'bgtitle_tabs'
		);
		
		$this->start_controls_tab(
			'bgtitle_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'bgtitle_color',
			[
				'label' => esc_html__( 'BG Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .heading-content .bg-title' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'bgtitle_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'bgtitle_color_hover',
			[
				'label' => esc_html__( 'Sub Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .heading-content:hover .bg-title' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'bg_title_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .heading-content .bg-title',
			]
		);

        $this->add_responsive_control(
			'bg_title_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .heading-content .bg-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
	  $this->end_controls_section();
	  
	  // End Style Section //
    }

    protected function render( ) { 
        $settings = $this->get_settings_for_display();
		?>
        
		<div class="heading-content <?php echo esc_attr($settings['text_align']); ?> border-show<?php echo esc_attr($settings['show_border']); ?>">
			<?php if($settings['title_style'] == 'style2'): ?>
            	<?php if($settings['text_align'] == 'left'): ?>
                	<div class="align-bottom">
						<?php if($settings['sub_title'] != ''): ?>
                        <span class="sub-title"><?php echo esc_html($settings['sub_title']); ?></span>
                        <?php endif; ?>
                        <?php if($settings['bg_title'] != ''): ?>
                        <span class="bg-title"><?php echo esc_html($settings['bg_title']); ?></span>
                        <?php endif; ?>
                        <?php if($settings['title'] != ''): ?>
                        <h2><?php echo esc_html($settings['title']); ?></h2>
                        <?php endif; ?>
                        <p><?php echo esc_html($settings['description']); ?></p>
                    </div>
                <?php elseif($settings['text_align'] == 'center'): ?>
            	<div class="row">
                    <div class="col-md-6">
						<?php if($settings['sub_title'] != ''): ?>
                        <span class="sub-title"><?php echo esc_html($settings['sub_title']); ?></span>
                        <?php endif; ?>
                        <?php if($settings['bg_title'] != ''): ?>
                        <span class="bg-title"><?php echo esc_html($settings['bg_title']); ?></span>
                        <?php endif; ?>
                        <?php if($settings['title'] != ''): ?>
                        <h2><?php echo esc_html($settings['title']); ?></h2>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-6">
                    	<p><?php echo esc_html($settings['description']); ?></p>
                    </div>
                </div>
                <?php else: ?>
                	<div class="align-bottom">
						<?php if($settings['sub_title'] != ''): ?>
                        <span class="sub-title"><?php echo esc_html($settings['sub_title']); ?></span>
                        <?php endif; ?>
                        <?php if($settings['bg_title'] != ''): ?>
                        <span class="bg-title"><?php echo esc_html($settings['bg_title']); ?></span>
                        <?php endif; ?>
                        <?php if($settings['title'] != ''): ?>
                        <h2><?php echo esc_html($settings['title']); ?></h2>
                        <?php endif; ?>
                        <p><?php echo esc_html($settings['description']); ?></p>
                    </div>
                <?php endif; ?>
            <?php else: ?>
                <?php if($settings['sub_title'] != ''): ?>
                <span class="sub-title"><?php echo esc_html($settings['sub_title']); ?></span>
                <?php endif; ?>
                <?php if($settings['bg_title'] != ''): ?>
                <span class="bg-title"><?php echo esc_html($settings['bg_title']); ?></span>
                <?php endif; ?>
                <?php if($settings['title'] != ''): ?>
                <h2><?php echo esc_html($settings['title']); ?></h2>
                <?php endif; ?>
            <?php endif; ?>       
		</div>
    	<?php  
    }
    protected function _content_template() {}
}