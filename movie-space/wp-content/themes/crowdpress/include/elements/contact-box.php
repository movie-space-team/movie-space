<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


class CrowdPress_Contact_Box extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-contactbox';
    }

    public function get_title() {

        return esc_html__( 'Contact Box', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fas fa-flag';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
        );

        $this->add_control(
			'contact_icon',
			[
				'label' => esc_html__( 'Icon', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'default' => [
					'value' => 'far fa-map',
					'library' => 'solid',
				],
			]
		);
		
		$this->add_control(
			'contact_title',
			[
				'label' => esc_html__( 'Title', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Office Address', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your title here', 'crowdpress' ),
			]
        );
	
		$this->add_control(
			'contact_text',
			[
				'label' => esc_html__( 'Description', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'default' => esc_html__( '99 NY Address Street, Brooklyn, United State', 'crowdpress' ),
				'placeholder' => esc_html__( 'Enter your description', 'crowdpress' ),
			]
        );        
        
        $this->add_responsive_control(
            'text_align', [
                'label'          => esc_html__( 'Alignment', 'crowdpress'  ),
                'type'           => Controls_Manager::CHOOSE,
                'options'        => [
    
                    'left'         => [
                        'title'    => esc_html__( 'Left', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-left',
                    ],
                    'center'     => [
                        'title'    => esc_html__( 'Center', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-center',
                    ],
                    'right'         => [
                        'title'     => esc_html__( 'Right', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-right',
                    ],
                ],
               'default'         => 'center',
               'selectors' => [
                   '{{WRAPPER}} .contact-box-content' => 'text-align: {{VALUE}};'
               ],
            ]
        );
 

        $this->add_responsive_control(
			'padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .contact-box-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
		$this->end_controls_section();
		
		// Icon Style Section //
		
		$this->start_controls_section(
			'icon_style_section',
			[
				'label' => esc_html__( 'Icon', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-icon' => 'color: {{VALUE}}',
				],
			]
		);

        $this->add_responsive_control(
			'icon_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .contact-icon' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
	  $this->end_controls_section();
	  
	  // Number Style Section //
		
		$this->start_controls_section(
			'text_style_section',
			[
				'label' => esc_html__( 'Text', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'text_color',
			[
				'label' => esc_html__( 'Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-box-content h5' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'text_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .contact-box-content h5',
			]
		);

        $this->add_responsive_control(
			'text_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .contact-box-content h5' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
	  $this->end_controls_section();

	  // Title Style Section //
		
		$this->start_controls_section(
			'des_style_section',
			[
				'label' => esc_html__( 'Description', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'des_color',
			[
				'label' => esc_html__( 'Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-box-content p' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'des_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .contact-box-content p',
			]
		);

        $this->add_responsive_control(
			'des_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .contact-box-content p' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
	  $this->end_controls_section();
	  
	  // End Style Section //
	  
	  // Title Style Section //
		
		$this->start_controls_section(
			'hover_style_section',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'border_color',
			[
				'label' => esc_html__( 'Border Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .contact-box-content' => 'border-color: {{VALUE}}',
				],
			]
		);
       
	  $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display();
        ?>
        <div class="contact-box-content">
            <div class="contact-icon">
            <span><i class="fal fa-check"></i></span>
			<?php \Elementor\Icons_Manager::render_icon( $settings['contact_icon'], [ 'aria-hidden' => 'true' ] ); ?>
            </div>
            
            <h5><?php echo esc_html($settings['contact_title']); ?></h5>
            <p class="contact-des"><?php echo wp_kses($settings['contact_text'],  array('br'=>array())); ?></p>     
        </div>

        <?php
	}

    protected function _content_template() {}
}