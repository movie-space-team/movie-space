<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class CrowdPress_Events extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-events';
    }

    public function get_title() {

        return esc_html__( 'Events', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fas fa-folder';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
	
		$this->add_control(
			'number_of_posts',
			[
				'label' => esc_html__( 'Number of Events', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 48,
				'step' => 1,
				'default' => 4,
			]
		);
		
		$this->add_control(
			'cateogries',
			[
				'label' => esc_html__( 'Select Categories', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => true,
				'options' => $this->get_events_categories(),
				'label_block' => true,
				'multiple'		=> true,
			]			
		);
		
		$this->add_control(
			'order',
			[
				'label' => esc_html__( 'Order', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'DESC',
				'options' => [
					'DESC'  => esc_html__( 'DESC', 'crowdpress' ),
					'ASC' => esc_html__( 'ASC', 'crowdpress' ),
				],
			]
		);
		
		$this->add_control(
			'orderby',
			[
				'label' => esc_html__( 'Order By', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'date',
				'options' => [
					'title'  => esc_html__( 'Title', 'crowdpress' ),
					'date' => esc_html__( 'Date', 'crowdpress' ),
					'ID'  => esc_html__( 'ID', 'crowdpress' ),
					'name'  => esc_html__( 'Name', 'crowdpress' ),
					'rand' => esc_html__( 'Rand', 'crowdpress' ),
					'comment_count'  => esc_html__( 'Comment Count', 'crowdpress' ),
					'menu_order' => esc_html__( 'Menu Order', 'crowdpress' ),					
					'author' => esc_html__( 'Author', 'crowdpress' ),
				],
			]
		);
		
		
		$this->add_control(
			'buy_button_text',
			[
				'label' => esc_html__( 'Buy Button Text', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Get Ticket', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your buy button text here', 'crowdpress' ),
			]
		);		
		
		$this->end_controls_section();

		// Style Section //
		
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Title', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'title_tabs'
		);
		
		$this->start_controls_tab(
			'title_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .events-loop h4, .events-loop h4 a' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'title_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'title_color_hover',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .events-loop h4 a:hover' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .events-loop h4 a',
			]
		);

        $this->add_responsive_control(
			'title_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .events-loop h4' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
	  
	  // Time style Section //
		
		$this->start_controls_section(
			'time_style_section',
			[
				'label' => esc_html__( 'Time', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'time_color',
			[
				'label' => esc_html__( 'Date Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .events-loop .event-date-img .event-date' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'month_color',
			[
				'label' => esc_html__( 'Month Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .events-loop .event-date-img .event-month' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'time_typography',
				'label' => esc_html__( 'Date Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .events-loop .event-date-img .event-date',
			]
		);
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'month_typography',
				'label' => esc_html__( 'Month Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .events-loop .event-date-img .event-month',
			]
		);

        $this->add_responsive_control(
			'time_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .events-loop .event-date-img' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
	  
	  // Button Style Section //
		
		$this->start_controls_section(
			'button_style_section',
			[
				'label' => esc_html__( 'Button', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'button_tabs'
		);
		
		$this->start_controls_tab(
			'button_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'button_color',
			[
				'label' => esc_html__( 'Button Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .events-loop .buy-tickets-btn' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'button_bg_color',
			[
				'label' => esc_html__( 'Button BG Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .events-loop .buy-tickets-btn' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'button_border_color',
			[
				'label' => esc_html__( 'Button Border Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .events-loop .buy-tickets-btn' => 'border-color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'button_icon_color',
			[
				'label' => esc_html__( 'Button Icon Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .events-loop .buy-tickets-btn i' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'button_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'button_color_hover',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .events-loop .buy-tickets-btn:hover' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'button_hover_bg_color',
			[
				'label' => esc_html__( 'Button BG Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .events-loop .buy-tickets-btn:hover' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'button_hover_border_color',
			[
				'label' => esc_html__( 'Button Border Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .events-loop .buy-tickets-btn:hover' => 'border-color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'button_hover_icon_color',
			[
				'label' => esc_html__( 'Button Icon Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .events-loop .buy-tickets-btn:hover i' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'button_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .events-loop .buy-tickets-btn',
			]
		);

        $this->add_responsive_control(
			'button_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .events-loop .buy-tickets-btn' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
		
    }

    protected function render( ) { 
        $settings = $this->get_settings_for_display();
		
		$args = array(
		'post_type' => 'tribe_events',
		'posts_per_page' => $settings['number_of_posts'],
		'order'          => $settings['order'],
		'orderby'        => $settings['orderby'],
		);
		
		if(!empty($settings['cateogries'])){
			
			$args['tax_query'] = array(
            array(
				'taxonomy' => 'tribe_events_cat',
				'terms'    => $settings['cateogries'],
				'field' => 'id',
				),
		    );		
		}
		
		$query = get_posts( $args );
		?>
        
		<div class="events-content">
				<?php
				if ($query): ?>
                
                    <?php foreach ( $query as $post ):				
					$featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');					
					?>
                    <div class="events-loop">
                        <div class="row align-items-center">
                            <div class="col-lg-2 col-md-3 col-5">
                                <div class="event-date-img" style="background:url(<?php echo esc_url($featured_img_url); ?>);">
                                    <span class="event-date">
                                        <?php echo tribe_get_start_date( null, false, 'd' ); ?>
                                    </span>
                                    <span class="event-month">
                                        <?php echo tribe_get_start_date( null, false, 'M' ); ?>
                                    </span>
                                </div>                        
                            </div>
                            
                            <div class="col-lg-4 col-md-6 col-7">
                                <h4><a href="<?php esc_url(the_permalink($post->ID)); ?>"><?php echo get_the_title($post->ID); ?></a></h4>
                            </div>
                            <div class="col-lg-3 col-md-3 col-6">
                            <h4 class="location-meta"><?php echo esc_html__('Location', 'crowdpress'); ?></h4>
                            <p><?php echo tribe_get_address( $post->ID ); ?></p>
                            </div>
                            <div class="col-lg-3 col-md-12 col-6">
                                <a href="<?php esc_url(the_permalink($post->ID)); ?>" class="buy-tickets-btn"><i class="fal fa-ticket"></i><?php echo esc_html($settings['buy_button_text']); ?></a>
                            </div>
                        </div>
                    </div>
                    <?php
					endforeach;
					?>
                
                <?php endif; ?>      
		</div>
    	<?php
    }
    protected function _content_template() {}
	
	public function get_events_categories(){
		$category = [];
		$args = array( 'hide_empty=0' );
		$terms = get_terms( 'tribe_events_cat', $args );		
		if(!empty($terms)){
			foreach ( $terms as $term ) {
				$category[$term->term_id] = [$term->name];
			}
		}
		return $category;
	}
}