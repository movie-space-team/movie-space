<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class CrowdPress_Posts extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-posts';
    }

    public function get_title() {

        return esc_html__( 'Posts', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fas fa-folder';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);
		
		$this->add_control(
			'posts_style',
			[
				'label' => esc_html__( 'Style', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'style1',
				'options' => [
					'style1'  => esc_html__( 'Style 1', 'crowdpress' ),
					'style2' => esc_html__( 'Style 2', 'crowdpress' ),
					'style3' => esc_html__( 'Style 3', 'crowdpress' ),
				],
			]
		);
	
		$this->add_control(
			'number_of_posts',
			[
				'label' => esc_html__( 'Number of Posts', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 48,
				'step' => 1,
				'default' => 3,
			]
		);
		
		$this->add_control(
			'cateogries',
			[
				'label' => esc_html__( 'Select Categories', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT2,
				'multiple' => true,
				'options' => $this->get_post_categories(),
				'label_block' => true,
				'multiple'		=> true,
			]			
		);
		
		$this->add_control(
			'order',
			[
				'label' => esc_html__( 'Order', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'DESC',
				'options' => [
					'DESC'  => esc_html__( 'DESC', 'crowdpress' ),
					'ASC' => esc_html__( 'ASC', 'crowdpress' ),
				],
			]
		);
		
		$this->add_control(
			'orderby',
			[
				'label' => esc_html__( 'Order By', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'date',
				'options' => [
					'title'  => esc_html__( 'Title', 'crowdpress' ),
					'date' => esc_html__( 'Date', 'crowdpress' ),
					'ID'  => esc_html__( 'ID', 'crowdpress' ),
					'name'  => esc_html__( 'Name', 'crowdpress' ),
					'rand' => esc_html__( 'Rand', 'crowdpress' ),
					'comment_count'  => esc_html__( 'Comment Count', 'crowdpress' ),
					'menu_order' => esc_html__( 'Menu Order', 'crowdpress' ),					
					'author' => esc_html__( 'Author', 'crowdpress' ),
				],
			]
		);
		
		$this->add_control(
			'show_featured_img',
			[
				'label' => esc_html__( 'Show Featured Image', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'crowdpress' ),
				'label_off' => esc_html__( 'Hide', 'crowdpress' ),
				'return_value' => 'yes',
				'default' => 'yes',
			]
		);		
		
		
		$this->end_controls_section();

		// Style Section //
		
		$this->start_controls_section(
			'style_section',
			[
				'label' => esc_html__( 'Title', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'title_tabs'
		);
		
		$this->start_controls_tab(
			'title_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .blog-des-title a' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'title_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'title_color_hover',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-loop:hover .blog-des-title a' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .blog-des-title a',
			]
		);

        $this->add_responsive_control(
			'title_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .blog-des-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
	  
	  // Content Style Section //
		
		$this->start_controls_section(
			'style_Content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'content_tabs'
		);
		
		$this->start_controls_tab(
			'content_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'content_color',
			[
				'label' => esc_html__( 'Content Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-content .rel-blog-desc p' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'content_bg_color',
			[
				'label' => esc_html__( 'Content BG Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-content .rel-blog-desc' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'content_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'content_color_hover',
			[
				'label' => esc_html__( 'Content Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-content .posts-loop:hover .rel-blog-desc p' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'content_hover_bg_color',
			[
				'label' => esc_html__( 'Content BG Hover Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-content .posts-loop:hover .rel-blog-desc' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'content_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .posts-content .rel-blog-desc p',
			]
		);

        $this->add_responsive_control(
			'content_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .posts-content .rel-blog-desc' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
	  
	  // Meta Style Section //
		
		$this->start_controls_section(
			'style_meta_section',
			[
				'label' => esc_html__( 'Meta', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'meta_tabs'
		);
		
		$this->start_controls_tab(
			'meta_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'meta_color',
			[
				'label' => esc_html__( 'Meta Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-content .blog-meta a' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'meta_icon_color',
			[
				'label' => esc_html__( 'Meta Icon Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-content .blog-meta a .svg-inline--fa' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'meta_bg_color',
			[
				'label' => esc_html__( 'Meta BG Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-content .blog-meta' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'meta_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'meta_color_hover',
			[
				'label' => esc_html__( 'Meta Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-content .posts-loop:hover .blog-meta a' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'meta_hove_icon_color',
			[
				'label' => esc_html__( 'Meta Icon Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-content .posts-loop:hover .blog-meta a .svg-inline--fa' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'meta_hover_bg_color',
			[
				'label' => esc_html__( 'Meta BG Hover Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-content .posts-loop:hover .blog-meta' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'meta_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .posts-content .blog-meta a',
			]
		);

        $this->add_responsive_control(
			'meta_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .posts-content .blog-meta' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
	  
	  // Read More Style Section //
		
		$this->start_controls_section(
			'read_more_section',
			[
				'label' => esc_html__( 'Read More', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'read_more_tabs'
		);
		
		$this->start_controls_tab(
			'read_more_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'read_more_color',
			[
				'label' => esc_html__( 'Read More Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-loop .read-more-link' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'read_more_bg_color',
			[
				'label' => esc_html__( 'Read More BG Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-loop .read-more-link' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'read_more_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'readmore_color_hover',
			[
				'label' => esc_html__( 'Read More Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-loop:hover .read-more-link' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'read_more_hover_bg_color',
			[
				'label' => esc_html__( 'Read More Hover BG Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-loop:hover .read-more-link' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();

        $this->add_responsive_control(
			'read_more_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .posts-loop .read-more-link' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
	  
	  // Category Style Section //
		
		$this->start_controls_section(
			'category_section',
			[
				'label' => esc_html__( 'Category', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'category_tabs'
		);
		
		$this->start_controls_tab(
			'category_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'category_color',
			[
				'label' => esc_html__( 'Category Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-loop .cat-readmore-con .text-right a' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'category_bg_color',
			[
				'label' => esc_html__( 'Category BG Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-loop .cat-readmore-con .text-right a' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'category_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'category_color_hover',
			[
				'label' => esc_html__( 'Category Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-loop:hover .cat-readmore-con .text-right a' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'category_hover_bg_color',
			[
				'label' => esc_html__( 'Category Hover BG Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .posts-loop:hover .cat-readmore-con .text-right a' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'category_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .posts-loop .cat-readmore-con .text-right a',
			]
		);

        $this->add_responsive_control(
			'category_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .posts-loop .cat-readmore-con .text-right a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
	  
	  $this->add_responsive_control(
			'category_borderradius',
			[
				'label' =>esc_html__( 'Border Radius', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .posts-loop .cat-readmore-con .text-right a' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
	  
	  $this->add_responsive_control(
			'category_margin',
			[
				'label' =>esc_html__( 'Margin', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .posts-content .cat-readmore-con .text-right a' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
      $this->end_controls_section();
		
    }

    protected function render( ) { 
        $settings = $this->get_settings_for_display();
		
		$args = array(
		'posts_per_page' => $settings['number_of_posts'],
		'order'          => $settings['order'],
		'orderby'        => $settings['orderby'],
		);
		
		if(!empty($settings['cateogries'])){
			
			$args['tax_query'] = array(
            array(
                'taxonomy' => 'category',
                'terms'    => $settings['cateogries'],
                'field' => 'id',
                 ),
               );		
		} 
		
		$query = get_posts( $args );
		?>
        
		<div class="posts-content">
			<?php if($settings['posts_style'] == 'style2'): ?>
            	<?php
				if ($query): ?>
                <div class="row low-padding justify-content-center">
                    <?php foreach ( $query as $post ): ?>
                    <div class="col-md-6 col-lg-4 col-xl-3 posts-style2">
                    	<div class="posts-loop">
                    	<?php if(has_post_thumbnail($post->ID) && $settings['show_featured_img'] == 'yes'): ?>
                        <div class="blog-media">
                            <a href="<?php esc_url(the_permalink($post->ID)); ?>">
                            <?php echo get_the_post_thumbnail( $post->ID, array( 600, 525) ); ?>
                            </a>
                            <div class="cat-readmore-con text-right">
                                <?php
                                $post_categories = wp_get_post_categories( $post->ID );							 
                                foreach($post_categories as $c){
                                    $cat = get_category( $c );
                                    ?>
                                    <a href="<?php echo esc_url( get_category_link( $cat->term_id ) ) ?>"><?php echo esc_html($cat->name); ?></a>
                                    <?php
                                }
                                ?>
                            </div>                        
                    	</div>
                        <?php endif; ?>
                        
                        <div class="rel-blog-desc">                        
                            <h4 class="blog-des-title"><a href="<?php esc_url(the_permalink($post->ID)); ?>"><?php echo wp_trim_words( get_the_title($post->ID), 6, '' ); ?></a></h4>
                            <p><?php echo wp_trim_words( get_the_content('', '', $post->ID), 17, '' ); ?></p>
                            <div class="blog-meta">
                                <a href="<?php esc_url(the_permalink($post->ID)); ?>"><i class="far fa-calendar-alt"></i><?php echo get_the_date(get_option( 'date_format' ), $post->ID); ?></a>
                            </div>
                        </div>
                        </div>                 
                    </div>
                    <?php
					endforeach;
					?>
                </div>
                <?php endif; ?>            	
            <?php else: ?>
				<?php
				if ($query): ?>
                <div class="row justify-content-center">
                    <?php foreach ( $query as $post ): ?>
                    <div class="col-md-6 col-lg-4 posts-loop posts-loop-style1">
                    	<?php if(has_post_thumbnail($post->ID) && $settings['show_featured_img'] == 'yes'): ?>
                        <div class="blog-media">
                            <a href="<?php esc_url(the_permalink($post->ID)); ?>">
                            <?php
							$featured_img_url = get_the_post_thumbnail_url($post->ID, 'full');
							$img_src = crowdpress_image_resize($featured_img_url , 740, 480);
							?>
                            <img src="<?php echo esc_url($img_src); ?>" alt="<?php echo esc_attr(get_the_title($post->ID)); ?>" />
                            </a>                                                    
                    	</div>
                        <?php endif; ?>
                        
                        <div class="rel-blog-desc">
                        	<?php if($settings['posts_style'] == 'style3'): ?>
                        	<div class="cat-readmore-con row">
                            	<div class="col-3">
                                <a class="read-more-link" href="<?php esc_url(the_permalink($post->ID)); ?>"><i class="fal fa-arrow-right"></i></a>
                                </div>
                                <div class="col-9 text-right">
                                <?php
                                $post_categories = wp_get_post_categories( $post->ID );							 
                                foreach($post_categories as $c){
                                    $cat = get_category( $c );
                                    ?>
                                    <a href="<?php echo esc_url( get_category_link( $cat->term_id ) ) ?>"><?php echo esc_html($cat->name); ?></a>
                                    <?php
                                }
                                ?>
                                </div>
                            </div>
                            <?php endif; ?>
                        	
                            <div class="blog-meta">
                            	<?php $author_id = $post->post_author; ?>
                            	<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID', $author_id ) ) ); ?>"><i class="far fa-user"></i><?php echo esc_html__( 'By ', 'crowdpress' ); ?><?php echo get_the_author_meta('display_name', $author_id); ?></a>
                                <a href="<?php esc_url(the_permalink($post->ID)); ?>"><i class="far fa-calendar-alt"></i><?php echo get_the_date(get_option( 'date_format' ), $post->ID); ?></a>
                            </div>
                            
                            <h4 class="blog-des-title"><a href="<?php esc_url(the_permalink($post->ID)); ?>"><?php echo wp_trim_words( get_the_title($post->ID), 8, '' ); ?></a></h4>
                            
                            <a href="<?php esc_url(the_permalink($post->ID)); ?>" class="blog-read-more"><?php echo esc_html__('Read More', 'crowdpress'); ?></a>
                            
                        </div>
                                          
                    </div>
                    <?php
					endforeach;
					?>
                </div>
                <?php endif; ?>
            <?php endif; ?>       
		</div>
    	<?php  
    }
    protected function _content_template() {}
	
	public function get_post_categories(){
		$args = array( 'hide_empty=0' );
		$terms = get_terms( 'category', $args );
		$category = [];
		foreach ( $terms as $term ) {
			$category[$term->term_id] = [$term->name];
		}
		return $category;
	}
}