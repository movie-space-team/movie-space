<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


class CrowdPress_Team extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-team';
    }

    public function get_title() {

        return esc_html__( 'Team', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fas fa-comments';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
        );
		
		$this->add_control(
			'team_image',
			[
				'label' => esc_html__( 'Image', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
			]
        );
        
        $this->add_control(
			'author_name',
			[
				'label' => esc_html__( 'Name', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Rosalina D. William', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your title here', 'crowdpress' ),
			]
        );
		
		$this->add_control(
			'team_designation',
			[
				'label' => esc_html__( 'Designation', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Founder of Mirax Co.', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your designation', 'crowdpress' ),
			]
        );
		
		$this->add_control(
			'team_icon',
			[
				'label' => esc_html__( 'Icon', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'default' => [
					'value' => 'fas fa-star',
					'library' => 'solid',
				],
			]
		);
		
		$repeater = new \Elementor\Repeater();
        
        $repeater->add_control(
			'social_name',
			[
				'label' => esc_html__( 'Social Name', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Facebook', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your title here', 'crowdpress' ),
			]
        );

        $repeater->add_control(
			'social_icon',
			[
				'label' => esc_html__( 'Social Icon', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'default' => [
					'value' => 'fab fa-facebook',
					'library' => 'solid',
				],
			]
		);
		
		$repeater->add_control(
			'social_link',
			[
				'label' => esc_html__( 'Link', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://', 'crowdpress' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);
		
		$this->add_control(
			'list',
			[
				'label' => esc_html__( 'Social List', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'social_name' => esc_html__( 'Facebook', 'crowdpress' ),
						'social_link' => esc_html__( '#', 'crowdpress' ),
						'social_icon' => esc_html__( 'Social Icon', 'crowdpress' ),
					],
				],
				'title_field' => '{{{ social_name }}}',
			]
		);
        
        $this->add_responsive_control(
            'text_align', [
                'label'          => esc_html__( 'Alignment', 'crowdpress'  ),
                'type'           => Controls_Manager::CHOOSE,
                'options'        => [
    
                    'left'         => [
                        'title'    => esc_html__( 'Left', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-left',
                    ],
                    'center'     => [
                        'title'    => esc_html__( 'Center', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-center',
                    ],
                    'right'         => [
                        'title'     => esc_html__( 'Right', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-right',
                    ],
                ],
               'default'         => '',
               'selectors' => [
                   '{{WRAPPER}} .team-content' => 'text-align: {{VALUE}};'
               ],
            ]
        );
 

        $this->add_responsive_control(
			'padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .team-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
        $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display();
		
        
		?>
        <div class="team-content">
        	<?php if($settings['team_image']['url'] != ''): ?>
            <img src="<?php echo esc_html($settings['team_image']['url']); ?>" alt="<?php echo esc_attr($settings['author_name']); ?>" />
            <?php endif; ?>
        	<div class="team-name">
            	<?php if ( $settings['list'] ): ?>
                <span><?php \Elementor\Icons_Manager::render_icon( $settings['team_icon'], [ 'aria-hidden' => 'true' ] ); ?></span>
                <ul class="team-social-icons">           
                <?php			
                foreach (  $settings['list'] as $item ):
                    $target = $item['social_link']['is_external'] ? ' target="_blank"' : '';
                    $nofollow = $item['social_link']['nofollow'] ? ' rel="nofollow"' : ''; ?>
                    <?php if($item['social_name'] != ''): ?>
                    <li><a title="<?php echo esc_attr($item['social_name']); ?>" class="text-center" href="<?php echo esc_url($item['social_link']['url']); ?>" <?php echo esc_attr($target); ?> <?php echo esc_attr($nofollow); ?>><?php \Elementor\Icons_Manager::render_icon( $item['social_icon'], [ 'aria-hidden' => 'true' ] ); ?></a></li>                
                    <?php endif; ?>                
                <?php endforeach; ?>
                </ul>
                <?php endif; ?>
                        	
            	<?php if($settings['team_designation'] != ''): ?>
                <p><?php echo esc_html($settings['team_designation']); ?></p>
                <?php endif; ?>
                
                <?php if($settings['author_name'] != ''): ?>
                <h5><?php echo esc_html($settings['author_name']); ?></h5>
                <?php endif; ?>                
            </div> 
        </div>
		<?php
	}

    protected function _content_template() {}
}