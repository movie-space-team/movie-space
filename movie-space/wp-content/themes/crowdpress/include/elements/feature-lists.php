<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


class CrowdPress_Feature_Lists extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-feature-lists';
    }

    public function get_title() {

        return esc_html__( 'Feature Lists', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fas fa-handshake';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
        );
		
		$repeater = new \Elementor\Repeater();
	
		$repeater->add_control(
			'title',
			[
				'label' => esc_html__( 'Title', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Feature 1', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your feature title here', 'crowdpress' ),
				'label_block' => true,
			]
        );
		
		$repeater->add_control(
			'feature_icon_class',
			[
				'label' => esc_html__( 'Icon Class', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Paste font awesome icon class', 'crowdpress' ),
			]
        );
		
		$repeater->add_control(
			'feature_icon',
			[
				'label' => esc_html__( 'Icon', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'default' => [
					'value' => 'fas fa-check',
					'library' => 'solid',
				],
			]
		);
		
		$this->add_control(
			'list',
			[
				'label' => esc_html__( 'Feature List', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::REPEATER,
				'fields' => $repeater->get_controls(),
				'default' => [
					[
						'title' => esc_html__( 'Title #1', 'crowdpress' ),
						'feature_icon_class' => esc_html__( 'Icon Class', 'crowdpress' ),
						'feature_icon' => esc_html__( 'Icon', 'crowdpress' ),
					],
				],
				'title_field' => '{{{ title }}}',
			]
		);

		$this->add_control(
			'content_image',
			[
				'label' => esc_html__( 'Content Image', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
				'label_block' => true,
			]
		);
		
		$this->add_responsive_control(
			'feature_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .featured-lists-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
       
        $this->end_controls_section();
		
		// Icon Style Section //
		
		$this->start_controls_section(
			'icon_style_section',
			[
				'label' => esc_html__( 'Style', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'icon_text_color',
			[
				'label' => esc_html__( 'Icon Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .featured-lists-content .featured-main-content i' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'BG Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .featured-lists-content .featured-main-content:before' => 'background-color: {{VALUE}}',
				],
			]
		);
		
		$this->add_control(
			'icon_bg_color',
			[
				'label' => esc_html__( 'Icon BG Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .featured-lists-content .services_item-icon:before, {{WRAPPER}} .featured-lists-content .services_item-icon:after' => 'background-color: {{VALUE}}',
				],
			]
		);
       
	  $this->end_controls_section();
    }

    protected function render() {
        $settings = $this->get_settings_for_display();
		
		if ( $settings['list'] ):
		$img_src = crowdpress_image_resize($settings['content_image']['url'] , 540, 540);
		?>
        <div class="featured-lists-content">
        	<div class="featured-main-content">           
				<?php			
                foreach (  $settings['list'] as $item ):
				
                ?>
                <div class="services_item-wrap">
                    <div class="services_item-icon">
                        <?php if ( $item['feature_icon_class'] !=''): ?>
                        <span class="services_icon"><i class="<?php echo esc_attr($item['feature_icon_class']); ?>"></i></span>
                        <?php else: ?>
                        <span class="services_icon"><?php \Elementor\Icons_Manager::render_icon( $item['feature_icon']); ?></span>                       
                        <?php endif; ?>
                    </div>
                    <div class="services_item-content">
                        <img class="content-img" src="<?php echo esc_url($img_src); ?>" alt="<?php echo esc_attr($item['title']); ?>" />
                    </div>
                </div>
                <?php endforeach; ?>  
        	</div>
        </div>
        <?php endif; ?>

        <?php
	}

    protected function _content_template() {}
}