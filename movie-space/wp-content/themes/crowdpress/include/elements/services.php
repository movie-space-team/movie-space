<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


class CrowdPress_Services extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-services';
    }

    public function get_title() {

        return esc_html__( 'Services', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fas fa-grip-horizontal';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
        );
		
		$this->add_control(
			'posts_style',
			[
				'label' => esc_html__( 'Style', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'style1',
				'options' => [
					'style1'  => esc_html__( 'Style 1', 'crowdpress' ),
					'style2' => esc_html__( 'Style 2', 'crowdpress' ),
					'style3' => esc_html__( 'Style 3', 'crowdpress' ),
					'style4' => esc_html__( 'Style 4', 'crowdpress' ),
				],
			]
		);
        
        $this->add_control(
			'number',
			[
				'label' => esc_html__( 'Number', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 100,
				'step' => 1,
				'default' => 01,
				'condition' => ['posts_style' => ['style2', 'style1', 'style4']],
			]
		);
	
		$this->add_control(
			'title',
			[
				'label' => esc_html__( 'Title', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Add Your Listing', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your title here', 'crowdpress' ),
			]
        );
		
		$this->add_control(
			'read_more_text',
			[
				'label' => esc_html__( 'Read More Text', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Contact Us', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type read more text here', 'crowdpress' ),
				'condition' => ['posts_style' => ['style2']],
			]
        );
		
		$this->add_control(
			'read_more_link',
			[
				'label' => esc_html__( 'Read More Link', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://', 'crowdpress' ),
				'show_external' => false,
				'default' => [
					'url' => '',
					'is_external' => false,
					'nofollow' => true,
				],
				'condition' => ['posts_style' => ['style2']],
			]
		);
        
        $this->add_control(
			'services_description',
			[
				'label' => esc_html__( 'Description', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXTAREA,
				'rows' => 10,
				'default' => esc_html__( 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. ', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your description here', 'crowdpress' ),
			]
        );

        $this->add_control(
			'services_image',
			[
				'label' => esc_html__( 'Choose Image', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'condition' => ['posts_style' => ['style1', 'style2']],
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
			]
		);
		
		$this->add_control(
			'service_icon_class',
			[
				'label' => esc_html__( 'Icon Class', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Paste font awesome icon class', 'crowdpress' ),
				'condition' => ['posts_style' => ['style3']],
			]
        );
		
		$this->add_control(
			'serviceicon',
			[
				'label' => esc_html__( 'Icon', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'condition' => ['posts_style' => ['style3', 'style4']],
				'default' => [
					'value' => 'fas fa-check',
					'library' => 'solid',
				],
			]
		);
		
		$this->add_control(
			'serviceicon_hover',
			[
				'label' => esc_html__( 'Icon Hover', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'condition' => ['posts_style' => ['style4']],
				'default' => [
					'value' => 'fas fa-check',
					'library' => 'solid',
				],
			]
		);
        
        $this->add_responsive_control(
            'text_align', [
                'label'          => esc_html__( 'Alignment', 'crowdpress'  ),
                'type'           => Controls_Manager::CHOOSE,
                'options'        => [
    
                    'left'         => [
                        'title'    => esc_html__( 'Left', 'crowdpress'  ),
                        'icon'     => 'fa fa-align-left',
                    ],
                    'center'     => [
                        'title'    => esc_html__( 'Center', 'crowdpress'  ),
                        'icon'     => 'fa fa-align-center',
                    ],
                    'right'         => [
                        'title'     => esc_html__( 'Right', 'crowdpress'  ),
                        'icon'     => 'fa fa-align-right',
                    ],
                ],
               'default'         => 'center',
               'selectors' => [
                   '{{WRAPPER}} .services-content' => 'text-align: {{VALUE}};'
               ],
            ]
        );
 

        $this->add_responsive_control(
			'padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .services-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
	  
	  $this->add_control(
			'active_style',
			[
				'label' => esc_html__( 'Active', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Yes', 'crowdpress' ),
				'label_off' => esc_html__( 'No', 'crowdpress' ),
				'default' => 'no',
			]
		);
		
		$this->add_responsive_control(
			'border_width',
			[
				'label' =>esc_html__( 'Boeder Width', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .services-content' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
        );
       
        $this->end_controls_section();

        // Number style tab //
		
		$this->start_controls_section(
			'style_number_section',
			[
				'label' => esc_html__( 'Number', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->add_control(
			'number_color',
			[
				'label' => esc_html__( 'Number Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .services-content .number' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'number_border_color',
			[
				'label' => esc_html__( 'Number Border Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .services-content .number' => 'border-color: {{VALUE}}',
				],
			]
		);
       
      $this->end_controls_section();

      		// Title style tab //
		
		$this->start_controls_section(
			'title_style_section',
			[
				'label' => esc_html__( 'Title', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'title_tabs'
		);
		
		$this->start_controls_tab(
			'title_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .services-content h3' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'title_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'title_color_hover',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .services-content:hover h3' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
				'selector' => '{{WRAPPER}} .heading-content h2',
			]
		);

        $this->add_responsive_control(
			'title_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .heading-content h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
       
	  $this->end_controls_section();
	  
	  // Description Style //
		$this->start_controls_section(
			'description_style_section',
			[
				'label' => esc_html__( 'Description', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'description_tabs'
		);
		
		$this->start_controls_tab(
			'description_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'description_color',
			[
				'label' => esc_html__( 'Description Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .services-content p' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'description_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'description_color_hover',
			[
				'label' => esc_html__( 'Description Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .services-content:hover p' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_control(
			'circle_color',
			[
				'label' => esc_html__( 'Circle Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'condition' => ['posts_style' => ['style4']],
				'selectors' => [
					'{{WRAPPER}} .services-content.style4 .default-icon:after' => 'background: {{VALUE}}',
				],
			]
		);

		$this->end_controls_section();

    }

    protected function render() {
        $settings = $this->get_settings_for_display();
        ?>
        <div class="services-content <?php echo esc_attr($settings['posts_style']); ?> active-<?php echo esc_attr($settings['active_style']); ?>">
        	
			<?php if($settings['number'] != ''): ?>
            	<span class="number">0<?php echo esc_html($settings['number']); ?></span>
            <?php endif; ?>
            <?php if($settings['posts_style'] == 'style2'):
				$target = $settings['read_more_link']['is_external'] ? ' target="_blank"' : '';
				$nofollow = $settings['read_more_link']['nofollow'] ? ' rel="nofollow"' : '';
				if($settings['read_more_text'] != ''):
			?>
            	<a href="<?php echo esc_url($settings['read_more_link']['url']); ?>" <?php echo esc_attr($target); ?> <?php echo esc_attr($nofollow); ?>><i class="fal fa-arrow-right"></i> <?php echo esc_html($settings['read_more_text']); ?></a>
                <?php endif; ?>
            <?php endif; ?>
            
            <?php if($settings['posts_style'] == 'style3'): ?>
            <h3>
            
            <?php if($settings['service_icon_class'] != ''): ?>
            	<span><i class="<?php echo esc_attr($settings['service_icon_class']); ?>"></i></span>
            <?php else: ?>
            	<span><?php \Elementor\Icons_Manager::render_icon( $settings['serviceicon']); ?></span>
            <?php endif; ?>
            <?php echo esc_html($settings['title']); ?></h3>
            <?php else: ?>
            <?php if($settings['title'] != ''): ?>
            	<h3><?php echo esc_html($settings['title']); ?></h3>
            <?php endif; ?>
            <?php endif; ?>
            <?php if($settings['services_description'] != ''): ?>
            	<p><?php echo esc_html($settings['services_description']); ?></p>
            <?php endif; ?>
            <?php if($settings['posts_style'] != 'style3'): ?>
            <?php if($settings['posts_style'] == 'style4'): ?>
            <span class="default-icon"><?php \Elementor\Icons_Manager::render_icon( $settings['serviceicon']); ?></span>
            <span class="hover-icon"><?php \Elementor\Icons_Manager::render_icon( $settings['serviceicon_hover']); ?></span>
            <?php else: ?>
            <?php if($settings['services_image']['url'] != ''): ?>
            	<img src="<?php echo esc_html($settings['services_image']['url']); ?>" alt="<?php echo esc_attr__('service image', 'crowdpress'); ?>">
            <?php endif; ?>
            <?php endif; ?>
            <?php endif; ?>
                   
        </div>

        <?php
	}

    protected function _content_template() {}
}