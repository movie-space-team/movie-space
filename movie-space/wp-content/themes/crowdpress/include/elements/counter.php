<?php

namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


class CrowdPress_Counter extends Widget_Base {


  public $base;

    public function get_name() {
        return 'crowdpress-counter';
    }

    public function get_title() {

        return esc_html__( 'Counter', 'crowdpress'  );

    }

    public function get_icon() { 
        return 'fas fa-flag';
    }

    public function get_categories() {
        return [ 'crowdpress-all-elements' ];
    }

    protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => esc_html__( 'Content', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
        );
		
		$this->add_control(
			'select_icon_type',
			[
				'label' => esc_html__( 'Icon Type', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'icon',
				'options' => [
					'icon'  => esc_html__( 'Icon', 'crowdpress' ),
					'image' => esc_html__( 'Image', 'crowdpress' ),
				],
			]
		);

        $this->add_control(
			'counter_icon',
			[
				'label' => esc_html__( 'Icon', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::ICONS,
				'default' => [
					'value' => 'fas fa-star',
					'library' => 'solid',
				],
				'condition' => ['select_icon_type' => ['icon']],
			]
		);
		
		$this->add_control(
			'counter_image',
			[
				'label' => esc_html__( 'Choose Image', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::MEDIA,
				'default' => [
					'url' => \Elementor\Utils::get_placeholder_image_src(),
				],
				'condition' => ['select_icon_type' => ['image']],
			]
		);
        
        $this->add_control(
			'number',
			[
				'label' => esc_html__( 'Number', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::NUMBER,
				'min' => 1,
				'max' => 10000,
				'step' => 1,
				'default' => 84,
			]
		);
	
		$this->add_control(
			'number_text',
			[
				'label' => esc_html__( 'Number Text', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'K', 'crowdpress' ),
				'placeholder' => esc_html__( 'Ex: K,M etc', 'crowdpress' ),
			]
        );

        $this->add_control(
			'counter_title',
			[
				'label' => esc_html__( 'Title', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => esc_html__( 'Projects are completed', 'crowdpress' ),
				'placeholder' => esc_html__( 'Type your title here', 'crowdpress' ),
			]
        );
        
        $this->add_responsive_control(
            'text_align', [
                'label'          => esc_html__( 'Alignment', 'crowdpress'  ),
                'type'           => Controls_Manager::CHOOSE,
                'options'        => [
    
                    'left'         => [
                        'title'    => esc_html__( 'Left', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-left',
                    ],
                    'center'     => [
                        'title'    => esc_html__( 'Center', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-center',
                    ],
                    'right'         => [
                        'title'     => esc_html__( 'Right', 'crowdpress'  ),
                        'icon'     => 'fas fa-align-right',
                    ],
                ],
               'default'         => '',
               'selectors' => [
                   '{{WRAPPER}} .counter-content' => 'text-align: {{VALUE}};'
               ],
            ]
        );
 

        $this->add_responsive_control(
			'padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .counter-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
	  
	  $this->add_control(
			'show_circle',
			[
				'label' => esc_html__( 'Show Circle', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => esc_html__( 'Show', 'crowdpress' ),
				'label_off' => esc_html__( 'Hide', 'crowdpress' ),
				'return_value' => 'yes',
				'default' => 'no',
			]
		);
       
		$this->end_controls_section();
		
		// Icon Style Section //
		
		$this->start_controls_section(
			'icon_style_section',
			[
				'label' => esc_html__( 'Icon', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
				'condition' => ['select_icon_type' => ['icon']],
			]
		);
		
		$this->start_controls_tabs(
			'icon_tabs'
		);
		
		$this->start_controls_tab(
			'icon_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .counter-icon' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'icon_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'icon_color_hover',
			[
				'label' => esc_html__( 'Icon Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .counter-icon:hover' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();

        $this->add_responsive_control(
			'icon_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .heading-content h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
	  $this->end_controls_section();
	  
	  // Number Style Section //
		
		$this->start_controls_section(
			'Number_style_section',
			[
				'label' => esc_html__( 'Number', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'number_tabs'
		);
		
		$this->start_controls_tab(
			'number_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'number_color',
			[
				'label' => esc_html__( 'Number Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .count-number' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'number_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'number_color_hover',
			[
				'label' => esc_html__( 'Number Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .count-number:hover' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'number_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .heading-content h2',
			]
		);

        $this->add_responsive_control(
			'number_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .heading-content h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
       
	  $this->end_controls_section();

	  // Title Style Section //
		
		$this->start_controls_section(
			'title_style_section',
			[
				'label' => esc_html__( 'Title', 'crowdpress' ),
				'tab' => \Elementor\Controls_Manager::TAB_STYLE,
			]
		);
		
		$this->start_controls_tabs(
			'title_tabs'
		);
		
		$this->start_controls_tab(
			'title_normal_tab',
			[
				'label' => esc_html__( 'Normal', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .counter-content h5' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->start_controls_tab(
			'title_hover_tab',
			[
				'label' => esc_html__( 'Hover', 'crowdpress' ),
			]
		);
		
		$this->add_control(
			'title_color_hover',
			[
				'label' => esc_html__( 'Title Color', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .counter-content:hover h5' => 'color: {{VALUE}}',
				],
			]
		);
		
		$this->end_controls_tab();
		
		$this->end_controls_tabs();
		
		$this->add_group_control(
			\Elementor\Group_Control_Typography::get_type(),
			[
				'name' => 'title_typography',
				'label' => esc_html__( 'Typography', 'crowdpress' ),
				'selector' => '{{WRAPPER}} .heading-content h2',
			]
		);

        $this->add_responsive_control(
			'title_padding',
			[
				'label' =>esc_html__( 'Padding', 'crowdpress'  ),
				'type' => \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}} .heading-content h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
      );
	  
	  $this->add_control(
			'circle_color',
			[
				'label' => esc_html__( 'Circle', 'crowdpress' ),
				'type' => \Elementor\Controls_Manager::COLOR,
				'condition' => ['show_circle' => ['yes']],
				'selectors' => [
					'{{WRAPPER}} .counter-content span.number.count-number.show-circle:before' => 'background: {{VALUE}}',
				],
			]
		);
       
	  $this->end_controls_section();
	  
	  // End Style Section //
    }

    protected function render() {
        $settings = $this->get_settings_for_display();
		
		
        ?>
        <div class="counter-content">
            <div class="counter-icon">
            <?php if($settings['select_icon_type'] == 'image' && $settings['counter_image']['url'] != ''): ?>
            <img src="<?php echo esc_html($settings['counter_image']['url']); ?>" alt="<?php echo esc_attr__('counter image', 'crowdpress'); ?>" />
            <?php else: ?>
			<?php \Elementor\Icons_Manager::render_icon( $settings['counter_icon'], [ 'aria-hidden' => 'true' ] ); ?>
            <?php endif; ?>
            </div>
            <?php if($settings['show_circle'] == 'yes'):
			$class_cir = ' show-circle';
			else:
			$class_cir = '';
			endif;
			?>
            <span class="number count-number<?php echo esc_attr($class_cir); ?>" data-count="<?php echo esc_attr($settings['number'] ); ?>"><span class="counter"><?php echo esc_html($settings['number'] ); ?></span><?php echo esc_html($settings['number_text']); ?></span>
            <h5><?php echo esc_html($settings['counter_title']); ?></h5>      
        </div>

        <?php
	}

    protected function _content_template() {}
}