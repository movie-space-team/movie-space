<?php
require CROWDPRESSTHEMEDIR . '/include/custom-menu/custom-menu.php';
require CROWDPRESSTHEMEDIR . '/include/breadcrumbs.php';
require CROWDPRESSTHEMEDIR . '/include/woo_functions.php';

function crowdpress_excerpt_more( $more ) {
		return '<div class="read-more-wrapper"><a class="more-link read-more-link" href="' . esc_url(get_permalink( get_the_ID() ) ) . '"><i class="fal fa-arrow-right"></i>' . esc_html__('Read More', 'crowdpress' ) . '</a></div>';
} // end tradingblock_excerpt_more function

function crowdpress_more_link($more) {
    return '<div class="read-more-wrapper"><i class="fal fa-arrow-right"></i>'.wp_kses($more, array('a'=>array('href'=>array(), 'class'=>array()))).'</div>';
}
add_filter('the_content_more_link','crowdpress_more_link');

add_filter( 'excerpt_more', 'crowdpress_excerpt_more' );

function crowdpress_excerpt_length( $length ) {
	$length = (function_exists('ot_get_option'))? ot_get_option("excerpt_length", 20) : 20;
	if( $length!= ''){
		return $length;
	} else{
		return 20;
	}	
} // end crowdpress_excerpt_length function

add_filter( 'excerpt_length', 'crowdpress_excerpt_length', 999 );

function crowdpress_body_class( $classes ) {
	
	$classes[] = (function_exists('ot_get_option'))? ot_get_option( 'background_layout', 'wide' ) : 'wide';
	$classes[] = (function_exists('ot_get_option'))? ot_get_option( 'global_style', 'button-square' ) : 'button-square';
	
	if(is_page()){
		$layout = get_post_meta( get_the_ID(), 'page_layout', true );
		if($layout != ''){
			$layout = $layout;
		} else {
			$layout = 'rs';
		}
	}
	elseif(is_single()){
		$layout = (function_exists('ot_get_option'))? ot_get_option( 'single_layout', 'rs' ) : 'rs';
	}
	else{
		$layout = (function_exists('ot_get_option'))? ot_get_option( 'blog_layout', 'rs' ) : 'rs';
	}
	
	if ( class_exists( 'woocommerce' ) ){
		if( is_product() ){
			$layout = (function_exists('ot_get_option'))? ot_get_option( 'shop_single_layout', 'full' ) : 'full';
		}
		elseif( is_woocommerce() ){
			$layout = (function_exists('ot_get_option'))? ot_get_option( 'shop_layout', 'rs' ) : 'rs';
		}
	}
	
	$classes[] = 'layout-'.$layout;

	return $classes;
}
add_filter( 'body_class', 'crowdpress_body_class' );

 if ( ! function_exists( 'crowdpress_comments' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own crowdpress_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 */
function crowdpress_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments.
	?>
    <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
		<p><?php esc_html_e( 'Pingback:', 'crowdpress' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( esc_html__( 'Edit', 'crowdpress' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
		// Proceed with normal comments.
		global $post;
	?>
	<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
    	<div class="comment">            
            <div class="media">            	
                <a href="<?php echo esc_url(get_comment_author_link()); ?>" class="pull-left"><?php echo get_avatar( $comment, 100 ); ?></a>
                <div class="media-body">
                	<h4 class="media-heading"><?php comment_author(); ?> <span><?php echo human_time_diff( get_comment_time( 'U' ), current_time('timestamp') ) . esc_html__(' ago', 'crowdpress'); ?></span></h4>
                    <span class="position-edit-links">
					<?php					
                    edit_comment_link( esc_html__( 'Edit', 'crowdpress' ), '<span class="edit-link">', '</span>' );
                    comment_reply_link( array_merge( $args, array( 'reply_text' => esc_html__( 'Reply', 'crowdpress' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) );
                    ?>
                    </span>                   
                    <p><?php comment_text(); ?></p>                   
                    <?php if ( '0' == $comment->comment_approved ) : ?>
                    <p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'crowdpress' ); ?></p>
                    <?php endif; ?> 
                </div>
             </div>
         </div>							
	<?php
		break;
	endswitch; // end comment_type check
} // end crowdpress_comments function
endif;

function crowdpress_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'crowdpress_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'crowdpress_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so crowdpress_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so crowdpress_categorized_blog should return false.
		return false;
	}
} // end crowdpress_categorized_blog function

if ( ! function_exists( 'crowdpress_category_transient_flusher' ) ) :
function crowdpress_category_transient_flusher() {
	// Like, beat it. Dig?
	delete_transient( 'crowdpress_categories' );
} // end crowdpress_category_transient_flusher function
endif;

add_action( 'edit_category', 'crowdpress_category_transient_flusher' );
add_action( 'save_post',     'crowdpress_category_transient_flusher' );

if ( ! function_exists( 'crowdpress_fonts_url' ) ) :
function crowdpress_fonts_url() {
	$fonts_url = '';
	$fonts     = array();

	/* translators: If there are characters in your language that are not supported
	 * by Open Sans, translate this to 'off'. Do not translate into your own language.
	 */
	
	if ( 'off' !== _x( 'on', 'Roboto Font: on or off', 'crowdpress' ) ) {
		$fonts[] = 'Roboto: 100,100i,300,300i,400,400i,500,500i,700,700i,900,900i';
	}
	
	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) )
		), '//fonts.googleapis.com/css' );
	}

	return $fonts_url;
} // end crowdpress_fonts_url function
endif;

if ( ! function_exists( 'crowdpress_mce_css' ) ) :
function crowdpress_mce_css( $mce_css ) {
	$fonts_url = crowdpress_fonts_url();

	if ( empty( $fonts_url ) )
		return $mce_css;

	if ( ! empty( $mce_css ) )
		$mce_css .= ',';

	$mce_css .= esc_url_raw( str_replace( ',', '%2C', $fonts_url ) );

	return $mce_css;
} // end crowdpress_mce_css function
endif;

add_filter( 'mce_css', 'crowdpress_mce_css' );

function crowdpress_gutenberg_fonts() {
	$fonts_url = '';
	$fonts     = array();
	
	if ( 'off' !== _x( 'on', 'Roboto Font: on or off', 'crowdpress' ) ) {
		$fonts[] = 'Roboto: 100,100i,300,300i,400,400i,500,500i,700,700i,900,900i';
	}
	
	if ( $fonts ) {
		$fonts_url = add_query_arg( array(
			'family' => urlencode( implode( '|', $fonts ) )
		), '//fonts.googleapis.com/css' );
	}
	
    wp_enqueue_style('crowdpress-google-fonts', $fonts_url , array(), false );
}
add_action('enqueue_block_editor_assets', 'crowdpress_gutenberg_fonts');

if ( ! function_exists( 'crowdpress_fix_gallery' ) ) :
function crowdpress_fix_gallery($output, $attr) {
    global $post;

    static $instance = 0;
    $instance++;
    $size_class = '';

    // We're trusting author input, so let's at least make sure it looks like a valid orderby statement
    if ( isset( $attr['orderby'] ) ) {
        $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
        if ( !$attr['orderby'] )
            unset( $attr['orderby'] );
    }

    extract(shortcode_atts(array(
        'order'      => 'ASC',
        'orderby'    => 'menu_order ID',
        'id'         => $post->ID,
        'itemtag'    => 'div',
        'icontag'    => 'dt',
        'captiontag' => 'dd',
        'columns'    => 3,
        'size'       => '',
        'include'    => '',
        'exclude'    => ''
    ), $attr));

    $id = intval($id);
    if ( 'RAND' == $order )
        $orderby = 'none';

    if ( !empty($include) ) {
        $include = preg_replace( '/[^0-9,]+/', '', $include );
        $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

        $attachments = array();
        foreach ( $_attachments as $key => $val ) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    } elseif ( !empty($exclude) ) {
        $exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
        $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    } else {
        $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    }

    if ( empty($attachments) )
        return '';

    if ( is_feed() ) {
        $output = "\n";
        foreach ( $attachments as $att_id => $attachment )
            $output .= wp_get_attachment_link($att_id, $size, true) . "\n";
        return $output;
    }

    $itemtag = tag_escape($itemtag);
    $captiontag = tag_escape($captiontag);
    $columns = intval($columns);
    $itemwidth = $columns > 0 ? floor(100/$columns) : 100;
    $float = is_rtl() ? 'right' : 'left';

    $selector = "gallery-{$instance}";

    $gallery_style = $gallery_div = '';
    if ( apply_filters( 'use_default_gallery_style', true ) )
        /**
         * this is the css you want to remove
         *  #1 in question
         */
        /*
        */
	$size_class = ($size != '' )?sanitize_html_class( $size ) : 'normal';
	$blog_gallery_rtl = (function_exists('ot_get_option'))? ot_get_option('blog_gallery_rtl', 'off') : 'off';
	if($blog_gallery_rtl == 'on'){
		$rtl = 'true';
	} else{
		$rtl = 'false';
	}
    $gallery_div = '<div class="gallery-carousel owl-carousel" data-items="1" data-nav="true" data-dots="false" data-margin="0" data-autoplay="true" data-rtl="'.esc_attr($rtl).'">';
    $output = apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );

	$width = round(1250/$columns);
	$height = round(1250/$columns);
	
	$col_class = intval(12/$columns);
	
	foreach ( $attachments as $id => $attachment ) {

        $link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);
		 $image_url = wp_get_attachment_url($id, $size, false, false);
		 $attatchement_image = crowdpress_image_resize( $image_url, $width, $height, true, false, false );
        
		$output .= '<div class="item"><img src="' . esc_url($image_url) . '" alt="'.esc_attr__('images thumbnail', 'crowdpress').'" /></div>';
    }
    $output .= '</div>';
    return $output;
} // end crowdpress_fix_gallery function
endif;

add_filter("post_gallery", "crowdpress_fix_gallery",10,2);


if ( ! function_exists( 'crowdpress_posts_nav' ) ) :
function crowdpress_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 ) {

	} else {
	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<nav class="clearfix"><ul class="pagination">' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link( '<i class="fas fa-angle-double-left"></i>' ) );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? 'list-nav current' : 'list-nav';

		printf( '<li class="%s"><a href="%s">%s</a></li>' . "\n", esc_attr($class), esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>&hellip;</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? 'list-nav current' : 'list-nav';
		printf( '<li class="%s"><a href="%s">%s</a></li>' . "\n", esc_attr($class), esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>&hellip;</li>' . "\n";

		$class = $paged == $max ? 'list-nav current' : 'list-nav';
		printf( '<li class="%s"><a href="%s">%s</a></li>' . "\n", esc_attr($class), esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link( '<i class="fas fa-angle-double-right"></i>' ) );

	echo '</ul></nav>' . "\n";

} // end crowdpress_posts_nav function
}
endif;

function crowdpress_next_prev_posts() {
	$prev_post = get_previous_post();
	$next_post = get_next_post();
	$next_previous_title_crop = (function_exists('ot_get_option'))? ot_get_option( 'next_previous_title_crop', '100' ) : '100';
	if (!empty( $prev_post )){
		$title = $prev_post->post_title;
		if($title != ''){
			$title = wp_trim_words( $prev_post->post_title, $next_previous_title_crop, '' );
		} else {
			$title = get_the_date( '', $prev_post->ID );
		}
	}
	
	if (!empty( $next_post )){
		$title2 = $next_post->post_title;
		if($title2 != ''){
			$title2 = wp_trim_words( $next_post->post_title, $next_previous_title_crop, '' );
		} else {
			$title2 = get_the_date( '', $next_post->ID );
		}
	}
	
	if (!empty( $next_post ) || !empty( $prev_post )):
	?>
    <div class="blog-item-next-prev">
    	<hr class="solidhr">
        <div class="row">
            <div class="col-md-6">
                <div class="absolute-pager">
                    <?php
                    if (!empty( $prev_post )){?>					
                        <a class="text-left-previous" href="<?php echo esc_url(get_permalink( $prev_post->ID )); ?>"><span><?php echo esc_html__('Prev Post', 'crowdpress'); ?></span><?php echo esc_html($title); ?></a>
                    <?php } ?>
                </div><!-- end left -->
            </div><!-- end col -->
            <div class="center-icons text-center">
                <i class="flaticon-setup-dots"></i>
            </div>
            <div class="col-md-6">
                <div class="absolute-pager text-right">
                    <?php
                    if (!empty( $next_post )){?>						
                        <a class="text-right-next" href="<?php echo esc_url(get_permalink( $next_post->ID )); ?>"><span><?php echo esc_html__('Next Post', 'crowdpress'); ?></span><?php echo esc_html($title2); ?></a>
                    <?php } ?>
                </div><!-- end left -->
            </div><!-- end col -->
        </div><!-- end row -->
        <hr class="solidhr">
    </div>
    <?php
	endif;
}

//next previous portfolios
function crowdpress_next_prev_portfolios() {
	$prev_post = get_previous_post();
	$next_post = get_next_post();
	$next_previous_title_crop = (function_exists('ot_get_option'))? ot_get_option( 'next_previous_title_crop', '100' ) : '100';
	if (!empty( $prev_post )){
		$title = $prev_post->post_title;
		if($title != ''){
			$title = wp_trim_words( $prev_post->post_title, $next_previous_title_crop, '' );
		} else {
			$title = get_the_date( '', $prev_post->ID );
		}
	}
	
	if (!empty( $next_post )){
		$title2 = $next_post->post_title;
		if($title2 != ''){
			$title2 = wp_trim_words( $next_post->post_title, $next_previous_title_crop, '' );
		} else {
			$title2 = get_the_date( '', $next_post->ID );
		}
	}
	
	if (!empty( $next_post ) || !empty( $prev_post )):
	?>
    <div class="blog-item-next-prev portfolio-next-prev">
        <div class="row no-gutters align-items-center">
            <div class="col-md-6">
            	<?php if (!empty( $prev_post )){?>
                <div class="absolute-pager">                    					
                        <?php echo get_the_post_thumbnail( $prev_post->ID, 'thumbnail' ); ?>
                        <a class="text-left-previous" href="<?php echo esc_url(get_permalink( $prev_post->ID )); ?>"><span><?php echo esc_html__('Prev Project', 'crowdpress'); ?></span><?php echo esc_html($title); ?></a>
                </div><!-- end left -->
                <?php } ?>
            </div><!-- end col -->
            <div class="col-md-6">
            <?php if (!empty( $next_post )){?>
                <div class="absolute-pager text-right">                    						
                        <a class="text-right-next" href="<?php echo esc_url(get_permalink( $next_post->ID )); ?>"><span><?php echo esc_html__('Next Project', 'crowdpress'); ?></span><?php echo esc_html($title2); ?></a>
                        <?php echo get_the_post_thumbnail( $next_post->ID, 'thumbnail' ); ?>                    
                </div><!-- end left -->
                <?php } ?>
            </div><!-- end col -->
        </div><!-- end row -->
    </div>
    <?php
	endif;
}

//related posts shows in single post
function crowdpress_related_posts() {
	global $post;
	
	$terms =  array();
    $postid = get_the_ID();
	$terms = wp_get_post_terms( $postid, 'post_tag' , array('fields' => 'slugs') );
	
	$show_related_post = (function_exists('ot_get_option'))? ot_get_option( 'show_related_post', 'on' ) : 'on';
	
	if( !empty($terms) && ( $show_related_post == 'on') ):
	$total_related_post_show = (function_exists('ot_get_option'))? ot_get_option( 'total_related_post_show', 2 ) : 2;	
	$sticky = get_option( 'sticky_posts' );
	$sticky[] = $postid;	
	
	$args = array(
	'post__not_in' => $sticky,
	'posts_per_page' => $total_related_post_show,
	'tax_query' => array(
		array(
			'taxonomy' => 'post_tag',
			'field' => 'slug',
			'terms' => $terms
		))
	);
	$query = new WP_Query( $args );
	?>
    <?php if ( $query->have_posts() ): ?>
    <div class="related-posts">        	
        <h3 class="custom-title"><?php esc_html_e('Related Posts', 'crowdpress'); ?></h3>
        <div class="row">
            <?php            	 
            while ( $query->have_posts() ) :$query->the_post();
            ?>
            <div class="col-md-6">
            	<div class="related-single-post">
					<?php if(has_post_thumbnail()): ?>
                    <div class="blog-media">
                        <a href="<?php esc_url(the_permalink()); ?>">
                            <?php crowdpress_post_thumb( 640, 520, true, false ); ?>
                        </a>
                    </div>
                    <?php endif; ?>
                    
                    <div class="rel-blog-desc">
                        <div class="blog-meta">
                            <a href="<?php esc_url(the_permalink()); ?>"><i class="far fa-calendar-alt"></i><?php echo get_the_date(); ?></a>
                        </div>
                        <h4 class="blog-des-title"><a href="<?php esc_url(the_permalink()); ?>"><?php echo wp_trim_words( get_the_title(), 6, '' ); ?></a></h4>
                        <p><?php echo wp_trim_words( get_the_content(), 12, '' ); ?></p>
                    </div>
                </div>
            </div><!-- end col -->
            <?php endwhile; ?>				
        </div><!-- end row -->			
    </div><!-- end postpager -->
    <?php
	endif;
	wp_reset_postdata();
	endif;
}

function crowdpress_menu_select(){	
	$result = array();
	$menus = get_terms( 'nav_menu', array( 'hide_empty' => false ) ); 

	foreach ( $menus as $value ) {	
		$array1 = array('value' => $value->name, 'label' => $value->name);		
		$result[] = $array1;
	}
    return $result;
}

function crowdpress_insert_attachment($file_handler,$post_id,$setthumb='false') {
	if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK){ return __return_false();
	}

	$attach_id = media_handle_upload( $file_handler, $post_id );
	//set post thumbnail if setthumb is 1
	if ($setthumb == 1){
		update_post_meta($post_id,'_thumbnail_id',$attach_id);
	}
	return $attach_id;
}

function crowdpress_custom_search_form(){
	?>
    <div class="top-search-form">
        <form id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        	<button type="submit" class="top-search-submit"><i class="fal fa-search"></i></button>
            <input type="text" name="s" id="s" class="top-form-control" placeholder="<?php esc_attr_e('Search Here', 'crowdpress'); ?>" value="<?php echo get_search_query(); ?>">
        </form>
    </div>
    <?php
}

add_filter( 'pre_get_posts', 'crowdpress_give_forms_per_page' );
function crowdpress_give_forms_per_page( $query ) {
	if ( $query->is_post_type_archive( 'give_forms' ) && ! is_admin() && $query->is_main_query() ) {
		$query->set( 'posts_per_page', '6' );
	}
	return $query;
}