<?php
/**
 * Enqueue scripts and styles for the front end.
 *
 */
function crowdpress_scripts() {
	// Add Lato font, used in the main stylesheet.
	$fonts_url = crowdpress_fonts_url();
	if ( ! empty( $fonts_url ) ){
		wp_enqueue_style( 'crowdpress-fonts', esc_url_raw( $fonts_url ), array(), null );
	}
	wp_enqueue_style( 'bootstrap', esc_url(CROWDPRESSTHEMEURI . 'css/bootstrap.css'), array(), null );
	wp_enqueue_style( 'fontawesome-free', esc_url(CROWDPRESSTHEMEURI . 'css/all.css'), array(), null );
	wp_enqueue_style( 'fontawesome', esc_url(CROWDPRESSTHEMEURI . 'css/fontawesome-all.min.css'), array(), null );
	wp_enqueue_style( 'owl-carousel', esc_url(CROWDPRESSTHEMEURI . 'css/owl.carousel.css'), array(), null );
	wp_enqueue_style( 'flaticon', esc_url(CROWDPRESSTHEMEURI . 'fonts/flaticon.css'), array(), null );
	wp_enqueue_style( 'animate', esc_url(CROWDPRESSTHEMEURI . 'css/animate.css'), array(), null );
	wp_enqueue_style( 'lightbox', esc_url(CROWDPRESSTHEMEURI . 'css/lightbox.css'), array(), null );
	wp_enqueue_style( 'magnific-popup', esc_url(CROWDPRESSTHEMEURI . 'css/magnific-popup.css'), array(), null );
	wp_enqueue_style( 'slick', esc_url(CROWDPRESSTHEMEURI . 'css/slick.css'), array(), null );
	wp_enqueue_style( 'cubeportfolio', esc_url(CROWDPRESSTHEMEURI . 'js/lib/cubeportfolio/css/cubeportfolio.min.css'), array(), null );
	wp_enqueue_style( 'crowdpress-styles', esc_url(CROWDPRESSTHEMEURI . 'css/style.css'), array(), null );	

	// Load our main stylesheet.
	wp_enqueue_style( 'crowdpress-style', get_stylesheet_uri() );

	//scripts
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ){
		wp_enqueue_script( 'comment-reply' );
	}
	wp_enqueue_script( 'bootstrap', esc_url(CROWDPRESSTHEMEURI . 'js/lib/bootstrap.min.js'), array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'fontawesome', esc_url(CROWDPRESSTHEMEURI . 'js/lib/all.js'), array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'animate', esc_url(CROWDPRESSTHEMEURI . 'js/lib/animate.js'), array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'waypoints', esc_url(CROWDPRESSTHEMEURI . 'js/lib/jquery.waypoints.js'), array( 'jquery' ), '', true );
	wp_enqueue_script( 'jquery-appear', esc_url(CROWDPRESSTHEMEURI . 'js/lib/jquery.appear.js'), array( 'jquery' ), '', true );
	wp_enqueue_script( 'counter', esc_url(CROWDPRESSTHEMEURI . 'js/lib/counter.js'), array( 'jquery' ), '', true );
	wp_enqueue_script( 'jquery-easypiechart', esc_url(CROWDPRESSTHEMEURI . 'js/lib/jquery.easypiechart.min.js'), array( 'jquery' ), '', true );
	wp_enqueue_script( 'owl-carousel', esc_url(CROWDPRESSTHEMEURI . 'js/lib/owl.carousel.min.js'), array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'jquery-easing', esc_url(CROWDPRESSTHEMEURI . 'js/lib/jquery.easing.min.js'), array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'lightbox', esc_url(CROWDPRESSTHEMEURI . 'js/lib/lightbox.js'), array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'magnific-popup', esc_url(CROWDPRESSTHEMEURI . 'js/lib/jquery.magnific-popup.min.js'), array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'slick', esc_url(CROWDPRESSTHEMEURI . 'js/lib/slick.min.js'), array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'upload', esc_url(CROWDPRESSTHEMEURI . 'js/lib/upload.js'), array( 'jquery' ), '1.0', true );	
	wp_enqueue_script( 'cubeportfolio', esc_url(CROWDPRESSTHEMEURI . 'js/lib/cubeportfolio/jquery.cubeportfolio.js'), array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'jquery-fitvid', esc_url(CROWDPRESSTHEMEURI . 'js/lib/jquery.fitvid.js'), array( 'jquery' ), '1.0', true );	
	wp_enqueue_script( 'crowdpress-scripts', esc_url(CROWDPRESSTHEMEURI . 'js/scripts.js'), array( 'jquery' ), '', true );
}
add_action( 'wp_enqueue_scripts', 'crowdpress_scripts' );

function crowdpress_styles_custom_banner() {
	
	wp_enqueue_style('crowdpress-custom-banner-style', CROWDPRESSTHEMEURI . 'css/custom_style_banner.css');
	$custom_css = '';
	$banner_image = '';	
	$header_banner_type = get_post_meta(get_the_ID(), 'header_banner_type', true);
	if($header_banner_type == 'custom_image'):
		$banner_image = get_post_meta(get_the_ID(), 'header_custom_image', true);
	endif;
	if($banner_image != ''){
		$custom_css .= ".bg-banner {
			background: url(".esc_url($banner_image).");
		}";    
    }
	
	if(is_singular('give_forms') || is_post_type_archive('give_forms') || is_tax( 'give_forms_category' )):
		$header_banner_type = (function_exists('ot_get_option'))? ot_get_option( 'donation_header_banner_type', 'donation_bg_color' ) : 'donation_bg_color';	
		if($header_banner_type == 'donation_custom_image'):
			$default_banner_image = (function_exists('ot_get_option'))? ot_get_option( 'donation_default_banner_image', CROWDPRESSTHEMEURI. 'images/banner.jpg' ) : CROWDPRESSTHEMEURI. 'images/banner.jpg';
			$custom_css .= ".bg-banner {
				background: url(".esc_url($default_banner_image).");
			}";
		else:
			$banner_color = (function_exists('ot_get_option'))? ot_get_option( 'donation_header_custom_color', '#001d23' ) : '#001d23';
			$custom_css .= ".bg-banner {
				background-color: ".$banner_color.";
			}";
		endif;
	elseif(is_singular('portfolio') || is_post_type_archive('portfolio')):
		$header_banner_type = (function_exists('ot_get_option'))? ot_get_option( 'portfolio_header_banner_type', 'portfolio_bg_color' ) : 'portfolio_bg_color';	
		if($header_banner_type == 'portfolio_custom_image'):
			$default_banner_image = (function_exists('ot_get_option'))? ot_get_option( 'portfolio_default_banner_image', CROWDPRESSTHEMEURI. 'images/banner.jpg' ) : CROWDPRESSTHEMEURI. 'images/banner.jpg';
			$custom_css .= ".bg-banner {
				background: url(".esc_url($default_banner_image).");
			}";
		else:
			$banner_color = (function_exists('ot_get_option'))? ot_get_option( 'portfolio_header_custom_color', '#001d23' ) : '#001d23';
			$custom_css .= ".bg-banner {
				background-color: ".$banner_color.";
			}";
		endif;
	elseif(is_page()):
		$breadcrumb_class = 'bgwhite';
		$breadcrumb_bg_color = get_post_meta(get_the_ID(), 'breadcrumb_bg_color', true);
		if($breadcrumb_bg_color != ''){
			$custom_css .= ".breadcrumb-content.bgwhite {
				background-color: ".$breadcrumb_bg_color.";
			}";
		} else{
			$custom_css .= ".breadcrumb-content.bgwhite {
				background-color: #fff;
			}";
		}
		$header_banner_type = get_post_meta(get_the_ID(), 'header_banner_type', true);
		if($header_banner_type == 'custom_image'):
			$banner_image = get_post_meta(get_the_ID(), 'header_custom_image', true);
			$custom_css .= ".bg-banner {
				background: url(".esc_url($banner_image).");
			}";
		else:
			$banner_color = get_post_meta(get_the_ID(), 'page_header_custom_color', true);
			if($banner_color != ''){
				$custom_css .= ".bg-banner {
				background-color: ".$banner_color.";
				}";
			} else{
				$custom_css .= ".bg-banner {
				background-color: #001d23;
				}";
			}
					
		endif;
		
		$content_background = get_post_meta(get_the_ID(), 'content_background', true);
			if($content_background != ''){
				$custom_css .= ".section-main-container.section.bgwhite{
				background-color: ".$content_background.";
				}";
			} else{
				$custom_css .= ".section-main-container.section.bgwhite{
				background-color: #ffffff;
				}";
			}
				
	elseif(is_home()):
		$header_banner_type = (function_exists('ot_get_option'))? ot_get_option( 'blog_header_banner_type', 'blog_bg_color' ) : 'blog_bg_color';	
		if($header_banner_type == 'blog_custom_image'):
			$default_banner_image = (function_exists('ot_get_option'))? ot_get_option( 'blog_default_banner_image', CROWDPRESSTHEMEURI. 'images/banner.jpg' ) : CROWDPRESSTHEMEURI. 'images/banner.jpg';
			$custom_css .= ".bg-banner {
				background: url(".esc_url($default_banner_image).");
			}";
		else:
			$banner_color = (function_exists('ot_get_option'))? ot_get_option( 'blog_header_custom_color', '#001d23' ) : '#001d23';
			$custom_css .= ".bg-banner {
				background-color: ".$banner_color.";
			}";   
    	endif;	
	else:
		$header_banner_type = (function_exists('ot_get_option'))? ot_get_option( 'blog_header_banner_type', 'blog_bg_color' ) : 'blog_bg_color';	
		if($header_banner_type == 'blog_custom_image'):
			$default_banner_image = (function_exists('ot_get_option'))? ot_get_option( 'blog_default_banner_image', CROWDPRESSTHEMEURI. 'images/banner.jpg' ) : CROWDPRESSTHEMEURI. 'images/banner.jpg';
			$custom_css .= ".bg-banner {
				background: url(".esc_url($default_banner_image).");
			}";
		else:
			$banner_color = (function_exists('ot_get_option'))? ot_get_option( 'blog_header_custom_color', '#001d23' ) : '#001d23';
			$custom_css .= ".bg-banner {
				background-color: ".$banner_color.";
			}";
		endif;
	endif;
	
	if(class_exists( 'woocommerce' )):
		if( is_product() || is_woocommerce()){
			$header_banner_type = (function_exists('ot_get_option'))? ot_get_option( 'woo_header_banner_type', 'woo_bg_color' ) : 'woo_bg_color';	
			if($header_banner_type == 'woo_custom_image'):
				$default_banner_image = (function_exists('ot_get_option'))? ot_get_option( 'woo_default_banner_image', CROWDPRESSTHEMEURI. 'images/banner.jpg' ) : CROWDPRESSTHEMEURI. 'images/banner.jpg';
				$custom_css .= ".bg-banner {
					background: url(".esc_url($default_banner_image).");
				}";
			else:
				$banner_color = (function_exists('ot_get_option'))? ot_get_option( 'woo_header_custom_color', '#001d23' ) : '#001d23';
				$custom_css .= ".bg-banner {
					background-color: ".$banner_color.";
				}";   
			endif;

		}
	endif;
	
	$background_layout = (function_exists('ot_get_option'))? ot_get_option( 'background_layout', 'wide' ) : 'wide';
	if($background_layout == 'boxed'){
		$boxed_background_image = (function_exists('ot_get_option'))? ot_get_option( 'boxed_background_image', CROWDPRESSTHEMEURI.'images/wood_03.jpg' ) : CROWDPRESSTHEMEURI.'images/wood_03.jpg';
		$custom_css .='body.boxed{
			background: url('.esc_url($boxed_background_image).') repeat left center;
		}';
	}
	
	$custom_css .= crowdpress_custom_css_from_theme_options();
    wp_add_inline_style( 'crowdpress-custom-banner-style', $custom_css );

}
add_action( 'wp_enqueue_scripts', 'crowdpress_styles_custom_banner' );
?>