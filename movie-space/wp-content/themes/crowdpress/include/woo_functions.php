<?php
add_action( 'woocommerce_before_shop_loop_item_title', 'crowdpress_template_loop_product_before_thumbnail', 5 );
add_action( 'woocommerce_before_shop_loop_item_title', 'crowdpress_template_loop_product_after_thumbnail', 10 );

function crowdpress_template_loop_product_before_thumbnail(){
	?>
    <span class="image-wrap">
    <?php
}

function crowdpress_template_loop_product_after_thumbnail(){
	?>
    </span>
    <?php
}

// Change number or products per row to 3
add_filter('loop_shop_columns', 'crowdpress_loop_columns');
if (!function_exists('crowdpress_loop_columns')) {
	function crowdpress_loop_columns() {
		return 3; // 3 products per row
	}
}

remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

add_action( 'woocommerce_before_shop_loop_item', 'crowdpress_template_loop_product_link_open', 10 );
add_action( 'woocommerce_after_shop_loop_item', 'crowdpress_template_loop_product_link_close', 5 );

function crowdpress_template_loop_product_link_open() {
	?>
    <div class="shop-box clearfix">
    <?php
}

function crowdpress_template_loop_product_link_close() {
	?>
    </div>
    <?php
}

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );

add_action( 'woocommerce_before_shop_loop_item_title', 'crowdpress_template_loop_product_thumbnail', 10 );

function crowdpress_template_loop_product_thumbnail($size = 'shop_catalog', $deprecated1 = 0, $deprecated2 = 0, $args = array()) {
	global $post;
	global $product;
	
	if ( has_post_thumbnail() ) {
		$image = crowdpress_post_thumb( 440, 480, false, false );
	} elseif ( wc_placeholder_img_src() ) {
		$image = wc_placeholder_img( $size );
	}
	?>
    <div class="shop-media">
    	<a href="<?php echo esc_url(get_permalink()); ?>">
		<?php 
		echo wp_kses($image,array(
			'img' => array(
			  'src' => array(),
			  'alt' => array(),
			  'class' => array()
			),
		  ));
		?>
        </a>
    </div><!-- end shop-image -->
    <?php
}

remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
add_action( 'woocommerce_shop_loop_item_title', 'crowdpress_template_loop_product_title', 10 );

function crowdpress_template_loop_product_title(){
	global $post;
	global $product;
	?>
    <div class="shop-details">
        <div class="shop-title">
            <h4><a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a></h4>
            <?php if ( $price_html = $product->get_price_html() ) : ?>
            <?php echo wp_kses($price_html, array('p'=>array('class'=>array()), 'del'=>array('class'=>array()), 'ins'=>array('class'=>array()), 'span'=>array('class'=>array())));  ?>
            <?php endif; ?>
        </div>
    </div><!-- end details -->
    <?php
}

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 5 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 15 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 20 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 25 );

add_action( 'woocommerce_after_single_product_summary', 'crowdpress_output_product_data_tabs_start', 5 );
function crowdpress_output_product_data_tabs_start(){
	?>
    <div class="woo-tabs stretch-content">
    	<div class="container">
    <?php
}

add_action( 'woocommerce_after_single_product_summary', 'crowdpress_output_product_data_tabs_end', 25 );
function crowdpress_output_product_data_tabs_end(){
	?>
    </div>
    </div>
    <?php
}

add_filter( 'woocommerce_dropdown_variation_attribute_options_args', 'crowdpress_filter_dropdown_args', 10 );

function crowdpress_filter_dropdown_args( $args ) {
    $attribute             = get_taxonomy($args['attribute']);
	if(!empty($attribute)){
	$args['show_option_none'] = apply_filters( 'the_title', $attribute->labels->name );
	}
    return $args;
}

function crowdpress_change_page_menu_classes($menu){
    global $post;
    if (get_post_type($post) == 'portfolio' || get_post_type($post) == 'tribe_events' || get_post_type($post) == 'give_forms')
    {
        $menu = str_replace( 'current_page_parent', '', $menu ); // remove all current_page_parent classes
    }
    return $menu;
}
add_filter( 'nav_menu_css_class', 'crowdpress_change_page_menu_classes', 10,2 );
?>