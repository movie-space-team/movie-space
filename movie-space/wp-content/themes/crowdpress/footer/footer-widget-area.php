<?php
/**
 * footer widget area
 *
 *
 * @package WordPress
 * @subpackage crowdpress
 * @since CrowdPress 1.0.0
 */
?>
<?php
$page_footer_logo = get_post_meta(get_the_ID(), 'page_footer_logo', true);
$footer_logos = (function_exists('ot_get_option'))? ot_get_option( 'footer_logo', CROWDPRESSTHEMEURI . 'images/footer-logo.png' ) : CROWDPRESSTHEMEURI . 'images/footer-logo.png';
if($page_footer_logo != ''){
	$footer_logo = $page_footer_logo;
} else{
	$footer_logo = $footer_logos;
}
$footer_widget_area = (function_exists('ot_get_option'))? ot_get_option('footer_widget_area', 'on') : 'on';
$page_footer_style = get_post_meta(get_the_ID(), 'page_footer_style', true);
$footer_styles = (function_exists('ot_get_option'))? ot_get_option('footer_style', 'style1') : 'style1';
if($page_footer_style != ''){
	$footer_style = $page_footer_style;
} else {
	$footer_style = $footer_styles; 
}
	
$center_class = '';
if($footer_style == 'style2'){
	$center_class = ' align-items-center';
}
if($footer_style == 'style3'){
	$class = 'col-md-12 text-center';
} else {
	$class = 'col-md-6';
}
?>
<div class="footer-top-part">
	<div class="row">
	<?php
	if($footer_style == 'style2'):
		for( $j = 1; $j <= 2; $j++ ): ?>
        	<?php if($j == 1){
				$col_class_2 = 8;
				$col_class_3 = 7;
			} else{
				$col_class_2 = 4;
				$col_class_3 = 5;
			}
			?>
			<?php if ( is_active_sidebar( 'footer-'.$j ) ) : ?>
			<div class="col-lg-<?php echo esc_attr($col_class_2); ?> col-md-<?php echo esc_attr($col_class_3); ?> footer-widget-area">
			<?php dynamic_sidebar( 'footer-'.$j ); ?>
			</div>
			<?php
			endif;
			?>            
			<?php
		endfor;
	else:
	?>
    	<?php if($footer_style == 'style3'): ?>    
        <div class="<?php echo esc_attr($class); ?>">
            <a class="footer-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><img src="<?php echo esc_url($footer_logo); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
        </div>
        <?php endif; ?>  
    <?php endif; ?>
    </div>
</div>
<?php	
if( $footer_widget_area == 'on' ):
	$footer_widget_box = (function_exists('ot_get_option'))? ot_get_option('footer_widget_box', 3) : 3;	
	$col_class = 12/$footer_widget_box;		
	?>
    <div class="footer-bottom-part">  
        <div class="row<?php echo esc_attr($center_class); ?>">
            <?php
			if($footer_style == 'style2'):
				?>
                
                <div class="col-lg-3 col-md-12">
                	<?php $footer_logo = (function_exists('ot_get_option'))? ot_get_option( 'footer_logo', CROWDPRESSTHEMEURI . 'images/footer-logo.png' ) : CROWDPRESSTHEMEURI . 'images/footer-logo.png'; ?>
            <a class="footer-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><img src="<?php echo esc_url($footer_logo); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
                </div>
                <?php
				for( $k = 3; $k <= 4; $k++ ): ?> 
                	<?php if($k == 3){
						$col_class2 = 6;
						$col_class24 = 8;
					} else{
						$col_class2 = 3;
						$col_class24 = 4;
					}
					?>                   
					<?php if ( is_active_sidebar( 'footer-'.$k ) ) : ?>
                    <div class="col-lg-<?php echo esc_attr($col_class2); ?> col-md-<?php echo esc_attr($col_class24); ?> footer-widget-area">
                    <?php dynamic_sidebar( 'footer-'.$k ); ?>
                    </div>
                    <?php
                    endif;
                    ?>            
                    <?php
                endfor;
			else:
            for( $i = 1; $i <= $footer_widget_box; $i++ ): ?>                    
                <?php if ( is_active_sidebar( 'footer-'.$i ) ) : ?>
                <div class="col-lg-<?php echo esc_attr($col_class); ?> col-md-6 footer-widget-area">
                <?php dynamic_sidebar( 'footer-'.$i ); ?>
                </div>
                <?php
                endif;
                ?>            
                <?php
            endfor;
			endif;
             ?>
             <?php $footer_description_text = (function_exists('ot_get_option'))? ot_get_option('footer_description_text', '') : '';
             if( $footer_description_text != '' && $footer_style == 'style2'):
			 ?>
             <div class="col-12 footer-description">
                <?php echo wp_kses(do_shortcode($footer_description_text), array('div'=>array('class'=>array(), 'id'=>array()), 'a'=>array('class'=>array(), 'title'=>array(), 'href'=>array()), 'p'=>array('class'=>array()))); ?>
             </div>
             <?php
             endif;
             ?>                
        </div>
    </div>
<?php endif; ?>