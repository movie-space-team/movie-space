(function($){
'use strict';	
	$( window ).on( 'elementor/frontend/init', function() {
		
		var owl_carousel = function(){
		$('.owl-carousel').each( function() {
	    var $carousel = $(this);
		var $rtl = ($carousel.data('rtl') !== undefined) ? $carousel.data('rtl') : false;
	    var $items = ($carousel.data('items') !== undefined) ? $carousel.data('items') : 3;
	    var $items_tablet = ($carousel.data('items-tablet') !== undefined) ? $carousel.data('items-tablet') : 2;
	    var $items_mobile_landscape = ($carousel.data('items-mobile-landscape') !== undefined) ? $carousel.data('items-mobile-landscape') : 1;
	    var $items_mobile_portrait = ($carousel.data('items-mobile-portrait') !== undefined) ? $carousel.data('items-mobile-portrait') : 1;
	    $carousel.owlCarousel ({
		  rtl: $rtl,
	      loop : ($carousel.data('loop') !== undefined) ? $carousel.data('loop') : true,
	      items : $carousel.data('items'),
	      margin : ($carousel.data('margin') !== undefined) ? $carousel.data('margin') : 30,
	      dots : ($carousel.data('dots') !== undefined) ? $carousel.data('dots') : true,
	      nav : ($carousel.data('nav') !== undefined) ? $carousel.data('nav') : false,
	      navText : ["<div class='slider-no-current'><span class='current-no'></span><span class='total-no'></span></div><span class='current-monials'></span>", "<div class='slider-no-next'></div><span class='next-monials'></span>"],
	      autoplay : ($carousel.data('autoplay') !== undefined) ? $carousel.data('autoplay') : true,
	      autoplayTimeout : ($carousel.data('autoplay-timeout') !== undefined) ? $carousel.data('autoplay-timeout') : 5000,
	      animateIn : ($carousel.data('animatein') !== undefined) ? $carousel.data('animatein') : false,
	      animateOut : ($carousel.data('animateout') !== undefined) ? $carousel.data('animateout') : false,
	      mouseDrag : ($carousel.data('mouse-drag') !== undefined) ? $carousel.data('mouse-drag') : true,
	      autoWidth : ($carousel.data('auto-width') !== undefined) ? $carousel.data('auto-width') : false,
	      autoHeight : ($carousel.data('auto-height') !== undefined) ? $carousel.data('auto-height') : false,
	      center : ($carousel.data('center') !== undefined) ? $carousel.data('center') : false,
	      responsiveClass: true,
	      dotsEachNumber: true,
	      smartSpeed: 600,
	      autoplayHoverPause: true,
	      responsive : {
	        0 : {
	          items : $items_mobile_portrait,
	        },
	        480 : {
	          items : $items_mobile_landscape,
	        },
	        768 : {
	          items : $items_tablet,
	        },
	        992 : {
	          items : $items,
	        }
	      }
	    });
	    var totLength = $('.owl-dot', $carousel).length;
	    $('.total-no', $carousel).html(totLength);
	    $('.current-no', $carousel).html(totLength);
	    $carousel.owlCarousel();
	    $('.current-no', $carousel).html(1);
	    $carousel.on('changed.owl.carousel', function(event) {
	      var total_items = event.page.count;
	      var currentNum = event.page.index + 1;
	      $('.total-no', $carousel ).html(total_items);
	      $('.current-no', $carousel).html(currentNum);
	    });
	  });
	}; // end
	
	var cbp_cubeportfolio = function(){
		var column = $('#grid-container').data('columnnum');
		var layout_mode = $('#grid-container').data('layout');
		var gridContainer = $('#grid-container'), filtersContainer = $('#filters-container'), wrap, filtersCallback;
		
		$('#grid-container').each( function() {
		
		/*********************************
        init cubeportfolio
     *********************************/
	 if(layout_mode == 'grid'){
    gridContainer.cubeportfolio({
        layoutMode: 'grid',
        rewindNav: true,
        scrollByPage: false,
        defaultFilter: '*',
        animationType: 'flipOutDelay',
        gapHorizontal: 30,
        gapVertical: 30,
        gridAdjustment: 'responsive',
        mediaQueries: [{
            width: 1100,
            cols: column
        }, {
            width: 800,
            cols: column
        }, {
            width: 500,
            cols: 2
        }, {
            width: 320,
            cols: 1
        }],
        caption: 'overlayBottomReveal',
        displayType: 'lazyLoading',
        displayTypeSpeed: 100,

        // lightbox
        lightboxDelegate: '.cbp-lightbox',
        lightboxGallery: true,
        lightboxTitleSrc: 'data-title',
        lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

        // singlePage popup
        singlePageDelegate: '.cbp-singlePage',
        singlePageDeeplinking: true,
        singlePageStickyNavigation: true,
        singlePageCounter: '<div class="cbp-popup-singlePage-counter">{{current}} of {{total}}</div>',
        singlePageCallback: function(url, element) {
            // to update singlePage content use the following method: this.updateSinglePage(yourContent)
            var t = this;

            jQuery.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                    timeout: 5000
                })
                .done(function(result) {
                    t.updateSinglePage(result);
                })
                .fail(function() {
                    t.updateSinglePage("Error! Please refresh the page!");
                });
        },

        // single page inline
        singlePageInlineDelegate: '.cbp-singlePageInline',
        singlePageInlinePosition: 'above',
        singlePageInlineInFocus: true,
        singlePageInlineCallback: function(url, element) {
            // to update singlePage Inline content use the following method: this.updateSinglePageInline(yourContent)
        }
    });


    /*********************************
        add listener for filters
     *********************************/
    if (filtersContainer.hasClass('cbp-l-filters-dropdown')) {
        wrap = filtersContainer.find('.cbp-l-filters-dropdownWrap');

        wrap.on({
            'mouseover.cbp': function() {
                wrap.addClass('cbp-l-filters-dropdownWrap-open');
            },
            'mouseleave.cbp': function() {
                wrap.removeClass('cbp-l-filters-dropdownWrap-open');
            }
        });

        filtersCallback = function(me) {
            wrap.find('.cbp-filter-item').removeClass('cbp-filter-item-active');
            wrap.find('.cbp-l-filters-dropdownHeader').text(me.text());
            me.addClass('cbp-filter-item-active');
            wrap.trigger('mouseleave.cbp');
        };
    } else {
        filtersCallback = function(me) {
            me.addClass('cbp-filter-item-active').siblings().removeClass('cbp-filter-item-active');
        };
    }

    filtersContainer.on('click.cbp', '.cbp-filter-item', function() {
        var me = jQuery(this);

        if (me.hasClass('cbp-filter-item-active')) {
            return;
        }

        // get cubeportfolio data and check if is still animating (reposition) the items.
        if (!jQuery.data(gridContainer[0], 'cubeportfolio').isAnimating) {
            filtersCallback.call(null, me);
        }

        // filter the items
        gridContainer.cubeportfolio('filter', me.data('filter'), function() {});
    });


    /*********************************
        activate counter for filters
     *********************************/
    gridContainer.cubeportfolio('showCounter', filtersContainer.find('.cbp-filter-item'), function() {
        // read from url and change filter active
        var match = /#cbpf=(.*?)([#|?&]|$)/gi.exec(location.href),
            item;
        if (match !== null) {
            item = filtersContainer.find('.cbp-filter-item').filter('[data-filter="' + match[1] + '"]');
            if (item.length) {
                filtersCallback.call(null, item);
            }
        }
    });


    /*********************************
        add listener for load more
     *********************************/
    jQuery('.cbp-l-loadMore-button-link').on('click.cbp', function(e) {
        e.preventDefault();
        var clicks, me = jQuery(this),
            oMsg;

        if (me.hasClass('cbp-l-loadMore-button-stop')) {
            return;
        }

        // get the number of times the loadMore link has been clicked
        clicks = jQuery.data(this, 'numberOfClicks');
        clicks = (clicks) ? ++clicks : 1;
        jQuery.data(this, 'numberOfClicks', clicks);

        // set loading status
        oMsg = me.text();
        me.text('LOADING...');

        // perform ajax request
        jQuery.ajax({
            url: me.attr('href'),
            type: 'GET',
            dataType: 'HTML'
        }).done(function(result) {
            var items, itemsNext;

            // find current container
            items = jQuery(result).filter(function() {
                return jQuery(this).is('div' + '.cbp-loadMore-block' + clicks);
            });

            gridContainer.cubeportfolio('appendItems', items.html(),
                function() {
                    // put the original message back
                    me.text(oMsg);

                    // check if we have more works
                    itemsNext = jQuery(result).filter(function() {
                        return jQuery(this).is('div' + '.cbp-loadMore-block' + (clicks + 1));
                    });

                    if (itemsNext.length === 0) {
                        me.text('NO MORE WORKS');
                        me.addClass('cbp-l-loadMore-button-stop');
                    }

                });

        }).fail(function() {
            // error
        });

    });
	 }
		
		});
	}; // end
	
	//portfolios
  elementorFrontend.hooks.addAction( 'frontend/element_ready/crowdpress-projects.default', function($scope, $){
    var delay = 0;
	$('.progress-bar').each(function(i) {
      $(this).delay( delay*i ).animate ({
        width: $(this).attr('aria-valuenow') + '%'
      }, delay);
      $(this).prop('Counter',0).animate ({
        Counter: $(this).text()
      },
      {
        duration: delay,
        easing: 'swing',
      });
    });
    
  } );
  
  //portfolios
  elementorFrontend.hooks.addAction( 'frontend/element_ready/crowdpress-feature-lists.default', function($scope, $){
    /*---------------featured lists---------*/
	if ($('.featured-lists-content').length) {
    $('.featured-lists-content').each(function(){
      var $circle = $(this).find('.services_item-icon');

      var agle = 360 / $circle.length;
      var agleCounter = -1;

      $circle.each(function() {
        var $this = $(this);

        $(this).parents('.services_item-wrap:first-child').addClass('active');
        $this.on('hover', function(){
          $(this).parents('.services_item-wrap').addClass('active').siblings().removeClass('active');
        })

        var percentWidth = (100 * parseFloat($this.css('width')) / parseFloat($this.parent().css('width')));
        var curAgle = agleCounter * agle;
        var radAgle = curAgle * Math.PI / 180;
        var x = (50 + ((50 - (percentWidth / 2)) * Math.cos(radAgle))) - (percentWidth / 2);
        var y = (50 + ((50 - (percentWidth / 2)) * Math.sin(radAgle))) - (percentWidth / 2);
            
        $this.css({
          left: x + '%',
          top: y + '%'
        });
        
        agleCounter++;
      });

    });
  }

    
  } );
		
		elementorFrontend.hooks.addAction( 'frontend/element_ready/crowdpress-portfolios.default', function($scope, $){
			cbp_cubeportfolio();
		} );
		
		elementorFrontend.hooks.addAction( 'frontend/element_ready/crowdpress-testmonials.default', function($scope, $){
			owl_carousel();
		} );
		
		elementorFrontend.hooks.addAction( 'frontend/element_ready/crowdpress-services.default', function($scope, $){
			
			$( '.services-content.style4' ).each( function () {
				$(this).on('mouseleave', function() {
					$(this).removeClass('active-yes');
					$(this).addClass('active-no');
				});
				$(this).on('mouseover', function() {
					$(this).addClass('active-yes');
					$(this).removeClass('active-no');
				});
			});
			
		} );
		
		elementorFrontend.hooks.addAction( 'frontend/element_ready/crowdpress-gallery.default', function($scope, $){
			
			$( '.slider-nav' ).each( function () {
        var column = $(this).data('column');
        var rtl = $(this).data('rtl');
				$(this).slick({
				  
				  slidesToScroll: 1,
				  slidesToShow: column,
				  dots: false,
				  centerMode: true,
				  centerPadding: '400px',
				  focusOnSelect: true,
				  autoplay: true,
				  autoplaySpeed: 2000,
				  arrows: false,
				  responsive: [
					{
					  breakpoint: 1900,
					  settings: {
						slidesToShow: column,
						slidesToScroll: 1,
						infinite: true,
						dots: false,
						centerPadding: '300px',
					  }
					},
					{
					  breakpoint: 1600,
					  settings: {
						slidesToShow: column,
						slidesToScroll: 1,
						infinite: true,
						dots: false,
						centerPadding: '200px',
					  }
					},
					{
					  breakpoint: 1200,
					  settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						infinite: true,
						dots: false,
						centerPadding: '100px',
					  }
					},
					{
					  breakpoint: 768,
					  settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						centerPadding: '50px',
					  }
					},
					
				]
				},
				'setPosition'
				);
			});
			
		} );
		
	});
	
	

})(jQuery);