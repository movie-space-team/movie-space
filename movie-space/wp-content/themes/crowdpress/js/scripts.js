jQuery(window).load(function() {
	
	"use strict";
	
	var preload = jQuery('.preloading-image').data('bgimage');	
	jQuery(".preloading-image").css({'background-image': 'url('+preload+')'});
	jQuery(".preloading-image").fadeOut();	
    // will fade out the whole DIV that covers the website.
    jQuery(".preloading").delay(1000).fadeOut("slow");

});

jQuery(document).ready(function( $ ) {
	"use strict";	
	
	$('p').each(function() {
        var $this = jQuery(this);
        if($this.html().replace(/\s|&nbsp;/g, '').length == 0) {
            $this.remove();
        }
    });
	
	/*=============================================
	=    		Mobile Menu			      =
	=============================================*/
	//SubMenu Dropdown Toggle
	if ($('.navbar li.dropdown ul').length) {
		$('.navbar .navbar-nav li.dropdown').append('<div class="dropdown-btn"><span class="fas fa-angle-down"></span></div>');
	
	}
	
	//Mobile Nav Hide Show
	if ($('.mobile-menu').length) {
	
		var mobileMenuContent = $('.navbar .navbar-collapse').html();
		$('.mobile-menu .menu-box .menu-outer').append(mobileMenuContent);
	
		//Dropdown Button
		$('.mobile-menu li.dropdown .dropdown-btn').on('click', function () {
			$(this).toggleClass('open');
			$(this).prev('ul').slideToggle(500);
		});
		//Menu Toggle Btn
		$('.mobile-nav-toggler').on('click', function () {
			$('body').addClass('mobile-menu-visible');
		});
	
		//Menu Toggle Btn
		$('.mobile-menu .menu-backdrop,.mobile-menu .close-btn').on('click', function () {
			$('body').removeClass('mobile-menu-visible');
		});
	}
	
	$( '.panel-row-style' ).each( function () {
		var overlay = $(this).data('overlay');
		var opacity = $(this).data('opacity');
		var curvecolor = $(this).data('curvecolor');
		var curveheight = $(this).data('curveheight');
		var curvewidth = $(this).data('curvewidth');
		var curveposition = $(this).data('curveposition');
		var curveheight2 = $(this).data('curveheight2');
		var curvewidth2 = $(this).data('curvewidth2');
		var curveposition2 = $(this).data('curveposition2');
		if($(this).attr('data-overlay') != undefined){
		$(this).prepend( '<div class="overlay-row" style="background: rgba(' +overlay+ ', ' +opacity+ ');">' );
		}
		if($(this).attr('data-curvecolor') != undefined){
			if($(this).attr('data-curveposition') == 'left_top'){
				$(this).prepend( '<div class="angle-background-shape '+curveposition+'" style="border-left: '+curveheight+'px solid '+curvecolor+'; border-top: '+curvewidth+'px solid '+curvecolor+'; border-right: '+curvewidth+'px solid transparent; border-bottom: '+curveheight+'px solid transparent;"></div>' );
			}
			if($(this).attr('data-curveposition2') == 'right_bottom'){
				$(this).prepend( '<div class="angle-background-shape '+curveposition2+'" style="border-right: '+curveheight2+'px solid '+curvecolor+'; border-bottom: '+curvewidth2+'px solid '+curvecolor+'; border-left: '+curvewidth2+'px solid transparent; border-top: '+curveheight2+'px solid transparent;"></div>' );
			}		
		}
	});
	
	$('.ts-button').each(function () {
		var hoverback = $(this).data('hoverback');
		var currentbg = $(this).data('currentbg');
		var currentbor = $(this).data('currentbor');
		var hoverbor = $(this).data('hoverbor');
		var currentcolor = $(this).data('currentcolor');
		var hovercolor = $(this).data('hovercolor');
		$(this).on('mouseenter',function() {
			$(this).css({'background': hoverback, 'border-color': hoverbor, 'color': hovercolor});
		});
		$(this).on('mouseleave',function() {
			$(this).css({'background': currentbg, 'border-color': currentbor, 'color': currentcolor});
		});
	});
	
	$('.site-colors ul > li > a').each(function () {
		var backgroundcolor = $(this).data('backgroundcolor');
		$(this).css({'background': backgroundcolor});
	});
	
	$('.element').each(function () {
		var text1 = $(this).data('text1');
		var text2 = $(this).data('text2');
		var text3 = $(this).data('text3');
		var text4 = $(this).data('text4');
	$(function(){
	  $(".element").typed({
		strings: [text1, text2, text3, text4],
		typeSpeed: 300
	  });
	});
	});
	
	$(".content").fitVids();
	
	// Counter
	$(".count-number").appear(function(){
		$('.count-number').each(function(){
			var datacount = $(this).attr('data-count');
			$(this).find('.counter').delay(8000).countTo({
				from: 10,
				to: datacount,
				speed: 4000,
				refreshInterval: 50,
			});
		});
	});
	
	/* ==============================================
    BACK TOP
    =============================================== */
	
	// Scroll top top
	var offset = 200,
		offset_opacity = 1200,
		scroll_top_duration = 700,
		$back_to_top = $('.dmtop');

	$(window).scroll(function(){
		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('dmtop dmtop-show') : $back_to_top.removeClass('dmtop-show cd-fade-out');
		if( $(this).scrollTop() > offset_opacity ) { 
			$back_to_top.addClass('cd-fade-out');
		}
	});

	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		 	}, scroll_top_duration
		);
	});
	
	/*======================= tooltip==============*/
	
	$(function() {
        $('[data-toggle="tooltip"]').tooltip()
    })
    $('.dmtop').click(function(){
        $('html, body').animate({scrollTop: '0px'}, 800);
        return false;
    });
    $(document).ready(function() {
        setTimeout(function(){
            $('body').addClass('loaded');
        }, 3000);
    });
    $(document).ready(function() {
        $('#nav-expander').on('click', function(e) {
            e.preventDefault();
            $('body').toggleClass('nav-expanded');
        });
        $('#nav-close').on('click', function(e) {
            e.preventDefault();
            $('body').removeClass('nav-expanded');
        });
    });
	
	if($('.layout-full .stretch-content').find('div.row').length != 0) {
		$('.layout-full .stretch-content .row').css('margin', '0'); 	
	}
  
  var authorContainer = $('body');
    // This will handle stretching the cells.
	
    $('.layout-full .stretch-content').each(function(){
		
        var $$ = $(this);

        var onResize = function(){

            $$.css({
                'margin-left' : 0,
                'margin-right' : 0,
                'padding-left' : 0,
                'padding-right' : 0
            });

            var leftSpace = $$.offset().left - authorContainer.offset().left;
            var rightSpace = authorContainer.outerWidth() - leftSpace - $$.parent().outerWidth();

            $$.css({
                'margin-left' : -leftSpace,
                'margin-right' : -rightSpace,
                'padding-left' : $$.data('stretch-type') === 'full' ? leftSpace : 0,
                'padding-right' : $$.data('stretch-type') === 'full' ? rightSpace : 0
            });
        };

        $(window).resize( onResize );
        onResize();

        $$.css({
            'border-left' : 0,
            'border-right' : 0
        });
    });
	
	$('.gallery-carousel').each(function() {		
		var margin = $(this).data('margin');
		var items = $(this).data('items');
		var nav = $(this).data('nav');
		var dots = $(this).data('dots');
		var autoplay = $(this).data('autoplay');
		var items_tablet = 1;
		var rtl = $(this).data('rtl');
		$('.gallery-carousel').owlCarousel({
			rtl: rtl,
			loop:true,
			margin: margin,
			nav: nav,
			autoplay: autoplay,
			dots: dots,
			navText : ['<i class="fal fa-arrow-left"></i>','<i class="fal fa-arrow-right"></i>'],
			responsive : {
	        0 : {
	          items : 1,
	        },
	        480 : {
	          items : 1,
	        },
	        768 : {
	          items : items_tablet,
	        },
	        992 : {
	          items : items,
	        }
	      }
		});
	});
	
	//Progress Bar Script
	$('.project-cause-bar').waypoint(function() {
		var delay = 0;
		$('.progress-bar').each(function(i) {
		$(this).delay( delay*i ).animate ({
		width: $(this).attr('aria-valuenow') + '%'
		}, delay);
		$(this).prop('Counter',0).animate ({
		Counter: $(this).text()
		},
		{
		duration: delay,
		easing: 'swing',
		});
		});
		},
		{
		offset: '100%',
		triggerOnce: true,
	});
	
	/*----------------------------------------------------*/
	/*	Video Link Lightbox
	/*----------------------------------------------------*/		
	$('.video-popup').each(function(){
		var url = $(this).data('url');
		$('.video-popup').magnificPopup({
			type: 'iframe',				  
			iframe: {
			patterns: {
			youtube: {				   
			index: 'youtube.com',
			src: url

				}
			}
		}		  		  
		});
	});
	
	/*----------------------------------------------------*/
	/*	Video Link Lightbox
	/*----------------------------------------------------*/		
	$('.gallery-content').each(function() { // the containers for all your galleries
		$(this).magnificPopup({
		delegate: 'a', // the selector for gallery item
		type: 'image',
		gallery: {
		enabled:true
		}
		});
	});
	
	/*----------------pie chart --------------*/
	$(".piechart").appear(function(){
		$('.piechart').each(function() {
			allCharts();
		});
	});
	
	$('.features-area-group-item').hover(function () {
		$(this).find('.features-area-group-btn').slideToggle(400);
		return false;
	});
	
});

// Pie Chart 
function allCharts() {
	
	"use strict";

	jQuery(window).load( function(){

		var pieChartData = [
			{
				value: 90,
				color:"#2196f3"
			},

			{
				value : 30,
				color : "#333"
			},

			{
				value : 60,
				color : "#2196f3"
			},

			{
				value : 100,
				color : "#2196f3"
			},

			{
				value : 20,
				color : "#2196f3"
			}

		];


		function showPieChart(){
			var ctx = document.getElementById("pieChart").getContext("2d");
			new Chart(ctx).Pie(pieChartData,{	responsive: true	});
		}	
		$('#pieChart').appear( function(){ $(this).css({ opacity: 1 }); setTimeout(showPieChart,300); },{accX: 0, accY: -155},'easeInCubic');

	});
}