<?php
/**
 * Content wrappers
 *
 * @package     Give
 * @subpackage  Templates/Global
 * @copyright   Copyright (c) 2016, GiveWP
 * @license     https://opensource.org/licenses/gpl-license GNU Public License
 * @since       1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_template_part( 'layout/after', '' );
