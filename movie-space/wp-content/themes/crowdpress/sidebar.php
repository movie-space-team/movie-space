<?php
/**
 * The sidebar containing the main widget area
 *
 * If no active widgets are in the sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage crowdpress
 * @since crowdpress 1.0.0
 */

$sidebar_col = 'col-md-4';
$class_give = '';
if(is_page()){
	$sidebar = get_post_meta( get_the_ID(), 'sidebar', true );		
	if($sidebar == '') $sidebar = 'sidebar-1';
}
elseif(is_single()){
	$sidebar = (function_exists('ot_get_option'))? ot_get_option( 'blog_single_sidebar', 'sidebar-1' ) : 'sidebar-1';
}
else {
	$sidebar = (function_exists('ot_get_option'))? ot_get_option( 'blog_sidebar', 'sidebar-1' ) : 'sidebar-1';
}

if(is_singular('portfolio')){
	if(is_single()){
		$sidebar = (function_exists('ot_get_option'))? ot_get_option( 'portfolio_single_sidebar', 'sidebar-1' ) : 'sidebar-1';
	} else{
		$sidebar = (function_exists('ot_get_option'))? ot_get_option( 'portfolio_sidebar', 'sidebar-1' ) : 'sidebar-1';
	}
}

if ( class_exists( 'woocommerce' ) ){
	if( is_product() ){
		$sidebar = (function_exists('ot_get_option'))? ot_get_option( 'shop_single_sidebar', 'sidebar-1' ) : 'sidebar-1';
	}
	elseif( is_woocommerce() ){
		$sidebar = (function_exists('ot_get_option'))? ot_get_option( 'shop_sidebar', 'sidebar-1' ) : 'sidebar-1';
		$sidebar_col = 'col-lg-3 col-md-4';
	}
}

if ( $sidebar == 'give-forms-sidebar' ){
	$class_give = ' give-single-form-sidebar-left';
} else {
	$class_give = '';
}
?>
<?php if ( is_active_sidebar( $sidebar ) ) : ?>
<div id="sidebar" class="<?php echo esc_attr($sidebar_col); ?> col-sm-12 col-xs-12">    
    <div class="sidebar<?php echo esc_attr($class_give); ?>">
		<div class="widget-area">
        	<?php dynamic_sidebar( $sidebar ); ?>
        </div>    
    </div>                
</div>	
<?php endif; ?>
