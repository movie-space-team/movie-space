<?php
/**
 * The template for displaying Author Archive pages
 *
 * Used to display archive-type pages for posts by an author.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage crowdpress
 * @since crowdpress 1.0.0
 */
 
?>
<?php $description = get_the_author_meta( 'description' ); ?>
<?php if($description != ''): ?>
<div class="authorbox">
    <div class="row align-items-center">
        <div class="col-md-4">
        	<?php		
			$author_bio_avatar_size = apply_filters( 'crowdpress_author_bio_avatar_size', 180 );
			echo get_avatar( get_the_author_meta( 'user_email' ), $author_bio_avatar_size, null, null, array( 'class' => array( 'img-responsive' ) ) );
			?>
        </div>
        <div class="col-md-8">
            <h4><span><?php echo esc_html__('Written By ', 'crowdpress'); ?></span><?php echo get_the_author(); ?></h4>
            <p><?php the_author_meta( 'description' ); ?></p>
        </div><!-- end col -->
    </div>
</div>
<!-- end details -->
<?php endif; ?>