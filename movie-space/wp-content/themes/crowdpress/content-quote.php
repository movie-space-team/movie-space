<?php $blog_style = (function_exists('ot_get_option'))? ot_get_option( 'blog_style', 'default' ) : 'default';
	$blog_layout = (function_exists('ot_get_option'))? ot_get_option( 'blog_layout', 'rs' ) : 'rs';
	$blog_text_align = (function_exists('ot_get_option'))? ot_get_option( 'blog_text_align', '' ) : '';
	$show_social_sharing_icons = (function_exists('ot_get_option'))? ot_get_option( 'show_social_sharing_icons', 'off' ) : 'off';
	$width = 1140;
	$height = 600;
	$class = '';
	$class2 = '';
	$class3 = '';
	$align_class = '';
	$class_list = '';
	if(!is_single()){
		if($blog_text_align == 'center'){
			$align_class = ' text-center';
		} elseif($blog_text_align == 'right') {
			$align_class = ' text-right';
		} else {
			$align_class = '';
		}
	}
	if($blog_style == 'grid_view'):
		if(!is_single()):
			if($blog_layout == 'full'):
				$class = ' col-md-4 grid-view';
			else:
				$class = ' col-md-6 grid-view';
			endif;
		endif;
	elseif($blog_style == 'list_view'):
		if(!is_single()):
			$class_list = ' row list-view';
			if($blog_layout == 'full'):
				$class2 = ' col-md-3';
				$class3 = ' col-md-9';
			else:
				if(has_post_thumbnail()){
					$class2 = ' col-md-5';
					$class3 = ' col-md-7';
				} else{
					$class2 = ' col-md-12';
					$class3 = ' col-md-12';
				}
			endif;
			$width = 400;
			$height = 400;
		endif;	
	else:
		$class = '';
	endif;
	?>
    <?php if(!is_single()): ?>
    	<?php if($blog_style == 'grid_view'):?>
        <div class="<?php echo esc_attr($class); ?>">
        <?php endif; ?>
    <?php endif; ?>
    <div id="post-<?php the_ID(); ?>" <?php post_class() ?>>
    
    <?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>  
        <div class="post-meta sticky-posts text-center">                
            <?php $sticky_post_text = (function_exists('ot_get_option'))? ot_get_option( 'sticky_post_text', esc_html__('Featured', 'crowdpress') ) : esc_html__('Featured', 'crowdpress'); ?>                
            <div class="sticky-content"><?php printf( '<span class="sticky-post">%s</span>', $sticky_post_text ); ?></div>                
        </div>            
    <?php endif; ?>
    
    <div class="blog-box clearfix<?php echo esc_attr($class_list); ?>">
        <?php if($blog_style == 'list_view' && !is_single()):?>
        <div class="<?php echo esc_attr($class2); ?>">
        <?php endif; ?>
        <div class="blog-media formate-quote">
        	<?php 
			$quote_text = get_post_meta( get_the_ID(), 'quote_text', true );		
			$source_url = get_post_meta( get_the_ID(), 'quote_text_url', true );							
			$quote_cite = get_post_meta( get_the_ID(), 'quote_cite', true );							
			if( $quote_text != '' ):
			?>
			<blockquote class="wp-block-quote">
            	<i class="fal fa-quote-left"></i>       
				<p><?php echo esc_html($quote_text); ?></p>                                
                <cite title="<?php echo esc_attr($quote_cite); ?>">
                <a href="<?php echo (is_single())? esc_url($source_url) : esc_url(get_permalink()); ?>"><?php echo esc_html($quote_cite); ?></a>
                </cite>
               
                <div class="blog-meta">
					<?php if(function_exists('pvc_get_post_views')): ?>
                    <a href="<?php esc_url(the_permalink()); ?>"><i class="far fa-eye"></i><?php echo wp_kses(pvc_get_post_views( get_the_ID() ), array('span'=>array())); ?><?php echo esc_html__(' Views', 'crowdpress'); ?></a>
        <?php endif; ?>
                    
                    <a href="<?php esc_url(the_permalink()); ?>"><i class="far fa-calendar-alt"></i><?php echo get_the_date(); ?></a>
                    
                    <a href="<?php esc_url(comments_link()); ?>"><i class="far fa-comments"></i> <?php comments_number( esc_html__('0 Comments', 'crowdpress'), esc_html__('1 Comment', 'crowdpress'), esc_html__('% Comments', 'crowdpress') ) ?></a>
                </div><!-- end meta -->
                                                            
			</blockquote>
			<?php 
			endif;
			?>
            <?php if(has_post_thumbnail()): ?>
				<?php if(!is_single()):?>
                    <a href="<?php esc_url(the_permalink()); ?>">
                    <?php crowdpress_post_thumb( $width, $height, true, false ); ?>
                    </a>
                <?php else: ?>
                    <?php crowdpress_post_thumb( $width, $height, true, false ); ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        
        <!-- end blog-media -->
		
        <?php if($blog_style == 'list_view' && !is_single()):?>
        </div>
        <div class="<?php echo esc_attr($class3); ?>">
        <?php endif; ?>
            
        <?php if(is_single()): ?>
        <div class="blog-desc<?php echo esc_attr($align_class); ?>">
        	<div class="blog-des-top">
            <?php if(!empty(get_the_category())):?>
            <span class="cat-title"><?php the_category(', '); ?></span>
            <?php endif; ?>
            
            <?php the_title('<h4 class="blog-des-title">', '</h4>'); ?>
            <?php the_content(sprintf(esc_html__( 'Read More', 'crowdpress' ) ) ); ?>
            </div>
            
            <?php
			if(is_single()):
			wp_link_pages( array(				
				'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'crowdpress' ) . '</span>',					
				'after'       => '</div>',					
				'link_before' => '<span>',					
				'link_after'  => '</span>',					
				'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'crowdpress' ) . ' </span>%',					
				'separator'   => '<span class="screen-reader-text">, </span>',					
			) );
			
			if(function_exists('crowdpress_social_share')):
				$col_social_class = '6';
			else:
				$col_social_class = '12';
			endif;
							
			?>
            <div class="tags-social-box">
                <div class="row">
                    <div class="col-lg-<?php echo esc_attr($col_social_class); ?>">
                        <?php
                        $tags_list = get_the_tag_list('<ul class="list-inline cat_list"><li>','</li><li>','</li></ul>');					
                        if ( $tags_list ): ?>					
                            <div class="blog-tags">
                                <h5><?php echo esc_html__('Tags', 'crowdpress'); ?></h5>				
                                <?php echo wp_kses( 							
                                  $tags_list,							  
                                  // Only allow a tag							  
                                  array(
                                  'ul' => array(								
                                      'class' => array()								  
                                    ),
                                    'li' => array(								
                                      'class' => array()							  
                                    ),								
                                    'a' => array(								
                                      'href' => array()								  
                                    ),								
                                  )							  
                                ); ?>						
                            </div>						
                        <?php					 
                        endif;
                        ?>
                    </div>
                    <?php if($show_social_sharing_icons == 'on'): ?>
                    <div class="col-lg-<?php echo esc_attr($col_social_class); ?>">                	
                        <?php if(function_exists('crowdpress_social_share')): ?>
                            <?php crowdpress_social_share(); ?>
                        <?php endif; ?>                    
                    </div>
                    <?php endif; ?>
                </div>
            </div>
            
			<?php crowdpress_next_prev_posts(); ?>
             
            <?php crowdpress_related_posts(); ?>
            
            <?php get_template_part('author', 'bio'); ?>                 
            
            <?php comments_template( '', true ); ?>
            <?php
        endif; ?>
        </div><!-- end desc -->
        <?php endif; ?>
        <?php if($blog_style == 'list_view' && !is_single()):?>
        </div>
        <?php endif; ?>
    </div>    
</div><!-- end content -->

<?php if(!is_single()): ?>
	<?php if($blog_style == 'grid_view'):?>
    </div>
    <?php endif; ?>
<?php endif; ?>