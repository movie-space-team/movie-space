<div class="ts-posts-default-loop row">
    <?php
	// Posts are found
	if ( $posts->have_posts() ) {
		while ( $posts->have_posts() ) :
			$show_post_thumb = $posts->data['show_post_thumb'];
			$posts->the_post();
			global $post;
			?>
            <div class="col-md-4 col-sm-12">
                <div class="blog-wrapper clearfix">
                	<?php if($show_post_thumb != 'no'): ?>
                	<?php if(has_post_thumbnail()): ?>
                    <div class="post-media">
                    	<a href="<?php esc_url(the_permalink()); ?>"><?php crowdpress_post_thumb( 1000, 650 ); ?></a>
                    </div><!-- end media -->
                    <?php endif; ?>
                    <?php endif; ?>
                    <div class="blog-title">
                        <h4 class="font-heading"><a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a></h4>
                        <?php the_excerpt(); ?>
                        <a href="<?php esc_url(the_permalink()); ?>" class="readmore"><?php echo esc_html__( 'Read more', 'crowdpress' ); ?></a>
                    </div><!-- end blog-title -->
                </div><!-- end clearfix -->
            </div>
		<?php
		endwhile;
	}
	// Posts not found
	else {
	echo '<h4>' . esc_html__( 'Posts not found', 'crowdpress' ) . '</h4>';
	}
	?>
</div>