<div class="container-invis woocommerce product-loop row">
        <?php
		if ( $posts->have_posts() ) {
			$column = round(12/$posts->data['column']);
			
			?>
            <div class="loop-products">
                <?php
                // Posts are found
                    while ( $posts->have_posts() ) :
                        $posts->the_post();
                        global $post;
						global $product;
						$size = 'shop_catalog';
						$args = array();
                        ?>
                    <div class="col-md-<?php echo esc_attr($column); ?>">
                        <div class="product-item">
                            <?php
                            if ( has_post_thumbnail() ) {
                                $image = crowdpress_post_thumb( 600, 500, false );
                            } elseif ( wc_placeholder_img_src() ) {
                                $image = wc_placeholder_img( $size );
                            }
                            ?>
                            <a href="<?php esc_url(the_permalink()); ?>"><?php 
                            echo wp_kses($image,array(
                                'img' => array(
                                  'src' => array(),
                                  'alt' => array(),
                                  'class' => array()
                                ),
                              ));
                            ?></a>
                                
                            <div class="product-hover-content">
                                <div class="hover-content text-right">
                                	<?php
									if ( $product ) {
										$defaults = array(
											'quantity' => 1,
											'class'    => implode( ' ', array_filter( array(
													'button',
													'product_type_' . $product->get_type(),
													$product->is_purchasable() && $product->is_in_stock() ? 'add_to_cart_button' : '',
													$product->supports( 'ajax_add_to_cart' ) ? 'ajax_add_to_cart' : ''
											) ) )
										);
							
										$args = apply_filters( 'woocommerce_loop_add_to_cart_args', wp_parse_args( $args, $defaults ), $product );
										
										echo apply_filters( 'woocommerce_loop_add_to_cart_link',
											sprintf( '<a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="%s"><i class="fa fa-shopping-cart"></i></a>',
												esc_url( $product->add_to_cart_url() ),
												esc_attr( isset( $quantity ) ? $quantity : 1 ),
												esc_attr( $product->get_id() ),
												esc_attr( $product->get_sku() ),
												esc_attr( isset( $class ) ? $class : 'button' )
												),
										$product );
									}
									?>
                                    <h4><a href="<?php esc_url(the_permalink()); ?>"><?php the_title(); ?></a></h4>
                                   <?php if ( $price_html = $product->get_price_html() ) : ?>
                                        <p class="product-price"><?php echo wp_kses($price_html, array('p'=>array('class'=>array()), 'del'=>array('class'=>array()), 'ins'=>array('class'=>array()), 'span'=>array('class'=>array())));  ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div><!-- end col -->
                    </div>
                    <?php
                    endwhile;
                	?>
            </div><!-- end list-wrapper -->
            <?php
            }
            // Posts not found
            else { ?>
            <h4><?php echo esc_html__( 'Product not found', 'crowdpress' ); ?></h4>
            <?php } ?>
</div>