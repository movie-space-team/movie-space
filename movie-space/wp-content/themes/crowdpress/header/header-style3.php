<header class="header transparent-header">
    <div class="header-top">
        <div class="container-fluid">
            <div class="row justify-content-between">
                <div class="col-md-6">
                    <?php
                    $show_logout_link_topbar = (function_exists('ot_get_option'))? ot_get_option( 'show_logout_link_topbar', 'on' ) : 'on';
                    if($show_logout_link_topbar != 'off'):
                    ?>
                        <ul class="profile-menu white-text">                            
                            <?php
                            if(is_user_logged_in()):							
                            ?>                            
                            <li><a href="<?php echo esc_url(wp_logout_url( home_url('/') )); ?>"><i class="fal fa-sign-out-alt"></i><?php echo esc_html__('Logout', 'crowdpress'); ?></a></li>
                            
                            <?php
                            else:
                                if(class_exists( 'woocommerce' )):
                                    $log_in_url = get_permalink( get_option('woocommerce_myaccount_page_id') );
                                else:
                                    $log_in_url = wp_login_url();
                                endif;
                            ?>
                                <li><a href="<?php echo esc_url($log_in_url ); ?>" title="<?php esc_attr_e('Login/Register','crowdpress'); ?>"><i class="far fa-user"></i><?php echo esc_html__('Login/Register','crowdpress'); ?></a></li>
                            
                            <?php
                            endif;
                            ?>
                        </ul>
                    <?php
                    endif;
                    ?>
                    <?php											
                    wp_nav_menu( array(
                    'theme_location'  => 'top-menu-left',
                    'menu_class'      => 'profile-menu left-menu white-text',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'fallback_cb'     => '',
                    'container'       => '',
                    'depth'			  => 1
                    ) );
                    ?>
                </div>
                
                <div class="col-md-6 text-right align-self-end">
                    <?php											
                    wp_nav_menu( array(
                    'theme_location'  => 'top-menu-right',
                    'menu_class'      => 'profile-menu right-menu white-text',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'fallback_cb'     => '',
                    'container'       => '',
                    'depth'			  => 1
                    ) );
                    ?>
                    <?php 
					$show_search_header = (function_exists('ot_get_option'))? ot_get_option( 'show_search_header', 'on' ) : 'on';
					if($show_search_header == 'on'){
						crowdpress_custom_search_form();
					}
					?>
                </div>
            </div>
        </div><!-- end container-fluid -->
    </div><!-- end header-top -->
    <div class="header-main">
        <div class="container-fluid">
        	<nav class="navbar navbar-toggleable-md navbar-inverse">
            	<?php
                $logo = (function_exists('ot_get_option'))? ot_get_option( 'main_logo', CROWDPRESSTHEMEURI . 'images/logo.png' ) : CROWDPRESSTHEMEURI . 'images/logo.png';
				$mobile_logo = (function_exists('ot_get_option'))? ot_get_option( 'mobile_logo', CROWDPRESSTHEMEURI . 'images/logo-light.png' ) : CROWDPRESSTHEMEURI . 'images/logo-light.png';
				?>
                <a class="visible-sec navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><img src="<?php echo esc_url($logo); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
                                        
                <div class="mobile-menu">
                	<div class="menu-backdrop"></div>
                    <div class="close-btn"><i class="fas fa-times"></i></div>
                    <nav class="menu-box">
                        <div class="nav-logo">
                        	<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
							<?php if($mobile_logo != ''): ?>
                                <img src="<?php echo esc_attr($mobile_logo); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
                            <?php else: ?>
                                <?php echo esc_html( get_bloginfo( 'name', 'display' ) ); ?>
                            <?php endif; ?>
                            </a>
                        </div>
                        <div class="menu-outer"></div>
                    </nav>
                </div>
                
                <div class="mobile-nav-toggler"><a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><img src="<?php echo esc_url($logo); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a><i class="fas fa-bars"></i></div>
                <div class="navbar-collapse visible-sec">
                    <?php							
                    wp_nav_menu(
                        array(
                            'theme_location' => 'main-menu',
                            'menu_class' => 'navbar-nav',
                            'container' => false,
                            'fallback_cb'     => '',
                            'walker' => new crowdpress_scm_walker
                        )
                    );
                    ?>
                    <?php $header_button = (function_exists('ot_get_option'))? ot_get_option( 'header_button', '' ) : '';
					$header_button_link = (function_exists('ot_get_option'))? ot_get_option( 'header_button_link', '' ) : '';
					if( $header_button != '' && $header_button_link != '' ):
					?>
                    <a href="<?php echo esc_url($header_button_link); ?>" class="btn btn-primary align-right"><?php echo esc_html($header_button); ?></a>
                    <?php
					endif;
					?>
                </div>
            </nav>
        </div>
    </div>
</header>