<?php
$title = '';
$subtitle = '';
$bg_class = 'bglight';	

if(is_singular('give_forms')){
	$title = (function_exists('ot_get_option'))? ot_get_option('give_single_title', esc_html__('Cause Details', 'crowdpress')) : esc_html__('Cause Details', 'crowdpress');
	$subtitle = (function_exists('ot_get_option'))? ot_get_option('give_single_subtitle', '') : '';
}elseif(is_singular('portfolio')){
	$bg_class = 'bgwhite';
	$title = (function_exists('ot_get_option'))? ot_get_option('portfolio_single_title', '') : '';
	$subtitle = (function_exists('ot_get_option'))? ot_get_option('portfolio_single_subtitle', '') : '';
} elseif(is_single()){
	$title = (function_exists('ot_get_option'))? ot_get_option('blog_single_title', '') : '';
	$subtitle = (function_exists('ot_get_option'))? ot_get_option('blog_single_subtitle', '') : '';
} elseif( is_home() ){		
	$title = (function_exists('ot_get_option'))? ot_get_option('blog_title', '') : '';
	$subtitle = (function_exists('ot_get_option'))? ot_get_option('blog_subtitle', '') : '';
} elseif ( is_category() ){
	$title = esc_html__( 'Category Archives: ', 'crowdpress' ).single_cat_title( '', false );
	if ( category_description() ) :
		$subtitle = category_description();
	endif;

} elseif(is_search()){
	$title = esc_html__('Search Result', 'crowdpress');
	$subtitle = esc_html__( 'This is a Search Results Page for: '. get_search_query(), 'crowdpress' );
} elseif ( is_author() ){
	$title = esc_html__( 'Author Archives: ', 'crowdpress' ).'' . get_the_author() . '';
	if ( category_description() ) :
		$subtitle = category_description();
	endif;
} elseif( is_tag() ) {
	$title = esc_html__( 'Tag Archives: ', 'crowdpress' ).single_tag_title( '', false );
	if ( tag_description() ) :
		$subtitle = tag_description();
	endif;
} elseif ( is_archive() ){	
		
	if ( is_day() ) :
		$title =  esc_html__( 'Daily Archives: ', 'crowdpress' ).'' . get_the_date() . '';
	elseif ( is_month() ) :
		$title = esc_html__( 'Monthly Archives: ', 'crowdpress' ). '' . get_the_date( esc_html_x( 'F Y', 'monthly archives date format', 'crowdpress' ) ) . '' ;
	elseif ( is_year() ) :
		$title = esc_html__( 'Yearly Archives: ', 'crowdpress' ).'' . get_the_date( esc_html_x( 'Y', 'yearly archives date format', 'crowdpress' ) ) . '' ;
	else :
		$title = esc_html__( 'Archives', 'crowdpress' );
	endif;
} elseif(is_404()){
	$title = esc_html__('Page Not Found', 'crowdpress');
	$subtitle = esc_html__( 'Sorry page not found...', 'crowdpress');
	$bg_class = 'bgwhite';
} elseif( is_page() ){
	$bg_class = 'bgwhite';
	$title = get_the_title();
	$custom_title = get_post_meta( get_the_ID(), 'custom_title', true );
	if( $custom_title == 'on' ){
		$alt_title = get_post_meta( get_the_ID(), 'title', true );
		$title = ( $alt_title != '' )? $alt_title : $title;

		$alt_subtitle = get_post_meta( get_the_ID(), 'subtitle', true );
		$subtitle = ( $alt_subtitle != '' )? $alt_subtitle : $subtitle;
	}
} else {
	$title = get_the_title();
}

if(is_post_type_archive('give_forms') || is_tax( 'give_forms_category') ){
	$title = (function_exists('ot_get_option'))? ot_get_option('give_title', esc_html__('Causes', 'crowdpress')) : esc_html__('Causes', 'crowdpress');
	$subtitle = (function_exists('ot_get_option'))? ot_get_option('give_subtitle', '') : '';
	$bg_class = 'bgwhite';
}

if(is_post_type_archive('portfolio')){
	$title = (function_exists('ot_get_option'))? ot_get_option('portfolio_title', '') : '';
	$subtitle = (function_exists('ot_get_option'))? ot_get_option('portfolio_subtitle', '') : '';
	$bg_class = 'bgwhite';
}

if ( class_exists( 'woocommerce' ) ){
	if( is_product() ){
		$title = (function_exists('ot_get_option'))? ot_get_option('shop_single_title', esc_html__('Product Details', 'crowdpress')) : esc_html__('Product Details', 'crowdpress');
		$subtitle = (function_exists('ot_get_option'))? ot_get_option('shop_single_subtitle', '') : '';
		$bg_class = 'bgwhite';
	}
	elseif( is_woocommerce() ){
		$shop_page_id = wc_get_page_id( 'shop' );
		$page_title   = get_the_title( $shop_page_id );
		$title = (function_exists('ot_get_option'))? ot_get_option('shop_title', $page_title) : $page_title;
		$subtitle = (function_exists('ot_get_option'))? ot_get_option('shop_subtitle', '') : '';
		$bg_class = 'bgwhite';
	}
}
?>
<?php
$header_style = (function_exists('ot_get_option'))? ot_get_option( 'header_style', 'style1' ) : 'style1';
?>
<section class="section bg-banner header-<?php echo esc_attr($header_style); ?>">
	<?php if($title != '' || $subtitle !=''): ?>    
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tagline-message page-title1 text-center">
                        <h1><?php echo esc_html($title); ?></h1>
                        <p><?php echo wp_kses($subtitle, array('p'=>array())); ?></p>
                    </div>
                </div><!-- end col -->
            </div><!-- end row -->
        </div>    
	<?php endif; ?>
</section>
    <?php 
    $show_breadcrumbs =  (function_exists('ot_get_option'))? ot_get_option('show_breadcrumbs', 'on') : 'on';
    if($show_breadcrumbs == 'on'): ?>
        <div class="breadcrumb-content <?php echo esc_attr($bg_class); ?>">
        	<div class="container text-center">
            	<?php crowdpress_breadcrumbs(); ?>
            </div>
        </div>
    <?php endif; ?>
