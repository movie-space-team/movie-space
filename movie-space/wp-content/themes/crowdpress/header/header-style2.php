<header class="header transparent-header light-header">
    <div class="header-main">
        <div class="container-fluid">
        	<nav class="navbar navbar-toggleable-md navbar-inverse">
            	<?php
                $logo = (function_exists('ot_get_option'))? ot_get_option( 'main_logo', CROWDPRESSTHEMEURI . 'images/logo.png' ) : CROWDPRESSTHEMEURI . 'images/logo.png';
				$mobile_logo = (function_exists('ot_get_option'))? ot_get_option( 'mobile_logo', CROWDPRESSTHEMEURI . 'images/logo-light.png' ) : CROWDPRESSTHEMEURI . 'images/logo-light.png';
				?>
                <a class="visible-sec navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><img src="<?php echo esc_url($logo); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
                                        
                <div class="mobile-menu">
                	<div class="menu-backdrop"></div>
                    <div class="close-btn"><i class="fas fa-times"></i></div>
                    <nav class="menu-box">
                        <div class="nav-logo">
                        	<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
							<?php if($mobile_logo != ''): ?>
                                <img src="<?php echo esc_attr($mobile_logo); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
                            <?php else: ?>
                                <?php echo esc_html( get_bloginfo( 'name', 'display' ) ); ?>
                            <?php endif; ?>
                            </a>
                        </div>
                        <div class="menu-outer"></div>
                    </nav>
                </div>
                
                <div class="mobile-nav-toggler"><a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><img src="<?php echo esc_url($logo); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a><i class="fas fa-bars"></i></div>
                <div class="navbar-collapse visible-sec">
                    <?php							
                    wp_nav_menu(
                        array(
                            'theme_location' => 'main-menu',
                            'menu_class' => 'navbar-nav',
                            'container' => false,
                            'fallback_cb'     => '',
                            'walker' => new crowdpress_scm_walker
                        )
                    );
                    ?>
                    <?php $header_button = (function_exists('ot_get_option'))? ot_get_option( 'header_button', '' ) : '';
					$header_button_link = (function_exists('ot_get_option'))? ot_get_option( 'header_button_link', '' ) : '';
					if( $header_button != '' && $header_button_link != '' ):
					?>
                    <a href="<?php echo esc_url($header_button_link); ?>" class="btn btn-primary align-right"><?php echo esc_html($header_button); ?></a>
                    <?php
					endif;
					?>
                    <?php 
					$show_search_header = (function_exists('ot_get_option'))? ot_get_option( 'show_search_header', 'on' ) : 'on';
					if($show_search_header == 'on'){
						crowdpress_custom_search_form();
					}
					?>
                </div>
            </nav>
        </div>
    </div>
</header>